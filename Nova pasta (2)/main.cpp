#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <locale>
#include "analisador_lexico/AnalisadorLexico.h"
#include "Scanner.cpp"
#include "analisador_sintatico/AnalisadorSintatico.h"

using namespace std;

int main(int argc, char const *argv[]){
	Scanner scanner;
	scanner.executar(argc, argv);	
	AnalisadorSintatico analisadorSintatico(scanner.getStreamToken());
	analisadorSintatico.executar();
	//scanner.gerarArquivo(argv);
	analisadorSintatico.gerarArquivo(argv);
	return 0;
};
