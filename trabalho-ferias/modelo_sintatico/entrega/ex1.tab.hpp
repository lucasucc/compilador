/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_EX1_TAB_HPP_INCLUDED
# define YY_YY_EX1_TAB_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    T_ID = 258,
    T_DECIMAL = 259,
    T_MAIS = 260,
    T_MENOS = 261,
    T_VEZES = 262,
    T_DIVISAO = 263,
    T_ESQ_PAREN = 264,
    T_DIR_PAREN = 265,
    T_PONTO_VIRGULA = 266,
    T_MAIOR = 267,
    T_MAIOR_IGUAL = 268,
    T_MENOR = 269,
    T_MENOR_IGUAL = 270,
    T_IGUAL_IGUAL = 271,
    T_DIFERENTE_IGUAL = 272,
    T_E = 273,
    T_OU = 274,
    T_DIFERENCA = 275,
    T_CHAVE_ABERTO = 276,
    T_CHAVE_FECHADA = 277,
    T_COLCHETE_ABERTO = 278,
    T_COLCHETE_FECHADO = 279,
    T_VIRGULA = 280,
    T_IGUAL = 281,
    T_FUNCTION = 282,
    T_IF = 283,
    T_ELSE = 284,
    T_WHILE = 285,
    T_RETURN = 286,
    T_BREAK = 287,
    T_CONTINUE = 288,
    T_VAR = 289,
    T_U_MENOS = 290
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 28 "ex1.ypp" /* yacc.c:1909  */
    
    Node *node;
    //NBlock *block;
    //NExpression *expr;
    //NStatement *stmt;
    NoExpr *expr;    
    NoId *ident;
    NoVar *var_decl;
    NoProgram *program;
    //NVariableDeclaration *var_decl;
    //std::vector<NVariableDeclaration*> *varvec;
    //std::vector<NExpression*> *exprvec;
    std::string *string;
    int token;
    NoVarNoFunc *var_dec_dec_func;
    NoDecFunc* dec_func;
    NoBlock *block;
    NoStmt *stmt;
    NoStmtAssign *assign;
    NoBlock1 *block1;
    NoParamList *paramlist;
    NoBlock2 *block2;
    NoStmtBreak *brea;
    NoStmtContinue *continu;
    NoStmtReturn *retur;
    NoArgList *arglist;
    NoStmtFuncall *funcall;
    NoStmtWhile *whil;
    NoStmtIfs *ifs;
    NoUnop *unop;

#line 122 "ex1.tab.hpp" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_EX1_TAB_HPP_INCLUDED  */
