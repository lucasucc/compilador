/********************************************************
 * ex1.l 
 ********************************************************/
%{
#include <iostream>
#include <string>	
#include <fstream>
#include <sstream>
using namespace std;
string teste = "";
string sym = "SYM";
string decimal = "DEC";
string key = "KEY";
string id = "ID";
string linha = "1";
int qtdLinha = 1;
std::ostringstream out;
%}

%option noyywrap

letra  [A-Za-z] 
DECIMAL [0-9][0-9]*
ID     ({letra})+({letra}|({DECIMAL}|_))*
COMENTARIO "//".*"\n"    

%%
"+"      {  teste = teste + sym +" \""+string(yytext)+"\"  "+linha+"\n"; }
"-"		 {  teste = teste + sym +" \"" + string(yytext)+"\" "+linha+"\n"; }	
"*"      {  teste = teste + sym +" \"" + string(yytext)+"\" "+linha+"\n"; }
"/"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n"; }
"("      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
")"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
";"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
">"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
">="     {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
"<"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"<="     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"=="     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"!="     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"&&"     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"||"     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"!"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"{"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"}"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"["      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"]"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
","      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"="      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"def"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"if"     {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"else"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"while"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"return"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+ "\n";  }
"break"    {  teste = teste + key  +"  \""+ string(yytext)+"\" " +linha+"\n";  }
"continue"    {  teste = teste + key  +" \""+ string(yytext)+"\" " +linha+ "\n";  }
"let"   {  teste = teste + key  +"  \""+ string(yytext)+"\" " +linha+ "\n";  }
{ID} {  teste = teste + id  +"  \""+ string(yytext)+"\" " +linha+ "\n";  }
{DECIMAL} {  teste = teste + decimal  +"  \""+ string(yytext)+"\"  " +linha+"\n";  }
{COMENTARIO} { qtdLinha++; out << qtdLinha; linha = out.str(); out.str(""); }
\n  { qtdLinha++; out << qtdLinha; linha = out.str(); out.str(""); }
[ \t\n] {}
.        { teste = teste + "ERROR"+" \"" + string(yytext) +"\" " +linha + "\n";  return 0; }
%%

int main(int argc, char * argv[])
{
  FILE *yyin;  
  ofstream yyout;
  yyout.open(argv[2]);
  yyin = fopen(argv[1], "r");
  yyset_in(yyin);
  yylex();
  fclose(yyin);  
  yyout << teste;
  yyout.close();  
  return 0;
}