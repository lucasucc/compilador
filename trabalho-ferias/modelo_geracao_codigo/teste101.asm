.data 
 MOD:  .word  0 
.text 
_f_mod: 
 move $fp, $sp 
 sw $ra, 0 ($sp) 
 addiu $sp, $sp, -4 
 lw $a0,  4($fp) 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 lw $a0, MOD 
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 div $a0, $t1 ,$a0 
  sw $a0 , 0($sp) 
 addiu $sp, $sp, -4 
lw $a0,  4($fp) 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 lw $a0, -4($fp) 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 lw $a0, MOD 
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 mul $a0, $t1 ,$a0 
  
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 sub $a0, $t1 ,$a0 
 
 j end__f_mod  
 end__f_mod: 
  lw $ra,8($sp) 
 addiu $sp, $sp ,4 
 lw $fp,12($sp) 
 addiu $sp,$sp,12 
 j $ra 
_f_f: 
 move $fp, $sp 
 sw $ra, 0 ($sp) 
 addiu $sp, $sp, -4 
 
 li $t5, -1 
  lw $a0,  4($fp) 
 sw $a0, 0($sp) 
  addiu $sp,$sp,-4 

 li $a0, 0 
 
 lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 beq $a0, $t1  if_0 
 j end_if_0 
 if_0: 

 li $a0, 0 
 
 j end__f_f 
 end_if_0: 
 
 li $t5, -1 
  lw $a0,  4($fp) 
 sw $a0, 0($sp) 
  addiu $sp,$sp,-4 

 li $a0, 1 
 
 lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 beq $a0, $t1  if_1 
 j end_if_1 
 if_1: 
 sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 1 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_mod 
 
 j end__f_f 
 end_if_1: 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 lw $a0,  4($fp) 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 
 li $a0, 1 
  
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 sub $a0, $t1 ,$a0 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_f 
  
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 lw $a0,  4($fp) 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 
 li $a0, 2 
  
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 sub $a0, $t1 ,$a0 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_f 
  
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 add $a0, $t1 ,$a0 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_mod 
 
 j end__f_f  
 end__f_f: 
  lw $ra,4($sp) 
 addiu $sp, $sp ,4 
 lw $fp,8($sp) 
 addiu $sp,$sp,8 
 j $ra 
_f_solve: 
 move $fp, $sp 
 sw $ra, 0 ($sp) 
 addiu $sp, $sp, -4 
  
 #rrr 
 lw $a0,  8($fp) 
 sw $a0 MOD 
 #** 
  #@
 #eee 
 sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 lw $a0,  4($fp) 
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_f 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_print 
   
 end__f_solve: 
  lw $ra,4($sp) 
 addiu $sp, $sp ,4 
 lw $fp,12($sp) 
 addiu $sp,$sp,12 
 j $ra 
_f_print: 
 lw $a0, 4($sp) 
 li $v0, 1 
 syscall 
 li $v0, 11 
 li $a0, 0x0a 
 syscall 
 addiu $sp, $sp, 4 
 lw $fp, 4($sp) 
 addiu $sp, $sp, 4 
 j $ra 
main: 
 sw $fp, 0($sp) 
 addiu $sp, $sp, -4 
 jal _f_main 
 li $v0, 10 
 syscall 
 _f_main: 
 move $fp, $sp 
 sw $ra, 0 ($sp) 
 addiu $sp, $sp, -4 
  
 li $a0 , 0 
 sw $a0 , 0($sp) 
 addiu $sp, $sp, -4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 1 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 1 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_solve 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 10 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 10 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_solve 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 21 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 17 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_solve 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 1000000 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 15 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_solve 
 
 li $t5, -1 
 topo_while_0: 
  #ppppppppp 
 lw $a0, -4($fp) 
 sw $a0, 0($sp) 
  addiu $sp,$sp,-4 

 li $a0, 10 
 
 lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 ble $t1,$a0  while_0
 j end_while_0 
 while_0: #pppp 
 sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 10000 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 lw $a0, -4($fp) 
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_solve 
  
 #sss 
 lw $a0, -4($fp) 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 
 li $a0, 1 
  
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 add $a0, $t1 ,$a0 
 
 #%% 
 sw $a0 , -4($fp) #z 
 
 j topo_while_0
 end_while_0: 
  
 end__f_main: 
  lw $ra,8($sp) 
 addiu $sp, $sp ,4 
 lw $fp,8($sp) 
 addiu $sp,$sp,8 
 j $ra 
