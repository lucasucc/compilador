#include <iostream>
#include <vector>
//#include "node3.h"
//#include <llvm/Value.h>

class TiposEscopoInterface{
public:
    virtual ~TiposEscopoInterface(){};    
    virtual std::string getNome(int qtdParametros) = 0;
};

class Escopo {
public:
    std::vector<TiposEscopoInterface *> atributos;
    int id;

    bool declarado(std::string nome, int qtdParametros){
        int i;
        for ( i = 0; i < this->atributos.size(); ++i)
        {
            if(this->atributos.at(i)->getNome(qtdParametros) == nome){
                return true;
            }
        }
        return false;
    };
};

class VariavelEscopo : public TiposEscopoInterface{
public:    
    int valor;
    std::string nome;

    VariavelEscopo(){

    };

    std::string getNome(int qtdParametros){                
                    return this->nome;
    };    
};

class FuncaoEscopo : public TiposEscopoInterface{
private:
    std::vector<std::string> parametros;
    std::string nome;
public:
    FuncaoEscopo(){

    };

    std::string getNome(int qtdParametros){
                    if(this->parametros.size() == qtdParametros){
                        return this->nome;
                    }
                    return 0;
    };    
};

class Escopos {
private:    
    int idAtual;
    std::vector<Escopo> escopos;
    int idEscopo;    

public:
    
    Escopos(){
        this->idAtual = 0;    
        //std::cout << " idAtual " << this->idAtual << std::endl;
    }; 

    void addEscopo(){
        Escopo escopo;
        escopo.id = this->idAtual;
        this->idAtual++;
        this->escopos.push_back(escopo);
    };

    void addVariavel(VariavelEscopo * variavelEscopo){
        //std::cout << "this->idAtual - 1 >>" << this->idAtual - 1 << std::endl;
        this->escopos.at(this->idAtual - 1).atributos.push_back(variavelEscopo);
    };

    void addFuncao(FuncaoEscopo * funcaoEscopo){
        this->escopos.at(this->idAtual - 1).atributos.push_back(funcaoEscopo);
    };

    bool declaradaNoEscopo(std::string nome, int qtdParametros){
        int i;
        //std::cout<< "no escopo " << this->escopos.size() << std::endl;
        for (i = (this->escopos.size() -1 ); i >= 0; --i)
        {
            //std::cout << "i " << i << std::endl;
            if(this->escopos.at(i).declarado(nome,qtdParametros)){
                return true;
            }
        }
        return false;
    };

    void removerEscopo(){        
        int idRemover = -1;
        int i;
        for (i = 0; i < this->escopos.size(); ++i)
        {
            if(this->escopos.at(i).id == (this->idAtual - 1) ){
                this->idAtual--;
                idRemover = i;
                break;
            }
        }        
        this->escopos.erase(this->escopos.begin()+idRemover);
    };

};


class PrintInterface{
public:
    virtual ~PrintInterface(){};
    virtual std::string print(Escopos *escopos) = 0;
};

class PrintDecVar {
public:
    virtual ~PrintDecVar(){};
    virtual std::string print(Escopos *escopos) = 0;
};

class PrintDecvarDecFuncInterface{
public:    
    virtual ~PrintDecvarDecFuncInterface(){};
    virtual std::string printDecvarDecFunc(Escopos *escopos) = 0;    
};

class PrintStmtInterface{
public:    
    virtual ~PrintStmtInterface(){};
    virtual std::string print(Escopos *escopos) = 0;    
};

class Node {
public:    
    virtual ~Node() {}
    //virtual llvm::Value* codeGen(CodeGenContext& context) { }
    //virtual void print() = 0;
};

class PrintStmtIfElse{
public:
    virtual ~PrintStmtIfElse(){};
    virtual std::string print(Escopos *escopos) = 0;
};


class NoId : public Node{
public:
    std::string id;
    NoId(std::string id){
        this->id = id;
    };    

    std::string printId(Escopos *escopos){
        std::string arvore = "";
        arvore = "["+ this->id + "]";
        return arvore;
    };

};

class NoDecimal : public Node
{
public:
    std::string decimal;
    NoDecimal(std::string decimal){
        this->decimal = decimal;
    };    

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "["+ this->decimal + "]";
        return arvore;
    };

};

//class NoExpr1;
//class NoExpr2;
class NoExpr1 ;

class NoExpr2 ;
/*
class NoExpr : public Node{
public:
    NoExpr1 *noExpr1;
    NoExpr2 *noExpr2;

    NoExpr(NoExpr1 *noExpr1){        
        this->noExpr1 = noExpr1;        
        this->noExpr2 = 0;
    };

    NoExpr(NoExpr2 *noExpr2){
        this->noExpr1 = 0;
        this->noExpr2 = noExpr2;        
    };

    std::string print();
};
*/

class NoExpr;

class NoExpr1 : public PrintInterface{
public:
    NoExpr *noAtual;
    std::string sinal;
    NoExpr *no2;

    NoExpr1(NoExpr *noAtual, std::string sinal, NoExpr *no2){
        this->noAtual = noAtual;
        this->sinal = sinal;
        this->no2 = no2;
    };
    
    std::string print(Escopos *escopos);
        
};

class NoExpr2 : public PrintInterface{
public:
    NoDecimal *noAtual;    

    NoExpr2(NoDecimal *noAtual){
        //std::cout << "construtor noexpr2" << std::endl;
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);
};

class NoExpr3 : public PrintInterface{
public:
    NoId *noAtual;    

    NoExpr3(NoId *noAtual){
        //std::cout << "construtor noexpr2" << std::endl;
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);
};

class NoStmtFuncall;

class NoExpr4 : public PrintInterface{
public:
    NoStmtFuncall *noAtual;    

    NoExpr4(NoStmtFuncall *noAtual){        
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);
};

class NoExpr;

class NoUnop {
public:

    NoExpr *noExpr;
    std::string unop;

    NoUnop(NoExpr *noExpr, std::string unop){
        this->noExpr = noExpr;
        this->unop = unop;
    };

    std::string print(Escopos *escopos);    

};



class NoExpr5 : public PrintInterface{
public:
    NoUnop *noAtual;    

    NoExpr5(NoUnop *noAtual){        
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);
};


class NoExpr {
public:
    PrintInterface *noExpr;

    NoExpr(NoExpr1 *noExpr){        
        this->noExpr = noExpr;                
    };

    NoExpr(NoExpr2 *noExpr){        
        this->noExpr = noExpr;        
    };

    NoExpr(NoExpr3 *noExpr){        
        this->noExpr = noExpr;        
    };

    NoExpr(NoExpr4 *noExpr){
        this->noExpr = noExpr;
    };

    NoExpr(NoExpr5 *noExpr){
        this->noExpr = noExpr;
    };
    
    std::string print(Escopos *escopos);
};

inline std::string NoExpr::print(Escopos *escopos){
    
    return this->noExpr->print(escopos);
    
};

/*
inline std::string NoExpr::print(){
    if(this->noExpr1 == 0 && this->noExpr2 != 0){
        return this->noExpr2->print();
    }else if(this->noExpr1 != 0 && this->noExpr2 == 0){
        return this->noExpr1->print();
    }   
}
*/

inline std::string NoExpr5::print(Escopos *escopos){
    
    return this->noAtual->print(escopos);
    
};

inline std::string NoUnop::print(Escopos *escopos){        
    return "[" + this->unop  + this->noExpr->print(escopos) + "]";    
};

inline std::string NoExpr3::print(Escopos *escopos){        
    return this->noAtual->printId(escopos);
};

inline std::string NoExpr1::print(Escopos *escopos){
    //std::cout << "print NoExpr " << std::endl;
    std::string arvore = "";
    arvore = "[" + this->sinal + " " + this->noAtual->print(escopos) + " " + this->no2->print(escopos) + "]";
    return arvore;
};


inline std::string NoExpr2::print(Escopos *escopos){
    //std::cout <<"print no expr2"<< std::endl;
    std::string arvore = "";
    arvore = this->noAtual->print(escopos);
    return arvore;
};

class NoVar : public Node , public PrintDecvarDecFuncInterface, public PrintDecVar {
public:
    NoId *esquerda;
    NoExpr *direita;
    
    NoVar(NoId *noAtual){
        this->esquerda = noAtual;
        this->direita = 0;    
    };    

    NoVar(NoId *noAtual, NoExpr *noExpr){
        this->esquerda = noAtual;
        this->direita = noExpr;    
    };

    std::string printDecvarDecFunc(Escopos *escopos){
        std::string arvore = "";
        //std::cout << "direita = " << this->direita << std::endl ;  
        //std::cout << "esquerda = " << this->esquerda << std::endl ;           
        VariavelEscopo *variavelEscopo = new VariavelEscopo();    
        if(this->direita == 0){            
            variavelEscopo->nome = this->esquerda->id;
            variavelEscopo->valor = -1;
            //std::cout<< "ate aqui3 chegou" << std::endl;
            escopos->addVariavel(variavelEscopo); //voltar aqui
            arvore = "[decvar " + this->esquerda->printId(escopos) +  "]";
        }else{
            variavelEscopo->nome = this->esquerda->id;
            variavelEscopo->valor = -1;
            //std::cout<< "ate aqui4 chegou" << std::endl;
            escopos->addVariavel(variavelEscopo); //voltar aqui
            arvore = "[decvar " + this->esquerda->printId(escopos) + " " +this->direita->print(escopos) + "]";
        }
        
        return arvore;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        //std::cout << "direita = " << this->direita << std::endl ;  
        //std::cout << "esquerda = " << this->esquerda << std::endl ;  
        if(this->direita == 0){
            arvore = "[decvar" + this->esquerda->printId(escopos) +  "]";
        }else{
            arvore = "[decvar" + this->esquerda->printId(escopos) + " " +this->direita->print(escopos) + "]";
        }
        
        return arvore;
    };
};

class NoParamList {
public:
    
    std::vector<NoId *> ids;

    NoParamList(){

    };

    std::string print(Escopos *escopos){
        std::string arvore = "[paramlist";        
        for (int i = 0; i < ids.size(); ++i)
        {
            arvore = arvore +" "+this->ids.at(i)->printId(escopos);            
        }
        arvore = arvore + "]";
        return arvore;
    };
};


class NoStmtAssign : public PrintStmtInterface{
public:
    NoId *noId;
    NoExpr *noExpr;

    NoStmtAssign(NoId *noId, NoExpr *noExpr){
        this->noId = noId;
        this->noExpr = noExpr;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[assign " + this->noId->printId(escopos) + this->noExpr->print(escopos) +"]";
        return arvore;
    };
};


class NoBlock {
public:    
    std::vector<PrintDecVar *> noPrintBlock;
    NoBlock(){
        //std::cout << "incrementox " << this->noPrintBlock.size() << std::endl;
    };

    std::string print(Escopos *escopos){
      //std::cout << "print NoBlock" << std::endl;
      std::string arvore;
       // std::cout << "novooo " << this->noPrintBlock.size() << std::endl;
        for (int i = 0; i < this->noPrintBlock.size(); ++i)
        {            
            arvore = arvore + this->noPrintBlock.at(i)->print(escopos);
        }        
        //std::cout << "fimm do print NoBlock" << std::endl;
        return arvore;  
    };

};

class NoStmt;

class NoBlock2 {
public:    
    std::vector<NoStmt *> noPrintStmt;
    NoBlock2(){
        //std::cout << "incrementox " << this->noPrintStmt.size() << std::endl;
    };

    std::string print(Escopos *escopos);    

};


class NoBlock1 {
public:    
    NoBlock *noBlock;
    NoBlock2 *noBlock2;
    NoBlock1(){
        this->noBlock = 0;
        this->noBlock2 = 0;
    };

    NoBlock1(NoBlock *noBlock,NoBlock2 *noBlock2){
        this->noBlock = noBlock;
        this->noBlock2 = noBlock2;
    };

    std::string print(Escopos *escopos){    
        std::string arvore = "[block ";
        if(this->noBlock != 0){
            arvore = arvore + this->noBlock->print(escopos);  
        }
        
        if(this->noBlock2 != 0){
            arvore = arvore + this->noBlock2->print(escopos);
        }

        arvore = arvore + "]";
        return arvore; 
    };

};


class NoStmtWhile : public PrintStmtInterface{
public:    
    NoExpr *noExpr;
    NoBlock1 *noBlock1;

    NoStmtWhile(NoExpr *noExpr,NoBlock1 *noBlock1){
        this->noExpr = noExpr;
        this->noBlock1 = noBlock1;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[while " + this->noExpr->print(escopos) + this->noBlock1->print(escopos) +"]";
        return arvore;
    };
};

class NoStmtReturn : public PrintStmtInterface{
public:
    
    NoExpr *noExpr;

    NoStmtReturn(NoExpr *noExpr){        
        this->noExpr = noExpr;
    };

    NoStmtReturn(){        
        this->noExpr = 0;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[return " ;
        if( this->noExpr != 0 ){
            arvore = arvore + this->noExpr->print(escopos);
        }
        arvore = arvore +"]";
        return arvore;
    };
};

class NoStmtBreak : public PrintStmtInterface{
public:
    
    NoStmtBreak(){};

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[break ]";
        return arvore;
    };
};

class NoStmtContinue : public PrintStmtInterface{
public:
    

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[continue ]";
        return arvore;
    };
};

class NoArgList{
public:

    std::vector<NoExpr *> noExprs;

    NoArgList(){

    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[arglist ";
        for (int i = 0; i < noExprs.size(); ++i)
        {
            arvore = arvore + this->noExprs.at(i)->print(escopos);            
        }
        arvore = arvore + "]";
        return arvore;
    };
};

class NoStmtFuncall : public PrintStmtInterface{
public:
    NoId *noId;
    NoArgList *noArgList;

    NoStmtFuncall(NoId *noId, NoArgList *noArgList){
        this->noId = noId;
        this->noArgList = noArgList;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[funccall " + this->noId->printId(escopos) + this->noArgList->print(escopos) +"]";
        return arvore;
    };
};

inline std::string NoExpr4::print(Escopos *escopos){        
    return this->noAtual->print(escopos);
};

class NoStmtIf : public PrintStmtIfElse{
public:
    NoExpr *noExpr;
    NoBlock1 *noBlock1;

    NoStmtIf(NoExpr *noExpr, NoBlock1 *noBlock1){
        this->noExpr = noExpr;
        this->noBlock1 = noBlock1;
    };

    std::string print(Escopos *escopos) {
        return "[if "+ this->noExpr->print(escopos)+ this->noBlock1->print(escopos) + "]";
    };

};

class NoStmtIfElse : public PrintStmtIfElse{
public:
    NoExpr *noExpr;
    NoBlock1 *noBlock1;
    NoBlock1 *noBlock1Else;

    NoStmtIfElse(NoExpr *noExpr, NoBlock1 *noBlock1, NoBlock1 *noBlock1Else){
        this->noExpr = noExpr;
        this->noBlock1 = noBlock1;
        this->noBlock1Else = noBlock1Else;
    };

    std::string print(Escopos *escopos) {
        return "[if "+ this->noExpr->print(escopos) + this->noBlock1->print(escopos) +" " +this->noBlock1Else->print(escopos) + "]";
    };

};

class NoStmtIfs : public PrintStmtInterface{
public:
    PrintStmtIfElse *stmtIfElse;

    NoStmtIfs(NoStmtIf *stmtIfElse){
        this->stmtIfElse = stmtIfElse;        
    };
    
    NoStmtIfs(NoStmtIfElse *stmtIfElse){
        this->stmtIfElse = stmtIfElse;        
    };

    std::string print(Escopos *escopos){
       return this->stmtIfElse->print(escopos);
    };
};

class NoStmt : public PrintDecVar{
public:
    
    PrintStmtInterface *stmt;

    NoStmt(NoStmtAssign *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtBreak *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtContinue *stmt){
        this->stmt = stmt;
    };    

    NoStmt(NoStmtReturn *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtFuncall *stmt){
        this->stmt = stmt;
    };
    
    NoStmt(NoStmtWhile *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtIfs *stmt){
        this->stmt = stmt;
    };

    std::string print(Escopos *escopos){
        return this->stmt->print(escopos);
    };

};


inline std::string NoBlock2::print(Escopos *escopos){
  //std::cout << "print NoBlock" << std::endl;
  std::string arvore;
    //std::cout << "novooo " << this->noPrintStmt.size() << std::endl;
    for (int i = 0; i < this->noPrintStmt.size(); ++i)
    {            
        arvore = arvore + this->noPrintStmt.at(i)->print(escopos);
    }        
   // std::cout << "fimm do print NoBlock" << std::endl;
    return arvore;  
};


class NoDecFunc1 : public PrintDecVar{
public:
    NoId *noAtual;
    NoParamList *no1;
    NoBlock1 *no2;

    NoDecFunc1(NoId *noAtual, NoParamList *no1, NoBlock1 *no2){
        this->noAtual = noAtual;
        this->no1 = no1;    
        this->no2 = no2;
    };

    std::string print(Escopos *escopos){
        //std::cout<< "print da funcao" << std::endl;
        std::string arvore;
        if(this->no2 != 0){
            arvore = "[decfunc " + this->noAtual->printId(escopos) + " "+this->no1->print(escopos) + this->no2->print(escopos) + "]";
        }else{
            arvore = "[decfunc " + this->noAtual->printId(escopos) + " " +this->no1->print(escopos) + "]";
        }
        return arvore;
    };

};


class NoDecFunc : public Node , public PrintDecvarDecFuncInterface
{
public:    
    
    PrintDecVar *decFunc;

    NoDecFunc(NoDecFunc1 *decFunc){        
        this->decFunc = decFunc; 
    };    
    
    std::string printDecvarDecFunc(Escopos *escopos){        
        //std::cout << "aquii print" << std::endl;
        return this->decFunc->print(escopos);        
    };

};


class NoVarNoFunc : public Node{
public:
    std::vector<PrintDecvarDecFuncInterface *> decVarDecFunc;
    NoVarNoFunc(NoVar *noAtual){
        this->decVarDecFunc.push_back(noAtual);
    };

    NoVarNoFunc(){        
    };

    NoVarNoFunc(NoDecFunc *noAtual){
        this->decVarDecFunc.push_back(noAtual);
    };

    std::string print(Escopos *escopos){        
        std::string arvore;
        //escopos->addEscopo();
        for (int i = 0; i < this->decVarDecFunc.size(); ++i)
        {
            arvore = arvore + this->decVarDecFunc.at(i)->printDecvarDecFunc(escopos);
        }        
        return arvore;
    };
};

class NoProgram : public Node{
public:
    NoVarNoFunc *noAtual;
    NoProgram(NoVarNoFunc *noAtual){
        this->noAtual = noAtual;
    };
    
    std::string printProgram(){        
        Escopos escopos;
        escopos.addEscopo();
        //std::cout << "print program" << std::endl;
        std::string arvore = "[program " + this->noAtual->print(&escopos) + "]";
        
        /*        
        if(escopos.declaradaNoEscopo("main",0)){
            std::cout << "foi declarado" << std::endl;
        }else{
            std::cout << "Não foi declarado" << std::endl;
        }
        */
        //aquiiiii
        //std::cout << " final print program" << std::endl;
        return arvore;
    };
};
