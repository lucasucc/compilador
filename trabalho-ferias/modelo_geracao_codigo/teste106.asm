.data 
 .text 
_f_mod: 
 move $fp, $sp 
 sw $ra, 0 ($sp) 
 addiu $sp, $sp, -4 
 
 #eee 
 lw $a0,  4($fp) 
 #444 
 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 
 #eee 
 lw $a0,  8($fp) 
 #444 
 
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 div $a0, $t1 ,$a0 
  sw $a0 , 0($sp) 
 addiu $sp, $sp, -4 

 #eee 
 lw $a0,  4($fp) 
 #444 
 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 
 #eee 
 lw $a0, -4($fp) 
 #444 
 
 sw $a0, 0($sp) 
 addiu $sp,$sp,-4 
 
 #eee 
 lw $a0,  8($fp) 
 #444 
 
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 mul $a0, $t1 ,$a0 
  
  lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 sub $a0, $t1 ,$a0 
 
 j end__f_mod  
 end__f_mod: 
  lw $ra,8($sp) 
 addiu $sp, $sp ,4 
 lw $fp,16($sp) 
 addiu $sp,$sp,16 
 j $ra 
_f_gcd: 
 move $fp, $sp 
 sw $ra, 0 ($sp) 
 addiu $sp, $sp, -4 
 
 li $t5, -1 
  
 #eee 
 lw $a0,  8($fp) 
 #444 
 
 sw $a0, 0($sp) 
  addiu $sp,$sp,-4 

 li $a0, 0 
 
 lw $t1, 4($sp) 
 addiu $sp, $sp , 4 
 beq $a0, $t1  if_0 
 j end_if_0 
 if_0: 

 #eee 
 lw $a0,  4($fp) 
 #444 

 j end__f_gcd 
 end_if_0: 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 #eee 
 lw $a0,  8($fp) 
 #444 
 
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 #eee 
 lw $a0,  4($fp) 
 #444 
 
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_mod 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 #eee 
 lw $a0,  8($fp) 
 #444 
 
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_gcd 
 
 j end__f_gcd  
 end__f_gcd: 
  lw $ra,4($sp) 
 addiu $sp, $sp ,4 
 lw $fp,12($sp) 
 addiu $sp,$sp,12 
 j $ra 
_f_print: 
 lw $a0, 4($sp) 
 li $v0, 1 
 syscall 
 li $v0, 11 
 li $a0, 0x0a 
 syscall 
 addiu $sp, $sp, 4 
 lw $fp, 4($sp) 
 addiu $sp, $sp, 4 
 j $ra 
main: 
 sw $fp, 0($sp) 
 addiu $sp, $sp, -4 
 jal _f_main 
 li $v0, 10 
 syscall 
 _f_main: 
 move $fp, $sp 
 sw $ra, 0 ($sp) 
 addiu $sp, $sp, -4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 21 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 42 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_gcd 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_print 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 1902931 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 259912 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_gcd 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_print 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 0 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 1 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_gcd 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_print 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 42 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 42 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_gcd 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_print 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 0 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 0 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_gcd 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_print 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
  sw $fp, 0($sp) 
 addiu $sp ,$sp ,-4 
 
 li $a0, 56 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
 
 li $a0, 42 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_gcd 
  
 sw $a0, 0($sp) 
 addiu $sp, $sp, -4 
  
 jal _f_print 
   
 end__f_main: 
  lw $ra,4($sp) 
 addiu $sp, $sp ,4 
 lw $fp,4($sp) 
 addiu $sp,$sp,4 
 j $ra 
