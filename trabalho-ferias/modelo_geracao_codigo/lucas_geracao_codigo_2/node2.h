#include <iostream>
#include <vector>
#include <sstream>
//#include "node3.h"
//#include <llvm/Value.h>

class TiposEscopoInterface{
public:    
    virtual ~TiposEscopoInterface(){};    
    virtual std::string getNome(int qtdParametros) = 0;
    virtual std::string getEndereco(int qtdParametros) = 0;
    virtual int getArgumento() = 0;
};

class Escopo {
public:
    std::vector<TiposEscopoInterface *> atributos;
    int id;
    int qtdPush;

    Escopo(){
        this->qtdPush = 0;
    };

    bool declarado(std::string nome, int qtdParametros){
        int i;
        for ( i = 0; i < this->atributos.size(); ++i)
        {
            if(this->atributos.at(i)->getNome(qtdParametros) == nome){
                return true;
            }
        }
        return false;
    };

    std::string endereco(std::string nome){
        int i;
        for ( i = 0; i < this->atributos.size(); ++i)
        {
            std::cout << "nome: " << nome << " "<< this->atributos.at(i)->getNome(0) << " endereco " << this->atributos.at(i)->getEndereco(0) << std::endl;
            if(this->atributos.at(i)->getNome(0) == nome){
                return this->atributos.at(i)->getEndereco(0);
            }
        }
        return "";  
    };

    std::string qtdVariaveisDeclaradas(){
        //int i = this->atributos->size() -1;
        int qtdT = 0;
        for (int i = 0; i < this->atributos.size(); ++i)
        {
            if(this->atributos.at(i)->getArgumento() == 1){
                qtdT++;
            }
        }
        //std::cout << "total " << this->atributos.size() << " qtd push " << this->qtdPush << std::endl;
        int qtd = 4 + 4 *qtdT;
        std::ostringstream ss;
        ss << qtd;
        std::cout << "aquiii " << ss.str() << std::endl;
        return ss.str();
    };

    std::string qtdFP(){
        int qtdT = 0;
        for (int i = 0; i < this->atributos.size(); ++i)
        {
            if(this->atributos.at(i)->getArgumento() == 2 || this->atributos.at(i)->getArgumento() == 1 ){
                qtdT++;
            }
        }
        //std::cout << "total " << this->atributos.size() << " qtd push " << this->qtdPush << std::endl;
        int qtd = 4 + 4 *qtdT;
        std::ostringstream ss;
        ss << qtd;
        std::cout << "aquiii qtdFP " << ss.str() << "size " << this->atributos.size() << std::endl;
        return ss.str();
    };

    std::string somaFP(){
        int qtdT = 0;
        for (int i = 0; i < this->atributos.size(); ++i)
        {
            if(this->atributos.at(i)->getArgumento() == 2 || this->atributos.at(i)->getArgumento() == 1 ){
                qtdT++;
            }
        }
        //std::cout << "total " << this->atributos.size() << " qtd push " << this->qtdPush << std::endl;
        int qtd = 4 + 4 *qtdT;
        std::ostringstream ss;
        ss << qtd;
        std::cout << "aquiii somaFp " << ss.str() << "size " << this->atributos.size() << std::endl;
        return ss.str();
    };

};

class VariavelEscopo : public TiposEscopoInterface{
public:    
    int valor;
    std::string nome;
    std::string endereco;
    std::string sinal;
    int argumento; /* 0 = nao usado , 1 = nao eh argumento e 2 = argumento */

    VariavelEscopo(){

    };

    VariavelEscopo(std::string nome, std::string endereco, int valor){
         this->valor = valor;
         this->nome = nome;
         this->endereco = endereco;
         this->sinal = "-";
         this->argumento = 1;
    };

    VariavelEscopo(std::string nome, std::string endereco, int valor ,std::string sinal , int argumento){
         this->valor = valor;
         this->nome = nome;
         this->endereco = endereco;
         this->sinal = sinal;
         this->argumento = argumento;
    };

    std::string getNome(int qtdParametros){                
                    return this->nome;
    };   

    std::string getEndereco(int qtdParametros){                
                    return this->sinal + this->endereco;
    };
     
    int getArgumento(){
        return this->argumento;
    };
};

class FuncaoEscopo : public TiposEscopoInterface{
private:
    std::vector<std::string> parametros;
    std::string nome;
    std::string endereco;
    int argumento;

public:
    FuncaoEscopo(){
        this->argumento = 0;
    };

    std::string getNome(int qtdParametros){
                    if(this->parametros.size() == qtdParametros){
                        return this->nome;
                    }
                    return 0;
    };    
    
    std::string getEndereco(int qtdParametros){                
                    return this->endereco;
    };

    int getArgumento(){
        return this->argumento;
    };

};

class Escopos {
private:    
    int idAtual;
    std::vector<Escopo> escopos;
    int idEscopo;
    int idIf;
    std::string nomeFS; 
    int idWhile; 
    int idNegado;
    int idAnd;

public:
    
    Escopos(){
        this->idAtual = 0;    
        //std::cout << " idAtual " << this->idAtual << std::endl;
        this->idIf = 0;
        this->nomeFS = "";
        this->idWhile = 0;
        this->idNegado = 0;
        this->idAnd = 0;
    }; 

    std::string getNomeFs(){
        return this->nomeFS;
    };

    void setNomeFs(std::string nomeFs){
        this->nomeFS = nomeFs;
    };

    void incrementarIdIf(){
        this->idIf++;
    };

    std::string getIdIf(){
        std::ostringstream ss;
        ss << this->idIf;
        return ss.str();
    };

    void incrementarIdWhile(){
        this->idWhile++;
    };

    std::string getIdWhile(){
        std::ostringstream ss;
        ss << this->idWhile;
        return ss.str();
    };

    int getIdWhileInt(){        
        return this->idWhile;        
    };

    void incrementarIdNegado(){
        this->idNegado++;
    };

    std::string getIdNegado(){
        std::ostringstream ss;
        ss << this->idNegado;
        return ss.str();
    };

    void incrementarIdAnd(){
        this->idAnd++;
    };

    std::string getIdAnd(){
        std::ostringstream ss;
        ss << this->idAnd;
        return ss.str();
    };

    void incrementarIdOr(){
        this->idAnd++;
    };

    std::string getIdOr(){
        std::ostringstream ss;
        ss << this->idAnd;
        return ss.str();
    };
    
    void addEscopo(){
        Escopo escopo;
        escopo.id = this->idAtual;
        this->idAtual++;
        this->escopos.push_back(escopo);
    };

    void addVariavel(VariavelEscopo * variavelEscopo){
        //std::cout << "this->idAtual - 1 >>" << this->idAtual - 1 << std::endl;
        this->escopos.at(this->idAtual - 1).atributos.push_back(variavelEscopo);
    };

    void addFuncao(FuncaoEscopo * funcaoEscopo){
        this->escopos.at(this->idAtual - 1).atributos.push_back(funcaoEscopo);
    };

    bool declaradaNoEscopo(std::string nome, int qtdParametros){
        int i;
        //std::cout<< "no escopo " << this->escopos.size() << std::endl;
        for (i = (this->escopos.size() -1 ); i >= 0; --i)
        {
            //std::cout << "i " << i << std::endl;
            if(this->escopos.at(i).declarado(nome,qtdParametros)){
                return true;
            }
        }
        return false;
    };

    std::string enderecoNoEscopo(std::string nome, int qtdParametros){
        int i;
        std::cout<< "no escopo size " << this->escopos.size() << std::endl;
        
        for (i = (this->escopos.size() -1 ); i > 0; --i)
        {
            //std::cout << "i " << i << std::endl;
            if(this->escopos.at(i).declarado(nome,qtdParametros)){
                std::cout << "esta aqui no escopo" << std::endl;

                return this->escopos.at(i).endereco(nome);
            }
        }
        return "0";
    };

    void removerEscopo(){        
        int idRemover = -1;
        int i;
        for (i = 0; i < this->escopos.size(); ++i)
        {
            if(this->escopos.at(i).id == (this->idAtual - 1) ){
                this->idAtual--;
                idRemover = i;
                break;
            }
        }        
        std::cout << " idRemover " << idRemover << std::endl;
        this->escopos.erase(this->escopos.begin()+idRemover);
    };

    int getQtdPilha(){
      return this->escopos.at(this->idAtual - 1).qtdPush;  
    };

    void setQtdPilha(){
      this->escopos.at(this->idAtual - 1).qtdPush = this->escopos.at(this->idAtual - 1).qtdPush + 4;  
    };

    std::string qtdVariaveisDeclaradas(int id){
        int i = this->escopos.size() - 1;
        std::cout << "id atual " << this->idAtual << " escopo size " << this->escopos.size() << std::endl;
        for (i = 0; i < this->escopos.size(); ++i)
        {
            if(this->escopos.at(i).id == (id) ){
                //this->idAtual--;
                //idRemover = i;
                std::cout << " i " << i << std::endl;
                return this->escopos.at(i).qtdVariaveisDeclaradas();
                //break;
            }
        }
        
    };

    std::string qtdFP(int id){
        int i = this->escopos.size() - 1;
        std::cout << "id atual " << this->idAtual << " escopo size " << this->escopos.size() << std::endl;
        for (i = 0; i < this->escopos.size(); ++i)
        {
            if(this->escopos.at(i).id == (id) ){
                //this->idAtual--;
                //idRemover = i;
                std::cout << " i " << i << std::endl;
                return this->escopos.at(i).qtdFP();
                //break;
            }
        }
        
    };

    std::string somaFP(int id){
        int i = this->escopos.size() - 1;
        std::cout << "id atual " << this->idAtual << " escopo size " << this->escopos.size() << std::endl;
        for (i = 0; i < this->escopos.size(); ++i)
        {
            if(this->escopos.at(i).id == (id) ){
                //this->idAtual--;
                //idRemover = i;
                std::cout << " i " << i << std::endl;
                return this->escopos.at(i).somaFP();
                //break;
            }
        }
        
    };


    int getEscopoAtual(){
        return this->idAtual;
    }
};


class PrintInterface{
public:
    virtual ~PrintInterface(){};
    virtual std::string print(Escopos *escopos) = 0;
    virtual std::string tipo(Escopos *escopos) = 0;
};

class PrintDecVar {
public:
    virtual ~PrintDecVar(){};
    virtual std::string print(Escopos *escopos) = 0;
};

class PrintDecvarDecFuncInterface{
public:    
    virtual ~PrintDecvarDecFuncInterface(){};
    virtual std::string printDecvarDecFunc(Escopos *escopos) = 0;    
};

class PrintStmtInterface{
public:    
    virtual ~PrintStmtInterface(){};
    virtual std::string print(Escopos *escopos) = 0;    
};

class Node {
public:    
    virtual ~Node() {}
    //virtual llvm::Value* codeGen(CodeGenContext& context) { }
    //virtual void print() = 0;
};

class PrintStmtIfElse{
public:
    virtual ~PrintStmtIfElse(){};
    virtual std::string print(Escopos *escopos) = 0;
};


class NoId : public Node{
public:
    std::string id;
    NoId(std::string id){
        this->id = id;
    };    

    std::string printId(Escopos *escopos){
        std::string arvore = "";
        arvore = this->id ;
        return arvore;
    };

};

class NoDecimal : public Node
{
public:
    std::string decimal;
    NoDecimal(std::string decimal){
        this->decimal = decimal;
    };    

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "\n li $a0, "+ this->decimal + " \n ";
        //arvore = "["+ this->decimal + "]";
        return arvore;
    };

};

//class NoExpr1;
//class NoExpr2;
class NoExpr1 ;

class NoExpr2 ;
/*
class NoExpr : public Node{
public:
    NoExpr1 *noExpr1;
    NoExpr2 *noExpr2;

    NoExpr(NoExpr1 *noExpr1){        
        this->noExpr1 = noExpr1;        
        this->noExpr2 = 0;
    };

    NoExpr(NoExpr2 *noExpr2){
        this->noExpr1 = 0;
        this->noExpr2 = noExpr2;        
    };

    std::string print();
};
*/

class NoExpr;

class NoExpr1 : public PrintInterface{
public:
    NoExpr *noAtual;
    std::string sinal;
    NoExpr *no2;

    NoExpr1(NoExpr *noAtual, std::string sinal, NoExpr *no2){
        this->noAtual = noAtual;
        this->sinal = sinal;
        this->no2 = no2;
    };
    
    std::string print(Escopos *escopos);
    
    std::string tipo(Escopos *escopos);
};

class NoExpr2 : public PrintInterface{
public:
    NoDecimal *noAtual;    

    NoExpr2(NoDecimal *noAtual){
        //std::cout << "construtor noexpr2" << std::endl;
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);

    std::string tipo(Escopos *escopos);
};

class NoExpr3 : public PrintInterface{
public:
    NoId *noAtual;    

    NoExpr3(NoId *noAtual){
        //std::cout << "construtor noexpr2" << std::endl;
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);

    std::string tipo(Escopos *escopos);
};

class NoStmtFuncall;

class NoExpr4 : public PrintInterface{
public:
    NoStmtFuncall *noAtual;    

    NoExpr4(NoStmtFuncall *noAtual){        
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);

    std::string tipo(Escopos *escopos);

};

class NoExpr;

class NoUnop {
public:

    NoExpr *noExpr;
    std::string unop;

    NoUnop(NoExpr *noExpr, std::string unop){
        this->noExpr = noExpr;
        this->unop = unop;
    };

    std::string print(Escopos *escopos);    

};



class NoExpr5 : public PrintInterface{
public:
    NoUnop *noAtual;    

    NoExpr5(NoUnop *noAtual){        
        this->noAtual = noAtual;        
    };

    std::string print(Escopos *escopos);
    std::string tipo(Escopos *escopos);
};


class NoExpr {
public:
    PrintInterface *noExpr;

    NoExpr(NoExpr1 *noExpr){        
        this->noExpr = noExpr;                
    };

    NoExpr(NoExpr2 *noExpr){        
        this->noExpr = noExpr;        
    };

    NoExpr(NoExpr3 *noExpr){        
        this->noExpr = noExpr;        
    };

    NoExpr(NoExpr4 *noExpr){
        this->noExpr = noExpr;
    };

    NoExpr(NoExpr5 *noExpr){
        this->noExpr = noExpr;
    };
    
    std::string print(Escopos *escopos);

    std::string tipo(Escopos *escopos);

};

inline std::string NoExpr::print(Escopos *escopos){
    
    return this->noExpr->print(escopos);
    
};

inline std::string NoExpr::tipo(Escopos *escopos){
    
    return this->noExpr->tipo(escopos);
    
};

/*
inline std::string NoExpr::print(){
    if(this->noExpr1 == 0 && this->noExpr2 != 0){
        return this->noExpr2->print();
    }else if(this->noExpr1 != 0 && this->noExpr2 == 0){
        return this->noExpr1->print();
    }   
}
*/

inline std::string NoExpr5::print(Escopos *escopos){
    
    return this->noAtual->print(escopos);
    
};

inline std::string NoExpr5::tipo(Escopos *escopos){
    //std::cout << "print NoExpr " << std::endl;
    
    return "";
};

inline std::string NoUnop::print(Escopos *escopos){        
    //return "[" + this->unop  + this->noExpr->print(escopos) + "]";    
    std::string arvore = "";
    std::cout << "this->unop >>> " << this->unop << std::endl;
    std::string t = escopos->getIdNegado();
    escopos->incrementarIdNegado();
    if(this->unop == "-"){
       arvore = "\n "+this->noExpr->print(escopos) +" \n  move $t1, $a0  \n li $a0, -1 \n mul $a0, $t1 ,$a0  \n ";
    }else{
        //talvez alterar
        arvore = "\n " +this->noExpr->print(escopos) +" \n move $t1, $a0 \n li $a0, 0 \n beq $a0, $t1, inv_"+ t + " \n li $a0,0  \n j end_inv_"+t+ "\n inv_"+t+": \n li $a0, 1 \n end_inv_"+t+": \n";
    }
    return arvore;
    //return "[" + this->unop  + this->noExpr->print(escopos) + "]";    
};

inline std::string NoExpr3::print(Escopos *escopos){        
   std::cout << "escopoo: " << escopos->enderecoNoEscopo(this->noAtual->id,0) << this->noAtual->id << std::endl;
   std::string t = escopos->enderecoNoEscopo(this->noAtual->id,0);
   if(t != "0" ){
     //return "lw $a0, -"+ escopos->enderecoNoEscopo(this->noAtual->id,0) + "($fp)";
    return "lw $a0, "+ escopos->enderecoNoEscopo(this->noAtual->id,0) + "($fp)";
   }else{    
   //aquiii
     return "lw $a0, " +this->noAtual->printId(escopos);
   }
   //return "lw $a0, 0($sp)"; 
    //return this->noAtual->printId(escopos);
};

inline std::string NoExpr3::tipo(Escopos *escopos){
    //std::cout << "print NoExpr " << std::endl;
    return "";
};

inline std::string NoExpr1::print(Escopos *escopos){
    //std::cout << "print NoExpr " << std::endl;
    std::string arvore = "";
    if(this->sinal == "+"){
        
       //arvore = this->noAtual->print(escopos) +" \n sw $a0, 0($sp) \n addiu $sp,$sp,-4 \n " +  this->no2->print(escopos) +" \n  lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n add $a0, $t1 ,$a0 \n";
        arvore = this->noAtual->print(escopos) +" \n sw $a0, 0($sp) \n addiu $sp,$sp,-4 \n " +  this->no2->print(escopos) +" \n  lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n add $a0, $t1 ,$a0 \n ";
    }else if(this->sinal == "-"){
        arvore = this->noAtual->print(escopos) +" \n sw $a0, 0($sp) \n addiu $sp,$sp,-4 \n " +  this->no2->print(escopos) +" \n  lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n sub $a0, $t1 ,$a0 \n ";
    }else if(this->sinal == "*"){
        arvore = this->noAtual->print(escopos) +" \n sw $a0, 0($sp) \n addiu $sp,$sp,-4 \n " +  this->no2->print(escopos) +" \n  lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n mul $a0, $t1 ,$a0 \n ";
    }else if(this->sinal == "/"){    
        arvore = this->noAtual->print(escopos) +" \n sw $a0, 0($sp) \n addiu $sp,$sp,-4 \n " +  this->no2->print(escopos) +" \n  lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n div $a0, $t1 ,$a0 \n ";
    }else if(this->sinal == "=="){
        arvore = " " + this->noAtual->print(escopos) + " \n sw $a0, 0($sp) \n  addiu $sp,$sp,-4 \n" +  this->no2->print(escopos) + "\n lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n beq $a0, $t1 ";
    }else if(this->sinal == "!="){
        arvore = " " + this->noAtual->print(escopos) + " \n sw $a0, 0($sp) \n  addiu $sp,$sp,-4 \n" +  this->no2->print(escopos) + "\n lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n bne $a0, $t1 ";
    }else if(this->sinal == ">"){   
        arvore = " #qqqqqqqqq \n " + this->noAtual->print(escopos) + " \n sw $a0, 0($sp) \n  addiu $sp,$sp,-4 \n" +  this->no2->print(escopos) + "\n lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n slt $t0, $a0, $t1 \n bne $t0,$zero ";
    }else if(this->sinal == ">="){
        arvore = " #qqqqqqqqq \n " + this->noAtual->print(escopos) + " \n sw $a0, 0($sp) \n  addiu $sp,$sp,-4 \n" +  this->no2->print(escopos) + "\n lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n bge $t1, $a0 ";
    }else if(this->sinal == "<"){
        arvore = " #ppppppppp \n " + this->noAtual->print(escopos) + " \n sw $a0, 0($sp) \n  addiu $sp,$sp,-4 \n" +  this->no2->print(escopos) + "\n lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n slt $t0, $a0, $t1 \n beq $t0,$zero ";
    }else if(this->sinal == "<="){
        arvore = " #ppppppppp \n " + this->noAtual->print(escopos) + " \n sw $a0, 0($sp) \n  addiu $sp,$sp,-4 \n" +  this->no2->print(escopos) + "\n lw $t1, 4($sp) \n addiu $sp, $sp , 4 \n ble $t1,$a0 ";
    }else if(this->sinal == "&&"){
        std::string t = escopos->getIdAnd();
        escopos->incrementarIdAnd();
        arvore = "\n #$66 \n beq $t5, -1 inicio_"+ t+" \n j end_inicio_"+t+"\n inicio_"+ t+":\n li $t5 , 1 \n end_inicio_"+t + ": \n "+ this->noAtual->print(escopos) + " true_1_"+ t+ " \n li $t5,0 \n j end_true_1_"+t+" \n true_1_"+t+": li $t5, 1 \n  end_true_1_"+t+": \n #$ \n " + this->no2->print(escopos)+ " true_2_"+t +" \n li $t2,0 \n j end_true_2_"+t+" \n true_2_"+t+": \n  li $t2,1 \n end_true_2_"+t+": \n and $t5,$t5,$t2 \n li $t2,1 \n #teste \n beq $t5, $t2";
    }else if(this->sinal == "||"){
        std::string t = escopos->getIdOr();
        escopos->incrementarIdOr();
        arvore = "\n #$66 \n beq $t5, -1 inicio_"+ t+" \n j end_inicio_"+t+"\n inicio_"+ t+":\n li $t5 , 0 \n end_inicio_"+t + ": \n "+ this->noAtual->print(escopos) + " true_1_"+ t+ " \n li $t5,0 \n j end_true_1_"+t+" \n true_1_"+t+": li $t5, 1 \n  end_true_1_"+t+": \n #$ \n " + this->no2->print(escopos)+ " true_2_"+t +" \n li $t2,0 \n j end_true_2_"+t+" \n true_2_"+t+": \n  li $t2,1 \n end_true_2_"+t+": \n or $t5,$t5,$t2 \n li $t2,1 \n #teste \n beq $t5, $t2";
    }

    //arvore = "[" + this->sinal + " " + this->noAtual->print(escopos) + " " + this->no2->print(escopos) + "]";
    return arvore;
};

inline std::string NoExpr1::tipo(Escopos *escopos){
    //std::cout << "print NoExpr " << std::endl;
    return "";
};


inline std::string NoExpr2::print(Escopos *escopos){
    //std::cout <<"print no expr2"<< std::endl;
    std::string arvore = "";
    arvore = this->noAtual->print(escopos);
    //arvore = this->noAtual->print(escopos);
    return arvore;
};

inline std::string NoExpr2::tipo(Escopos *escopos){
    //std::cout << "print NoExpr " << std::endl;
    std::string arvore;
    arvore = this->noAtual->decimal;
    return arvore;
};

class NoVar : public Node , public PrintDecvarDecFuncInterface, public PrintDecVar {
public:
    NoId *esquerda;
    NoExpr *direita;
    
    NoVar(NoId *noAtual){
        this->esquerda = noAtual;
        this->direita = 0;    
    };    

    NoVar(NoId *noAtual, NoExpr *noExpr){
        this->esquerda = noAtual;
        this->direita = noExpr;    
    };

    std::string printDecvarDecFunc(Escopos *escopos){
        std::string arvore = "";
        std::cout << "aquii" << std::endl;
        //std::cout << "direita = " << this->direita << std::endl ;  
        //std::cout << "esquerda = " << this->esquerda << std::endl ;           
        VariavelEscopo *variavelEscopo = new VariavelEscopo();    
        if(this->direita == 0){            
            variavelEscopo->nome = this->esquerda->id;
            variavelEscopo->valor = -1;
            //std::cout<< "ate aqui3 chegou" << std::endl;
            escopos->addVariavel(variavelEscopo); //voltar aqui            
            //arvore = "[decvar " + this->esquerda->printId(escopos) +  "]";
            arvore = this->esquerda->printId(escopos) + ":  .word  0 \n";
        }else{
            variavelEscopo->nome = this->esquerda->id;
            variavelEscopo->valor = -1;
            //std::cout<< "ate aqui4 chegou" << std::endl;
            escopos->addVariavel(variavelEscopo); //voltar aqui
            //arvore = "[decvar " + this->esquerda->printId(escopos) + " " +this->direita->print(escopos) + "]";
            arvore =  this->esquerda->printId(escopos) + ":  .word   " +this->direita->tipo(escopos) + " \n ";
        }
        
        return arvore;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        std::cout << "print inicio" << std::endl;

        //std::cout << "direita = " << this->direita << std::endl ;  
        //std::cout << "esquerda = " << this->esquerda << std::endl ;  
        //escopos->getQtdPilha();
        std::ostringstream ss;
        VariavelEscopo *variavelEscopo;    
        if(this->direita == 0){
            ss << escopos->getQtdPilha();
            arvore = "li $a0 , 0 \n aaaaaaaa sw $a0 , "+ ss.str() +"($sp) \n addiu $sp, $sp, -4 ";
            
            escopos->setQtdPilha(); 
            variavelEscopo = new VariavelEscopo(this->esquerda->id,ss.str(),0);
            ss.clear();
            //variavelEscopo->nome = this->esquerda->id;
            //variavelEscopo->valor = 0;
            escopos->addVariavel(variavelEscopo);    
           // arvore = " sw $a0,  \n addiu $sp, 4($sp)"
            //arvore = "[decvar" + this->esquerda->printId(escopos) +  "]";
        }else{
            ss << escopos->getQtdPilha() + 4;
            this->direita->print(escopos); //aqui
            //arvore = this->direita->print(escopos) + " sw $a0 , "+ ss.str() +"($sp) \n addiu $sp, $sp, -4 \n";
            arvore = this->direita->print(escopos) + " sw $a0 , 0($sp) \n addiu $sp, $sp, -4 \n";
            escopos->setQtdPilha(); 
            variavelEscopo = new VariavelEscopo(this->esquerda->id,ss.str(),0);
            ss.clear();
            //variavelEscopo->nome = this->esquerda->id;
            //variavelEscopo->valor = 0;
            escopos->addVariavel(variavelEscopo);    
            //std::cout<< "variavies declaradas " << escopos->qtdVariaveisDeclaradas() << std::endl;
            //arvore = "[decvar" + this->esquerda->printId(escopos) + " " +this->direita->print(escopos) + "]";
        }
        std::cout << "print final" << std::endl;        
        return arvore;
    };
};

class NoParamList {
public:
    
    std::vector<NoId *> ids;

    NoParamList(){

    };

    std::string print(Escopos *escopos){
        std::cout << "no paramlist" << " ids.size() " << ids.size() << std::endl;
        //std::string arvore = "[paramlist";                
        std::string arvore ="";
        int i;
        std::string p;
        for ( i = (ids.size() -1) ; i >= 0 ; --i)
        {
            std::cout << "vai teste" << std::endl;
            int z = i*4 + 4;
            std::ostringstream ss;
            ss << z;
            std::cout << "z " << z << std::endl;
            p = "" + ss.str() + "";
            ss.clear();
            //arvore = arvore + " # \n addiu $sp, $sp, -4 \n";
            VariavelEscopo *v = new VariavelEscopo(this->ids.at(i)->printId(escopos),p,0," ",2);
            escopos->addVariavel(v);
            //arvore = arvore +" "+this->ids.at(i)->printId(escopos);            
        }        
        return arvore;
    };
};


class NoStmtAssign : public PrintStmtInterface{
public:
    NoId *noId;
    NoExpr *noExpr;

    NoStmtAssign(NoId *noId, NoExpr *noExpr){
        this->noId = noId;
        this->noExpr = noExpr;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = escopos->enderecoNoEscopo(this->noId->id,0);
        arvore = " \n "+ this->noExpr->print(escopos) + " \n sw $a0 , "+ arvore + "($fp) #z \n"; 
        //arvore = "[assign " + this->noId->printId(escopos) + this->noExpr->print(escopos) +"]";
        return arvore;
    };
};


class NoBlock {
public:    
    std::vector<PrintDecVar *> noPrintBlock;
    NoBlock(){
        //std::cout << "incrementox " << this->noPrintBlock.size() << std::endl;
    };

    std::string print(Escopos *escopos){
      //std::cout << "print NoBlock" << std::endl;
      std::string arvore;
       // std::cout << "novooo " << this->noPrintBlock.size() << std::endl;
        for (int i = 0; i < this->noPrintBlock.size(); ++i)
        {            
            arvore = arvore + this->noPrintBlock.at(i)->print(escopos);
        }        
        //std::cout << "fimm do print NoBlock" << std::endl;
        return arvore;  
    };

};

class NoStmt;

class NoBlock2 {
public:    
    std::vector<NoStmt *> noPrintStmt;
    NoBlock2(){
        //std::cout << "incrementox " << this->noPrintStmt.size() << std::endl;
    };

    std::string print(Escopos *escopos);    

};


class NoBlock1 {
public:    
    NoBlock *noBlock;
    NoBlock2 *noBlock2;
    NoBlock1(){
        this->noBlock = 0;
        this->noBlock2 = 0;
    };

    NoBlock1(NoBlock *noBlock,NoBlock2 *noBlock2){
        this->noBlock = noBlock;
        this->noBlock2 = noBlock2;
    };

    std::string print(Escopos *escopos){    
        //std::string arvore = "[block ";
        std::string arvore = "";
        if(this->noBlock != 0){
            arvore = arvore + this->noBlock->print(escopos);  
        }
        
        if(this->noBlock2 != 0){
            arvore = arvore + this->noBlock2->print(escopos);
        }

        //arvore = arvore + "]";
        arvore = arvore;
        return arvore; 
    };

};


class NoStmtWhile : public PrintStmtInterface{
public:    
    NoExpr *noExpr;
    NoBlock1 *noBlock1;

    NoStmtWhile(NoExpr *noExpr,NoBlock1 *noBlock1){
        this->noExpr = noExpr;
        this->noBlock1 = noBlock1;
    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        //arvore = "[while " + this->noExpr->print(escopos) + this->noBlock1->print(escopos) +"]";
        std::string t = escopos->getIdWhile();
        escopos->incrementarIdWhile();

        arvore = "\n li $t5, -1 \n topo_while_"+t+": \n "+ this->noExpr->print(escopos) + " while_"+t + "\n "+ "j end_while_" +t+ " \n while_"+t+": #pppp \n"  + this->noBlock1->print(escopos) +" \n j topo_while_"+ t + "\n end_while_" + t +": \n";
        return arvore;
    };
};

class NoStmtReturn : public PrintStmtInterface{
public:
    
    NoExpr *noExpr;

    NoStmtReturn(NoExpr *noExpr){        
        this->noExpr = noExpr;
    };

    NoStmtReturn(){        
        this->noExpr = 0;
    };

    /*
    std::string print(Escopos *escopos){
        std::string arvore = "";
        arvore = "[return " ;
        if( this->noExpr != 0 ){
            arvore = arvore + this->noExpr->print(escopos);
        }
        arvore = arvore +"]";
        return arvore;
    };
    */

    std::string print(Escopos *escopos){
        std::string arvore = "";
        //arvore = "[return " ;
        if( this->noExpr != 0 ){
            arvore = arvore + this->noExpr->print(escopos) + "\n j " + escopos->getNomeFs();
        }
        //arvore = arvore +"]";
        return arvore;
    };

};

class NoStmtBreak : public PrintStmtInterface{
public:
    
    NoStmtBreak(){};

    std::string print(Escopos *escopos){
        std::string arvore = "";
        //arvore = "[break ]";
        std::ostringstream ss;
        ss << escopos->getIdWhileInt() - 1 ;
        std::string t = ss.str();
        //int i = ;
        
        //t << i;

        arvore = "\n # ppp \n j end_while_"+ t + " \n #iii \n";
        return arvore;
    };
};

class NoStmtContinue : public PrintStmtInterface{
public:
    

    std::string print(Escopos *escopos){
        std::string arvore = "";
        //arvore = "[continue ]";
        
        std::ostringstream ss;
        ss << escopos->getIdWhileInt() - 1 ;
        std::string t = ss.str();
        arvore = "\n # ooo \n j topo_while_"+ t + " \n #hhhh \n";    

        return arvore;
    };
};

class NoArgList{
public:

    std::vector<NoExpr *> noExprs;

    NoArgList(){

    };

    std::string print(Escopos *escopos){
        std::string arvore = "";
        //arvore = "[arglist ";
        std::cout << "aquii no print arglist:" << this->noExprs.size() << std::endl;
        
        for (int i = (noExprs.size()- 1); i >= 0 ; --i)
        {
            std::cout << i << std::endl;
            arvore = arvore + this->noExprs.at(i)->print(escopos) + " \n sw $a0, 0($sp) \n addiu $sp, $sp, -4 \n ";
        }
        std::cout << "final print arglist:" << std::endl;
        //arvore = arvore + "]";
        return arvore;
    };
};

class NoStmtFuncall : public PrintStmtInterface{
public:
    NoId *noId;
    NoArgList *noArgList;

    NoStmtFuncall(NoId *noId, NoArgList *noArgList){
        this->noId = noId;
        this->noArgList = noArgList;
    };

    std::string print(Escopos *escopos){
        std::cout << "inicio funcall: " << this->noArgList << " " << this->noId->id << std::endl;
        std::string arvore = "";
        //arvore = "[funccall " + this->noId->printId(escopos) + this->noArgList->print(escopos) +"]";
        arvore = " sw $fp, 0($sp) \n addiu $sp ,$sp ,-4 \n " + this->noArgList->print(escopos) + " \n jal _f_"+this->noId->id +" \n ";
        //arvore = "[funccall " + this->noId->printId(escopos) + this->noArgList->print(escopos) +"]";
        std::cout << "final da funcall" << std::endl;
        return arvore;
    };
};

inline std::string NoExpr4::print(Escopos *escopos){        
    return this->noAtual->print(escopos);
};

inline std::string NoExpr4::tipo(Escopos *escopos){
    return "";
};

class NoStmtIf : public PrintStmtIfElse{
public:
    NoExpr *noExpr;
    NoBlock1 *noBlock1;

    NoStmtIf(NoExpr *noExpr, NoBlock1 *noBlock1){
        this->noExpr = noExpr;
        this->noBlock1 = noBlock1;
    };

    std::string print(Escopos *escopos) {
        std::string t = escopos->getIdIf();
        escopos->incrementarIdIf();
        //return "[if "+ this->noExpr->print(escopos)+ " if_" + t +" \n j end_if_"+ t + " \n kkkkkkkk  if_"+ t +": "+ this->noBlock1->print(escopos) + " end_if_" + t + ": \n ]";
        return "\n li $t5, -1 \n "+ this->noExpr->print(escopos)+ " if_" + t +" \n j end_if_"+ t + " \n if_"+ t +": \n"+ this->noBlock1->print(escopos) + " \n end_if_" + t + ": \n ";
    };

};

class NoStmtIfElse : public PrintStmtIfElse{
public:
    NoExpr *noExpr;
    NoBlock1 *noBlock1;
    NoBlock1 *noBlock1Else;

    NoStmtIfElse(NoExpr *noExpr, NoBlock1 *noBlock1, NoBlock1 *noBlock1Else){
        this->noExpr = noExpr;
        this->noBlock1 = noBlock1;
        this->noBlock1Else = noBlock1Else;
    };

    std::string print(Escopos *escopos) {
        std::string t = escopos->getIdIf();
        escopos->incrementarIdIf();
        return " \n li $t5 ,-1 \n"+ this->noExpr->print(escopos)+ " if_" + t +" \n j else_if_"+ t + " \n if_"+ t +": \n"+ this->noBlock1->print(escopos) + "\n j end_if_" + t + " \n else_if_"+ t + ": \n "+ this->noBlock1Else->print(escopos)+ " \n  end_if_"+t+": \n";

        //return "[if "+ this->noExpr->print(escopos) + this->noBlock1->print(escopos) +" " +this->noBlock1Else->print(escopos) + "]";
    };

};

class NoStmtIfs : public PrintStmtInterface{
public:
    PrintStmtIfElse *stmtIfElse;

    NoStmtIfs(NoStmtIf *stmtIfElse){
        this->stmtIfElse = stmtIfElse;        
    };
    
    NoStmtIfs(NoStmtIfElse *stmtIfElse){
        this->stmtIfElse = stmtIfElse;        
    };

    std::string print(Escopos *escopos){
       return this->stmtIfElse->print(escopos);
    };
};

class NoStmt : public PrintDecVar{
public:
    
    PrintStmtInterface *stmt;

    NoStmt(NoStmtAssign *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtBreak *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtContinue *stmt){
        this->stmt = stmt;
    };    

    NoStmt(NoStmtReturn *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtFuncall *stmt){
        this->stmt = stmt;
    };
    
    NoStmt(NoStmtWhile *stmt){
        this->stmt = stmt;
    };

    NoStmt(NoStmtIfs *stmt){
        this->stmt = stmt;
    };

    std::string print(Escopos *escopos){
        return this->stmt->print(escopos);
    };

};


inline std::string NoBlock2::print(Escopos *escopos){
  //std::cout << "print NoBlock" << std::endl;
  std::string arvore;
    //std::cout << "novooo " << this->noPrintStmt.size() << std::endl;
    for (int i = 0; i < this->noPrintStmt.size(); ++i)
    {            
        arvore = arvore + this->noPrintStmt.at(i)->print(escopos);
    }        
   // std::cout << "fimm do print NoBlock" << std::endl;
    return arvore;  
};


class NoDecFunc1 : public PrintDecVar{
public:
    NoId *noAtual;
    NoParamList *no1;
    NoBlock1 *no2;

    NoDecFunc1(NoId *noAtual, NoParamList *no1, NoBlock1 *no2){
        this->noAtual = noAtual;
        this->no1 = no1;    
        this->no2 = no2;
    };

    std::string declararMain(){
        std::string main = ".text main: \n sw $fp, 0($sp) \n addiu $sp, $sp, -4 \n jal _f_main \n li $v0, 10 \n syscall \n";
        return main;
    };
      
    std::string declararPrint(){
        std::string print = "_f_print: \n lw $a0, 4($sp) \n li $v0, 1 \n syscall \n li $v0, 11 \n li $a0, 0x0a \n syscall \n addiu $sp, $sp, 4 \n lw $fp, 4($sp) \n addiu $sp, $sp, 4 \n j $ra \n";
        return print;        
    };

    std::string print(Escopos *escopos){
        //std::cout<< "print da funcao" << std::endl;
        std::string arvore;
        std::string nomeF = " _f_";
        std::string nomeFS = "_f_";
        escopos->addEscopo();
        if(this->noAtual->id == "main"){
            arvore = declararMain();
            arvore = declararPrint() + arvore;
        }else{
            arvore = ".text";
        }
        nomeF = nomeF + this->noAtual->printId(escopos) + ": \n";
        nomeFS = "end_" +nomeFS + this->noAtual->printId(escopos) ;
        escopos->setNomeFs(nomeFS);
        if(this->no2 != 0){
            //arvore = "[decfunc " + this->noAtual->printId(escopos) + " "+this->no1->print(escopos) + this->no2->print(escopos) + "]";
            int z = 4*this->no1->ids.size() + 8;
            std::ostringstream ss;
            ss << z;
            //escopos->addEscopo();   
            std::cout << "nomeF =" << nomeF << " id atual = " << escopos->getEscopoAtual() << " novo " << escopos->getEscopoAtual() - 3 << std::endl;
            std::string part2 = "($sp) \n addiu $sp, $sp ,4 \n lw $fp,";
            std::string part2_1 = "($sp) \n addiu $sp,$sp,";
            //int t = escopos->getEscopoAtual();
            arvore = arvore + nomeF + " move $fp, $sp \n sw $ra, 0 ($sp) \n addiu $sp, $sp, -4 \n " + this->no2->print(escopos) + " "+ this->no1->print(escopos) + " \n "+ nomeFS+": \n  lw $ra," ;  
            std::string t = escopos->qtdVariaveisDeclaradas(escopos->getEscopoAtual() - 1);
            std::string part5 = "";
            std::string part3 = escopos->somaFP(escopos->getEscopoAtual() - 1) +" \n j $ra \n";
            std::cout << "aquii #3  escopos->qtdFP(escopos->getEscopoAtual() - 1) " << escopos->qtdFP(escopos->getEscopoAtual() - 1) << std::endl;
            arvore = arvore + t + part2 + escopos->qtdFP(escopos->getEscopoAtual() - 1) + part2_1+ part3;
            //std::cout << "|| teste|| "<<" v >>" << escopos->getEscopoAtual() << " qtd " << escopos->qtdVariaveisDeclaradas(escopos->getEscopoAtual() - 1) << std::endl;
            ss.clear();
            
            //arvore = "[decfunc " + this->noAtual->printId(escopos) + " "+this->no1->print(escopos) + this->no2->print(escopos) + "]";
        }else{
            arvore = arvore +  nomeF + ": \n move $fp, $sp \n sw $ra, 0 ($sp) \n addiu $sp, $sp, -4  \n " + this->no2->print(escopos) + " zzzzzzz "+ this->no1->print(escopos) + " addiu $sp, $sp, 8 \n lw $fp , 0($sp) \n j $ra \n" ; //talvez seja preciso ajustar aqui
            //arvore = "[decfunc " + this->noAtual->printId(escopos) + " " +this->no1->print(escopos) + "]";
        }
        //escopos->removerEscopo();
        return arvore;
    };

};


class NoDecFunc : public Node , public PrintDecvarDecFuncInterface
{
public:    
    
    PrintDecVar *decFunc;

    NoDecFunc(NoDecFunc1 *decFunc){        
        this->decFunc = decFunc; 
    };    
    
    std::string printDecvarDecFunc(Escopos *escopos){        
        std::cout << "aquii print da funcao" << std::endl;
        
        return this->decFunc->print(escopos);        
    };

};


class NoVarNoFunc : public Node{
public:
    std::vector<PrintDecvarDecFuncInterface *> decVarDecFunc;
    NoVarNoFunc(NoVar *noAtual){
        this->decVarDecFunc.push_back(noAtual);
    };

    NoVarNoFunc(){        
    };

    NoVarNoFunc(NoDecFunc *noAtual){
        this->decVarDecFunc.push_back(noAtual);
    };

    std::string print(Escopos *escopos){        
        std::cout << "aquii no print" << std::endl;
        std::string text = "";
        std::string data = "";
        std::string arvore;
        std::string teste;
        //escopos->addEscopo();
        for (int i = 0; i < this->decVarDecFunc.size(); ++i)
        {
            std::cout << "aquii no print loop: "<< this->decVarDecFunc.at(i) << std::endl;
            teste = this->decVarDecFunc.at(i)->printDecvarDecFunc(escopos);    
            std::cout << "aquii no print antes loop" << std::endl;
            std::size_t found = teste.find(".text");
            if(found!=std::string::npos){
               teste.replace(found,6,""); 
               text = text + teste;
            }else{
               data = data + teste; 
            }
            //arvore = arvore + this->decVarDecFunc.at(i)->printDecvarDecFunc(escopos);
            std::cout << "aquii no print loop final" << std::endl;
        }        
        return data + ".text \n" + text;
    };
};

class NoProgram : public Node{
public:
    NoVarNoFunc *noAtual;
    NoProgram(NoVarNoFunc *noAtual){
        this->noAtual = noAtual;
    };
    
    std::string printProgram(){        
        Escopos escopos;
        escopos.addEscopo();
        //std::cout << "print program" << std::endl;
        
        std::cout << "program" << std::endl;

        //std::string arvore = "[program " + this->noAtual->print(&escopos) + "]";
        std::string arvore = ".data \n " + this->noAtual->print(&escopos);   
        std::cout << "program final" << std::endl;        
        /*        
        if(escopos.declaradaNoEscopo("main",0)){
            std::cout << "foi declarado" << std::endl;
        }else{
            std::cout << "Não foi declarado" << std::endl;
        }
        */
        //aquiiiii
        //std::cout << " final print program" << std::endl;
        return arvore;
 