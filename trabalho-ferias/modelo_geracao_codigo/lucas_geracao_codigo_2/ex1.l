/********************************************************
 * ex1.l 
 ********************************************************/
%{
#include <iostream>
#include <string>	
#include <fstream>
#include <sstream>

#include "node2.h"
#include "ex1.tab.hpp"
#include "PrintAst.h"
using namespace std;
string teste = "";
string sym = "SYM";
string decimal = "DEC";
string key = "KEY";
string id = "ID";
string linha = "1";
int qtdLinha = 1;
std::ostringstream out;
%}

%option noyywrap

letra  [A-Za-z] 
DECIMAL [0-9][0-9]*
ID     ({letra})+({letra}|({DECIMAL}|_))*
COMENTARIO "//".*"\n"    

%%
"+"      {  return T_MAIS; }
"-"       { return T_MENOS; } 
"*"      {  return T_VEZES; }
"/"      {  return T_DIVISAO; }
"("      {  return T_ESQ_PAREN;  }
")"      {  return T_DIR_PAREN;  }
";"      {  return T_PONTO_VIRGULA;  }
">"      {  return T_MAIOR;  }
">="     {  return T_MAIOR_IGUAL;  }
"<"      {  return T_MENOR;  }
"<="     {  return T_MENOR_IGUAL;  }
"=="     {  return T_IGUAL_IGUAL;  }
"!="     {  return T_DIFERENTE_IGUAL;  }
"&&"     {  return T_E;  }
"||"     {  return T_OU;  }
"!"      {  return T_DIFERENCA;  }
"{"      {  return T_CHAVE_ABERTO;  }
"}"      {  return T_CHAVE_FECHADA;  }
"["      {  return T_COLCHETE_ABERTO;  }
"]"      {  return T_COLCHETE_FECHADO;  }
","      {  return T_VIRGULA;  }
"="      {  return T_IGUAL;  }
"def"     {  return T_FUNCTION;  }
"if"     {  return T_IF;  }
"else"    {  return T_ELSE;  }
"while"    {  return T_WHILE;  }
"return"    {  return T_RETURN;  }
"break"    {  return T_BREAK;  }
"continue"    {  return T_CONTINUE;  }
"let"   {  return T_VAR;  }
{ID} { yylval.string = new std::string(yytext, yyleng); return T_ID; }
{DECIMAL} { yylval.string = new std::string(yytext, yyleng); return T_DECIMAL;  }
{COMENTARIO} {qtdLinha++; out << qtdLinha; linha = out.str(); out.str("");}
\n  { qtdLinha++; out << qtdLinha; linha = out.str(); out.str(""); }
[ \t\n] {}
. { teste = teste + "ERROR"+" \"" + string(yytext) +"\" " +linha + "\n";  return 0; }
%%


/*
%%
"+"      {  teste = teste + sym +" \""+string(yytext)+"\"  "+linha+"\n"; }
"-"    {  teste = teste + sym +" \"" + string(yytext)+"\" "+linha+"\n"; } 
"*"      {  teste = teste + sym +" \"" + string(yytext)+"\" "+linha+"\n"; }
"/"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n"; }
"("      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
")"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
";"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
">"      {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
">="     {  teste = teste + sym  +" \""+ string(yytext)+"\" "+linha+"\n";  }
"<"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"<="     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"=="     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"!="     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"&&"     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"||"     {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"!"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"{"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"}"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"["      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"]"      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
","      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"="      {  teste = teste + sym  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"def"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"if"     {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"else"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"while"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+"\n";  }
"return"    {  teste = teste + key  +"  \""+ string(yytext)+"\" "+linha+ "\n";  }
"break"    {  teste = teste + key  +"  \""+ string(yytext)+"\" " +linha+"\n";  }
"continue"    {  teste = teste + key  +" \""+ string(yytext)+"\" " +linha+ "\n";  }
"let"   {  teste = teste + key  +"  \""+ string(yytext)+"\" " +linha+ "\n";  }
{ID} {  teste = teste + id  +"  \""+ string(yytext)+"\" " +linha+ "\n";  }
{DECIMAL} {  teste = teste + decimal  +"  \""+ string(yytext)+"\"  " +linha+"\n";  }
{COMENTARIO} { qtdLinha++; out << qtdLinha; linha = out.str(); out.str(""); }
\n  { qtdLinha++; out << qtdLinha; linha = out.str(); out.str(""); }
[ \t\n] {}
.        { teste = teste + "ERROR"+" \"" + string(yytext) +"\" " +linha + "\n";  return 0; }
%%
*/
/*
%%
"+"      {  return T_MAIS; }
"-"		   { return T_MENOS; }	
"*"      {  return T_VEZES; }
"/"      {  return T_DIVISAO; }
"("      {  return T_ESQ_PAREN;  }
")"      {  return T_DIR_PAREN;  }
";"      {  return T_PONTO_VIRGULA;  }
">"      {  return T_MAIOR;  }
">="     {  return T_MAIOR_IGUAL;  }
"<"      {  return T_MENOR;  }
"<="     {  return T_MENOR_IGUAL;  }
"=="     {  return T_IGUAL_IGUAL;  }
"!="     {  return T_DIFERENTE_IGUAL;  }
"&&"     {  return T_E;  }
"||"     {  return T_OU;  }
"!"      {  return T_DIFERENCA;  }
"{"      {  return T_CHAVE_ABERTO;  }
"}"      {  return T_CHAVE_FECHADA;  }
"["      {  return T_COLCHETE_ABERTO;  }
"]"      {  return T_COLCHETE_FECHADO;  }
","      {  return T_VIRGULA;  }
"="      {  return T_IGUAL;  }
"function"    {  return T_FUNCTION;  }
"if"     {  return T_IF;  }
"else"    {  return T_ELSE;  }
"while"    {  return T_WHILE;  }
"return"    {  return T_RETURN;  }
"break"    {  return T_BREAK;  }
"continue"    {  return T_CONTINUE;  }
"var"   {  return T_VAR;  }
{ID} { yylval.string = new std::string(yytext, yyleng); return T_ID; }
{DECIMAL} { yylval.string = new std::string(yytext, yyleng); return T_DECIMAL;  }
{COMENTARIO} {}
<<EOF>>  { return 0; }
[ \t\n]+ { }
.        { cerr << "token não reconhecido" << endl; exit(1); }
%%
*/