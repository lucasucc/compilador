#include <iostream>
#include <vector>
//#include "node2.h"
//#include <llvm/Value.h>

class NoExpr : public Node{
public:
    NoExpr1 *noExpr1;
    NoExpr2 *noExpr2;

    NoExpr(NoExpr1 *noExpr1){        
        this->noExpr1 = noExpr1;        
        this->noExpr2 = 0;
    };

    NoExpr(NoExpr2 *noExpr2){
        this->noExpr1 = 0;
        this->noExpr2 = noExpr2;        
    };

    std::string print(){
        if(this->noExpr1 == 0 && this->noExpr2 != 0){
            return this->noExpr2->print();
        }else if(this->noExpr1 != 0 && this->noExpr2 == 0){
            return this->noExpr1->print();
        }   
    };
};
