%{
#include <stdio.h>
#include <string.h>
extern FILE *yyin;
extern FILE *yyout;
char id_global[10][100];
char id_local[10][100];
char def_func[10][100];
char temp[20], id[50][100];
char temp2[20];
char temp3[20];
char temp4[20];
int i=0,a=0,b=0;
int j=0,dec[10];
int x,y;
int cont,cont2,cont3,cont4,cont5,cont6,cont7,cont8,cont9,cont10,cont11,cont12, cont13, cont14;

%}

// Symbols.
%union
{
  char *sval;
  int integer;
};
%token IF	
%token ELSE
%token WHILE
%token RETURN
%token BREAK
%token CONTINUE
%token LET 
%token VOID
%token <sval> ID
%token <integer> DEC
%token DEF
%token ATRIB
%token SOMA
%token SUBTRAC
%token MULTIPL
%token DIVISAO
%token MAIOR
%token MENOR
%token MAIORIGUAL
%token MENORIGUAL
%token IGUAL
%token DIFERENTE
%token END
%token OR
%token NEGACAO
%token VIRG
%token PVIRG
%token BLOCK
%token ENDBLOCK
%token ACOCH
%token FCOCH
%token APAR
%token FPAR
%type <sval> or end expression precedence value logica neg igual_desiguald expressao line funcCall neg_log
%start inicio
%%

inicio:


	{fprintf(yyout,"[program "); }
	| inicio {for (x = 0; x < 10; x++){
                for (y = 0; id_local[x][y]; y++)
					strcpy(id_local[x],"");
			 }
			} decvar_global 
	;
//------------------------------------------------------------------------------------------------------------------------------------------------
decvar_global:
	LET ID ATRIB {fprintf(yyout,"[decvar "); } {fprintf(yyout,"[%s]", $2); strcpy(id_global[i],$2);i++;} expressao PVIRG {fprintf(yyout,"]"); }
	| LET ID {fprintf(yyout,"[decvar "); } {fprintf(yyout,"[%s]", $2); strcpy(id_global[i],$2);i++;} PVIRG {fprintf(yyout,"]"); }
	| DEF decfunc
	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
decvar_local:
	LET ID ATRIB {fprintf(yyout,"[decvar "); } {fprintf(yyout,"[%s]", $2); strcpy(id_local[j],$2);j++;} expressao PVIRG {fprintf(yyout,"]"); }
	| LET ID {fprintf(yyout,"[decvar "); } {fprintf(yyout,"[%s]", $2); strcpy(id_local[j],$2);j++;} PVIRG {fprintf(yyout,"]"); }
	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
decfunc:
	type ID APAR {fprintf(yyout,"[decfunc [%s] ", $2);} paramList FPAR {fprintf(yyout,"] "); }
	BLOCK {fprintf(yyout,"[block "); } bloco {fprintf(yyout,"]"); } ENDBLOCK {fprintf(yyout,"]"); }
;
//-------------------------------------------------------------------------------------------------------------------------------------------------
type:
	
	| VOID
	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
paramList:
	{fprintf(yyout,"[paramlist"); }
	| paramList quant_paramList
	;
quant_paramList:
	 ID {fprintf(yyout,"[%s]", $1); }
	| expressao
	| DEC {fprintf(yyout,"[%d]", $1); }
	| VIRG

	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
bloco:
	| decvar_local bloco 
	| stmt bloco
;
//-------------------------------------------------------------------------------------------------------------------------------------------------
stmt:
	assign
	| funccall PVIRG
	| condicional_if
	| repeticao
	| retorna 
	| BREAK {fprintf(yyout,"[break ]"); }PVIRG
	| CONTINUE {fprintf(yyout,"[continue ]"); }PVIRG
	;
condicional_if:
	IF {fprintf(yyout,"[if "); } APAR expressao FPAR
	     BLOCK {fprintf(yyout,"[block "); } bloco ENDBLOCK {fprintf(yyout,"]"); } condicional_else {fprintf(yyout,"]");}
	;

condicional_else:
	| ELSE BLOCK {fprintf(yyout,"[block "); }bloco 
		ENDBLOCK {fprintf(yyout,"]"); }	
	;
repeticao:
	WHILE {fprintf(yyout,"[while "); }APAR expressao FPAR 
	BLOCK {fprintf(yyout,"[block "); } bloco ENDBLOCK {fprintf(yyout,"]"); }{fprintf(yyout,"]"); }
	;
retorna:
	RETURN PVIRG {fprintf(yyout,"[return ]"); }
	| RETURN {fprintf(yyout,"[return "); } expressao PVIRG {fprintf(yyout,"]"); } 
	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
assign:
    ID {strcpy(temp,$1);int z,w=0,q=0;
		for (x=0; x<10; x++){
    		if(strcmp($1, id_local[x]))
				continue;
			else{
				w=1;
				break;
			}
		}
		for(z=0; z<10; z++){
			if(strcmp($1, id_global[z]))
				continue;
			else{
				q=1;
				break;			
			}
		}
		if((w==0)&&(q==0)){
			yyerror();
			return;
		}  
   	}
	ATRIB {fprintf(yyout,"[assign [%s]", temp);} expressao PVIRG {fprintf(yyout,"]");}
	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
funccall:
	ID APAR {fprintf(yyout,"[funccall [%s]", $1); } argList FPAR {fprintf(yyout,"]"); }{fprintf(yyout,"]"); }
	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
argList:
	{fprintf(yyout,"[arglist "); }
	| argList quant_argList
	;
quant_argList:
	ID {fprintf(yyout,"[%s]", $1); }
	| expressao
	| DEC {fprintf(yyout,"[%d]", $1); }
	| VIRG
	
	;
//-------------------------------------------------------------------------------------------------------------------------------------------------
expressao:
	line
	| funccall
	;
line: or resultado
		;
or: end													  			 	 {$$ = $1;}
		| or OR end								 				     {strcpy(id[a],"||");a++;}
		;
end: igual_desiguald						  			 	 {$$ = $1;}
		| igual_desiguald END end 				     {strcpy(id[a],"&&");a++;}
		;
logica: expression						  {$$ = $1;}
		| expression MAIOR logica {strcpy(id[a],">");a++;}
		| expression MENOR logica {strcpy(id[a],"<");a++;}
		| expression MAIORIGUAL logica {strcpy(id[a],">=");a++;}
		| expression MENORIGUAL logica {strcpy(id[a],"<=");a++;}
		;

igual_desiguald: logica                                                                  {$$ = $1;}
                 | igual_desiguald IGUAL logica                           {strcpy(id[a],"==");a++;}
		 | igual_desiguald DIFERENTE logica               {strcpy(id[a],"!=");a++;}
                 ;

expression: precedence				{$$ = $1;}
		| precedence SOMA expression {strcpy(id[a],"+");a++;}
		| precedence SUBTRAC expression {strcpy(id[a],"-");a++;}
		;


value		: ID {strcpy(id[a],$1);a++;}				{$$ = $1;}
		| DEC {sprintf(temp, "%d", $1); strcpy(id[a],temp);a++;}
		| APAR or FPAR	{$$ = $2;}
		;


precedence	: neg_log				   {$$ = $1;}
		| neg_log MULTIPL precedence {strcpy(id[a],"*");a++;}	
		| neg_log DIVISAO precedence {strcpy(id[a],"/");a++;}
		;
neg_log		: neg					{$$ = $1;}
		| NEGACAO neg{strcpy(id[a],"!");a++;}
		;
neg		: value						{$$ = $1;}
		| SUBTRAC value {strcpy(id[a],"-");a++;}	
		;
resultado:




{int tam = a; cont=0; cont2=0, cont3=0, cont4=0,cont5=0, cont6=0,cont7=0,cont8=0,cont9=0, cont10=0, cont11=0, cont12=0, cont13=0, cont14=0;
	 for ( x = tam-1; x >= 0; x--,tam--){
	//fprintf(yyout,"%s",id[x]);
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
		if (!strcmp(id[x],"+")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
			    cont4=1;
			}
		if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(cont4==1)&&(strcmp(id[x-2],">"))&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
			   	fprintf(yyout,"[+[%s][%s]]",id[x-1],id[x-2]);
				tam=tam-2; x=tam-1;cont4=0;
			}
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
		else if (!strcmp(id[x],"-")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
			    cont5=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"\0"))&&(cont5==1)&&(strcmp(id[x-2],">"))&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
			   	fprintf(yyout,"[-[%s][%s]]", id[x-2], id[x-1]|| "[-[%s]",id[x-1]);
				tam=tam-2; x=tam-1;cont5=0;
			}

			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
			
	}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
		else if (!strcmp(id[x],"*")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont2=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(cont2==1)&&(strcmp(id[x-2],">"))&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
		    	fprintf(yyout,"[*[%s][%s]]",id[x-2],id[x-1]);
				tam=tam-2; x=tam-1;cont2=0;			
			}				
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
		else if (!strcmp(id[x],"/")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont3=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(cont3==1)&&(strcmp(id[x-2],">"))&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
		    	fprintf(yyout,"[/[%s][%s]]",id[x-2],id[x-1]);
				tam=tam-2; x=tam-1;cont3=0;			
			}				
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
else if (!strcmp(id[x],">")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont6=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"))&&(cont6==1)&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
		    	fprintf(yyout,"[>[%s][%s]]",id[x-2],id[x-1]);
				tam=tam-2; x=tam-1;cont6=0;	
			}				
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
else if (!strcmp(id[x],"<")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont7=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"))&&(cont7==1)&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
		    	fprintf(yyout,"[<[%s][%s]]",id[x-2],id[x-1]);
				tam=tam-2; x=tam-1;cont7=0;	
			}				
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
	else if (!strcmp(id[x],">=")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont8=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"))&&(cont8==1)&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
		    	fprintf(yyout,"[>=[%s][%s]]",id[x-2],id[x-1]);
				tam=tam-2; x=tam-1;cont8=0;	
			}				
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
else if (!strcmp(id[x],"<=")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont9=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"))&&(cont9==1)&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
				cont14=1;
				}
			if((cont14==1)&&(!strcmp(id[x-3],"!"))){
		    	fprintf(yyout,"[![<=[%s][%s]]]",id[x-2],id[x-1]);
				tam=tam-3; x=tam-1;cont9=0; cont14=0;	
			}
			else{
				if	((cont9==1)&&(cont14==1)){
					fprintf(yyout,"[<=[%s][%s]]",id[x-2],id[x-1]);
					tam=tam-2; x=tam-1;cont9=0;cont14=0;	
				}			
				else {
					fprintf(yyout,"[%s",id[x]);
					cont++;
				}
			}
	}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
else if (!strcmp(id[x],"==")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont10=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"))&&(cont10==1)&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
		    	fprintf(yyout,"[==[%s][%s]]",id[x-2],id[x-1]);
				tam=tam-2; x=tam-1;cont10=0;
			}				
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
else if (!strcmp(id[x],"!=")){
			if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1],"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
				cont11=1;
			}
			if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"))&&(cont11==1)&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
		    	fprintf(yyout,"[!=[%s][%s]]",id[x-2],id[x-1]);
				tam=tam-2; x=tam-1;cont11=0;
			}				
			else {
				fprintf(yyout,"[%s",id[x]);
				cont++;
			}
		}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
else if (!strcmp(id[x],"!")){
                         if((strcmp(id[x-1],"-"))&&(strcmp(id[x-1],"*"))&&(strcmp(id[x-1],"+"))&&(strcmp(id[x-1],"/"))&&(strcmp(id[x-1],">"    ))&&(strcmp(id[x-1],"<"))&&(strcmp(id[x-1],">="))&&(strcmp(id[x-1],"<="))&&(strcmp(id[x-1],"=="))&&(strcmp(id[x-1],"!="))&&(strcmp(id[x-1]    ,"&&"))&&(strcmp(id[x-1],"||"))&&(strcmp(id[x-1],"!"))){
                                 cont12=1;
                         }
                         if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"    ))&&(cont12==1)&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
                         fprintf(yyout,"[![%s]]",id[x-1]);
                                 tam=tam-2; x=tam-1;cont11=0;
                         }
                         else {
                                 fprintf(yyout,"[%s",id[x]);
                                 cont++;
                         }
                 }
//----------------------------------------------------------------------------------------------------------------------------------------






else if (!strcmp(id[x],"&&")){
	fprintf(yyout,"[%s",id[x]);
	cont++;
}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
	else {
			if ((!strcmp(id[x-1],"-"))&&(!strcmp(id[x+1],"-"))){
				fprintf(yyout,"[%s]]",id[x-2]);
				cont13=1;
			}
			else if (!strcmp(id[x-1],"-")){
				fprintf(yyout,"[-[%s]]",id[x-2]);
				cont12=1;
			}
			else if (!strcmp(id[x+1],"==")){
				if (!strcmp(id[x-1],"/")){
					if((strcmp(id[x-2],"-"))&&(strcmp(id[x-2],"*"))&&(strcmp(id[x-2],"+"))&&(strcmp(id[x-2],"/"))&&(strcmp(id[x-2],">"))&&(strcmp(id[x-2],"<"))&&(strcmp(id[x-2],">="))&&(strcmp(id[x-2],"<="))&&(strcmp(id[x-2],"=="))&&(strcmp(id[x-2],"!="))&&(strcmp(id[x-2],"&&"))&&(strcmp(id[x-2],"||"))&&(strcmp(id[x-2],"!"))){
				cont3=1;
					}
					if((strcmp(id[x-3],"-"))&&(strcmp(id[x-3],"*"))&&(strcmp(id[x-3],"+"))&&(strcmp(id[x-3],"/"))&&(cont3==1)&&(strcmp(id[x-3],">"))&&(strcmp(id[x-3],"<"))&&(strcmp(id[x-3],">="))&&(strcmp(id[x-3],"<="))&&(strcmp(id[x-3],"=="))&&(strcmp(id[x-3],"!="))&&(strcmp(id[x-3],"&&"))&&(strcmp(id[x-3],"||"))&&(strcmp(id[x-3],"!"))){
		    	fprintf(yyout,"[/[%s][%s]][%s]",id[x-3],id[x-2],id[x]);
				tam=tam-3; x=tam-1;cont3=0;			
					}				
					else {fprintf(yyout,"[%s",id[x]);cont++;}
				}
			}
	     	else{fprintf(yyout,"[%s]",id[x]);}
			if ((cont>0)&&(strcmp(id[x+1],"-"))&&(strcmp(id[x+1],"*"))&&(strcmp(id[x+1],"+"))&&(strcmp(id[x+1],"/"))&&(strcmp(id[x+1],">"))&&(strcmp(id[x+1],"<"))&&(strcmp(id[x+1],">="))&&(strcmp(id[x+1],"<="))&&(strcmp(id[x+1],"=="))&&(strcmp(id[x+1],"!="))&&(strcmp(id[x+1],"&&"))&&(strcmp(id[x+1],"||"))){
				fprintf(yyout,"]");
				cont--;
			}
	}
/**/
}
	for (x=0; x<cont; x++)
		fprintf(yyout,"]");
	a=0;
}
;
%%
int yyerror(char *s) {
  printf("yyerror : %s\n",s);
}

int main(int argc, char** argv){

	yyin = fopen(argv[1], "r");
	yyout = fopen(argv[2], "w");

	if(!yyin) printf("Arquivo nao pode ser aberto!\n");
	else{
		yyparse();
		fprintf(yyout,"]\n");
	}
	fclose(yyin);
	fclose(yyout);
	return 0;
}
