%{
#include "Parser.h"
%}

blanks		[ \t\n]+
identifier	[_a-zA-Z0-9]+
number		[0-9]+


%%
{blanks}	{ /* ignore */ }
"def"		return(DEF);
"if"		return(IF);	
"else"		return(ELSE);
"while"		return(WHILE);
"return"	return(RETURN);
"break"		return(BREAK);
"continue"	return(CONTINUE);
"let"		return(LET);
"void"		return(VOID);
"="		return(ATRIB);
"+"		return(SOMA);
"-"		return(SUBTRAC);
"*"		return(MULTIPL);
"/"		return(DIVISAO);
"<"		return(MENOR);
">"		return(MAIOR);
"<="		return(MENORIGUAL);
">="		return(MAIORIGUAL);
"=="		return(IGUAL);
"!="		return(DIFERENTE);
"&&"		return(END);
"||"		return(OR);
"!"		return(NEGACAO);
","		return(VIRG);
";"		return(PVIRG);
"{"		return(BLOCK);
"}"		return(ENDBLOCK);
"["		return(ACOCH);
"]"		return(FCOCH);
"("		return(APAR);
")"		return(FPAR);
{number} {
	yylval.integer = atoi(yytext);
	return DEC;
}
{identifier} {
	yylval.sval = malloc(strlen(yytext));
	strncpy(yylval.sval, yytext, strlen(yytext));
	return(ID);
}
