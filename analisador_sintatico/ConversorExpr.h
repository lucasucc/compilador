#ifndef CONVERSOREXPR_H_INCLUDED
#define CONVERSOREXPR_H_INCLUDED
#include <vector>
#include <iostream>
#include <string>
#include <list>
#include <stack>
#include "NoExpr.h"

class ConversorExpr{

	private:
		stack<NoExpr> pilhaExpressoes;
		//std::list<NoExpr> listaExpressoes;

	public:

		ConversorExpr(){
			
		};
		
		
		std::string executar(std::vector<NoExpr> expressoes){
			expressoes = prefixa(expressoes);
			string expr;
			//cout << "executar expressoes.size(): " << expressoes.size() << endl;
			std::reverse(expressoes.begin(),expressoes.end());
			printarExpressaoPrefixadaSem(expressoes);
			expressoes = adicionarColchetes(expressoes);	
			//cout << "add colchetes" << endl;
			expr = printarExpressaoPrefixada(expressoes);
			expressoes.clear();
			return expr;
		};

		/*

		void prefixoSimbolos(){
	         stack<NoExpr> pilhaOPerandos;
	         stack<NoExpr> pilhaOperadores;
	         stack<NoExpr> pilhaAux,pilhaAux2,pilhaAux3;
	         NoExpr noExpr;
	         NoExpr noExprCocheteFechado("]","novo",-1);
	         NoExpr noExprCocheteAberto("[","novo",-1);
	         NoExpr noAux;
	         bool fecharColchete = false;
	         
	         for (int i = 0; i < this->listaExpressoes.size(); ++i)
	         {
	            noExpr = this->listaExpressoes.at(i);
	            if(noExpr.getTipo() == "sinal"){
	               if(pilhaOperadores.empty()){
	                  pilhaOperadores.push(noExpr);
	               }else if( pilhaOperadores.top().getValor() == "+" && noExpr.getValor() == "+" ){                  
	                  pilhaOPerandos.push(noExpr);
	                  /*
	                  pilhaOPerandos.push(noExprCocheteFechado);
	                  while(!pilhaOPerandos.empty()){
	                     pilhaAux3.push(pilhaOPerandos.top());
	                     pilhaOPerandos.pop();
	                  }
	                  
	                  pilhaAux3.push(noExpr);
	                  pilhaAux3.push(noExprCocheteAberto);
	                  while(!pilhaAux3.empty()){
	                     pilhaOPerandos.push(pilhaAux3.top());
	                     pilhaAux3.pop();
	                  }
	                  */
	                  //pilhaOPerandos = pilhaAux3;                 
	      /*
	               }else if(pilhaOperadores.top().getValor() == "+" && noExpr.getValor() == "*"){
	                  
	                  pilhaOPerandos.push(noExpr);
	                  /*
	                  fecharColchete = true;

	                  noAux = pilhaOPerandos.top();
	                  pilhaOPerandos.pop();
	                  
	                  while(!pilhaOPerandos.empty()){
	                     pilhaAux3.push(pilhaOPerandos.top());
	                     pilhaOPerandos.pop();
	                  }

	                  pilhaOPerandos.push(noExprCocheteAberto);
	                  pilhaOPerandos.push(noExpr);                  
	                  pilhaOPerandos.push(noAux);
	                  pilhaOPerandos.push(this->listaExpressoes.at(i+1));
	                  pilhaOPerandos.push(noExprCocheteFechado);
	                  i++;

	                  
	                  while(!pilhaAux3.empty()){
	                     pilhaOPerandos.push(pilhaAux3.top());
	                     pilhaAux3.pop();
	                  }
	                  */
	                  /*
	                  pilhaAux3.push(noExpr);
	                  
	                  while(!pilhaAux3.empty()){
	                     pilhaOPerandos.push(pilhaAux3.top());
	                     pilhaAux3.pop();
	                  }                  
	                  */   
	        /*       }                                   
	            }else{               
	               pilhaOPerandos.push(noExpr);
	               if(fecharColchete == true){
	                  //pilhaOPerandos.push(noExprCocheteFechado);
	                  fecharColchete = false;
	               }
	            }
	            
	         }
	      
	         pilhaOPerandos.push(noExprCocheteFechado);
	         pilhaAux.push(noExprCocheteAberto);
	         while(!pilhaOperadores.empty()){
	            pilhaAux.push(pilhaOperadores.top());
	            pilhaOperadores.pop();
	         }

	         
	         //inverter pilha
	         while(!pilhaOPerandos.empty()){
	            pilhaAux2.push(pilhaOPerandos.top());   
	            pilhaOPerandos.pop();
	         }



	         while(!pilhaAux2.empty()){
	            pilhaAux.push(pilhaAux2.top());            
	            pilhaAux2.pop();
	         }

	         

	         //printPilha(pilhaAux);
      };
		*/

		std::vector<NoExpr> adicionarColchetes(std::vector<NoExpr> expressoes){
			std::vector<NoExpr> aux;
			std::vector<NoExpr> aux2;
			NoExpr noExprCocheteFechado("]","novo",-1);
         	NoExpr noExprCocheteAberto("[","novo",-1);
			NoExpr noAnterior;
			int qtdNumerosLidos = 0;
			int qtdColchetesAberto = 0;
			bool primeiraExpressao = true;
			std::vector<int> vetor;
			int h;
			int qtdTotalDeColchetesAbertos = 0;
			int qtdTotalDeColchetesFechados = 0;
			//cout << "expressoes size aqui: " << expressoes.size() << endl;
			//cout << "tamanho aux " << aux.size() << endl;
			for (int i = 0; i < expressoes.size(); ++i)
			{
				if(operador(expressoes.at(i)) == true){
					//cout << "i " << i << endl;
					aux.push_back(noExprCocheteAberto);
					aux.push_back(expressoes.at(i));					
					qtdTotalDeColchetesAbertos++;
					
				}else{
					aux.push_back(expressoes.at(i));
				}
			}	
			//cout << "tamanho aux aqui fora " <<  aux.size() << endl; 
			expressoes.clear();
			expressoes = aux;
			//std::reverse(expressoes.begin(),expressoes.end());
			//cout << "posicao 0 " << expressoes.at(0).getValor() << endl;
			
			for (int i = expressoes.size() - 1 ; i > -1; --i)
			{
				//cout << "i:"<< i << " expressoes.at(i) " << expressoes.at(i).getValor() << endl;
				if(expressoes.at(i).getValor() == "["){
					h = i + 3;
					for(int j = i+1; j < expressoes.size(); ++j){
						//cout << "j:" << j << endl; 
						//cout << "h:" << h << endl;
						if(expressoes.at(j).getValor() == "["){
							qtdColchetesAberto++;
							if(j == h){
								h++;
							}
						}else if( std::find(vetor.begin(), vetor.end(), j) != vetor.end() ){ //testa se [ 
							qtdColchetesAberto--;
							if(j == h){
								h++;
							}
						}else if( j == h ){
							if(qtdColchetesAberto == 0){
								vetor.push_back(h);
								qtdTotalDeColchetesFechados++;
								break;	
							}else{
								h++;								
							}
						}
					}										
				}
			}

			aux.clear();
			//cout << "tamanho do vetor size " << vetor.size() << endl;
			for (int i = 0; i < expressoes.size(); ++i)
			{
				if( std::find(vetor.begin(), vetor.end(), i) != vetor.end() ){
					for (int j = 0; j < vetor.size(); ++j)
					{						
						if(vetor.at(j) == i){
							vetor.erase(vetor.begin() +  j);	
							break;
						}
						
					}
					
					aux.push_back(expressoes.at(i));
					aux.push_back(noExprCocheteFechado);
				}else{
					aux.push_back(expressoes.at(i));
				}
			}
			//cout << "posicoes do vetor: " << vetor.size() << endl;
			
			if( qtdTotalDeColchetesAbertos > qtdTotalDeColchetesFechados){
				for (int i = 0; i < qtdTotalDeColchetesAbertos - qtdTotalDeColchetesFechados ; ++i)
				{
					aux.push_back(noExprCocheteFechado);	
				}
			}
			
		/*
			if(aux.size() == 1){
				aux2.clear();
				aux2.push_back(noExprCocheteAberto);
				aux2.push_back(aux.at(0));
				aux2.push_back(noExprCocheteFechado);
				aux = aux2;
			}
	*/
			std::reverse(aux.begin(),aux.end());	

			for (int i = 0; i < aux.size(); ++i)
			{
				if(aux.at(i).getValor() == "%"){
					aux.at(i).setValor("-");
				}
			}

			return aux;
		};

		 
		std::string printarExpressaoPrefixada(std::vector<NoExpr> expressoes){
			std::string stringPrefixa = "";
			//cout << "size " << expressoes.size() << endl;
			for (int i = 0; i < expressoes.size(); ++i)
			{
				//cout << "alias: " << expressoes.at(i).getAlias() << endl;
				//cout << "expressoes.at(i).getValor() " << expressoes.at(i).getValor() << endl; 
				if(expressoes.at(i).getAlias() == "" && expressoes.at(i).getTipo() == "numero" ){
					stringPrefixa = "[" + expressoes.at(i).getValor() + "]" + stringPrefixa ; 
				}else if(expressoes.at(i).getTipo() == "sinal"){
					stringPrefixa = expressoes.at(i).getValor() + stringPrefixa ; 
				}else if(expressoes.at(i).getValor() == "]" || expressoes.at(i).getValor() == "["){
					stringPrefixa = expressoes.at(i).getValor() + stringPrefixa ; 
				}else{
					stringPrefixa = expressoes.at(i).getAlias() + stringPrefixa ;
				}
				//cout <<  expressoes.at(i).getValor() << endl;
			}
			
			cout << "expressao completa:" << stringPrefixa << endl;
			return stringPrefixa;

		};

		void printarExpressaoPrefixadaSem(std::vector<NoExpr> expressoes){
			std::string stringPrefixa = "";
			//cout << "size " << expressoes.size() << endl;
			for (int i = 0; i < expressoes.size(); ++i)
			{
				stringPrefixa =  stringPrefixa + expressoes.at(i).getValor()  ; 
				//cout <<  expressoes.at(i).getValor() << endl;
			}
			//cout << "expressao completa:" << stringPrefixa << endl;
		};

		//char infix = expressoes
		std::vector<NoExpr> prefixa(std::vector<NoExpr> expressoes){
			NoExpr numeroOuSinal;
			
	    	//this->pilhaExpressoes.push(fimDePilha); 
	    	std::vector<NoExpr> expressaoPrefixada;
	    	
	    	std::reverse(expressoes.begin(),expressoes.end());
	    	for (int i = 0; i < expressoes.size(); ++i)
	    	{	    	
	    		//cout << "teste1" << endl;
	    		numeroOuSinal = expressoes.at(i);
	    		//cout << "teste2" << endl;
	    		if(operador(numeroOuSinal) == false) {
	    			//cout << "teste3" << endl;
	    			expressaoPrefixada.push_back(numeroOuSinal); 	    			
	    		}else{
	    			//cout << "teste4" << endl;
	    			if(numeroOuSinal.getValor() == ")") {
	    				//cout << "teste5" << endl;
	    				this->pilhaExpressoes.push(numeroOuSinal);
	    				//cout << "teste6" << endl;
	    			}else if(numeroOuSinal.getValor() == "(") {
	    				
	    				while (this->pilhaExpressoes.top().getValor() != ")") {
	    					expressaoPrefixada.push_back(this->pilhaExpressoes.top());
	    					this->pilhaExpressoes.pop();
	    					//prefix[j]=pop();
	    					//j++;
	    				}
	    				this->pilhaExpressoes.pop();
	    				
	    			}else{
	    				//cout << "teste7" << endl;
	    				//if(this->pilhaExpressoes.empty()){
	    					//this->pilhaExpressoes.push(numeroOuSinal);	    					
	    				if(!this->pilhaExpressoes.empty() && precedenciaOperador( this->pilhaExpressoes.top() ) <= precedenciaOperador(numeroOuSinal)) {
	    					//cout << "teste8" << endl;
	    					this->pilhaExpressoes.push(numeroOuSinal);	    					
	    				}else{
	    					//cout << "teste9" << endl;
	    					while(!this->pilhaExpressoes.empty() && precedenciaOperador(this->pilhaExpressoes.top()) >= precedenciaOperador(numeroOuSinal)) {
	    						expressaoPrefixada.push_back(this->pilhaExpressoes.top());
	    						this->pilhaExpressoes.pop();
	    						//prefix[j]=pop();
	    						//j++;
	    					}
	    					this->pilhaExpressoes.push(numeroOuSinal);
	    					//push(numeroOuSinal);
	    				}
	    				
	    			}
	    		}
	    		
	    	}
	    	
	    	//cout << "pilha Espressoes" << this->pilhaExpressoes.top().getValor() << endl;
	    	//cout << "expressaoPrefixada.size() " << expressaoPrefixada.size() << endl ;
	    	while(!this->pilhaExpressoes.empty()) {
	    		//this->pilhaExpressoes.push(numeroOuSinal)
	    		expressaoPrefixada.push_back(this->pilhaExpressoes.top());
	    		this->pilhaExpressoes.pop();
	    		
	    	}
	    	
	    
	    	return expressaoPrefixada;
	    };		


	    bool operador(NoExpr no) {
	    	//cout <<"valor :"<<  no.getValor() << endl;
	    	if(no.getValor() == "+"){
				return true;
	    	}else if(no.getValor() == "*"){
				return true;
	    	}else if(no.getValor() == "-"){
				return true;
	    	}else if(no.getValor() == "/"){
				return true;
	    	}else if(no.getValor() == "&&"){
				return true;
	    	}else if(no.getValor() == "||"){
				return true;
	    	}else if(no.getValor() == "("){
				return true;
	    	}else if(no.getValor() == ")"){
				return true;
	    	}else if(no.getValor() == "<"){
				return true;
	    	}else if(no.getValor() == "<="){
				return true;
	    	}else if(no.getValor() == ">="){
				return true;
	    	}else if(no.getValor() == "=="){
				return true;
	    	}else if(no.getValor() == "!="){
				return true;
	    	}else if(no.getValor() == "!"){
	    		return true;
	    	}else if(no.getValor() == "%"){
	    		return true;
	    	}else{
	    		return false;
	    	}
	    	
    	};

    	int precedenciaOperador(NoExpr no){ // returns the value that helps in the precedence {
	    	if(no.getValor() == "+"){
				return 3;
	    	}else if(no.getValor() == "*"){
				return 4;
	    	}else if(no.getValor() == "-"){
				return 3;
	    	}else if(no.getValor() == "/"){
				return 4;
	    	}else if(no.getValor() == "&&"){
				return 7;
	    	}else if(no.getValor() == "||"){
				return 8;
	    	}else if(no.getValor() == "("){
				return 1;
	    	}else if(no.getValor() == ")"){
				return 1;
	    	}else if(no.getValor() == "<"){
				return 5;
	    	}else if(no.getValor() == "<="){
				return 5;
	    	}else if(no.getValor() == ">"){
				return 5;
	    	}else if(no.getValor() == ">="){
	    		return 5;
	    	}else if(no.getValor() == "=="){
				return 5;
	    	}else if(no.getValor() == "!="){
				return 6;
	    	}else if(no.getValor() == "!"){
	    		return 6;
	    	}else if(no.getValor() == "%"){
	    		return 2;
	    	}else{
	    		return 0;
	    	}
	    	
    };


    	
};
#endif // CONVERSOREXPR_H_INCLUDED