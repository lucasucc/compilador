#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "CBasicMath.hpp"
#include "../../Scanner.cpp"
#include "../../Token/Token.h"

using namespace CppUnit;
using namespace std;

//-----------------------------------------------------------------------------

class TestTeste30 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestTeste30);
    CPPUNIT_TEST(TestTeste30Lexico);
    //CPPUNIT_TEST(testMultiply);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void TestTeste30Lexico(void);    

private:
    
    Scanner *scanner;
};

//-----------------------------------------------------------------------------

void
TestTeste30::TestTeste30Lexico(void)
{
    const char* fe[2];
    fe[0] = "comando"; 
    fe[1] = "../../testes/teste30.lucas"; 
    scanner->executar(1,fe);
    vector<Token> t = scanner->mostrar();
    
    


    CPPUNIT_ASSERT(171 == t.size());
    CPPUNIT_ASSERT("KEY" == t.at(0).getTipoToken());
    CPPUNIT_ASSERT("def" == t.at(0).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(1).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(1).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(2).getTipoToken());
    CPPUNIT_ASSERT("add" == t.at(2).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(3).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(3).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(4).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(4).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(5).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(5).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(6).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(6).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(7).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(7).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(8).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(8).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(9).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(9).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(10).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(10).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(11).getTipoToken());
    CPPUNIT_ASSERT("return" == t.at(11).getLexema());

    
    CPPUNIT_ASSERT("ID" == t.at(12).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(12).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(13).getTipoToken());
    CPPUNIT_ASSERT("+" == t.at(13).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(14).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(14).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(15).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(15).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(16).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(16).getLexema());


    //sub
    CPPUNIT_ASSERT("KEY" == t.at(17).getTipoToken());
    CPPUNIT_ASSERT("def" == t.at(17).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(18).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(18).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(19).getTipoToken());
    CPPUNIT_ASSERT("dif" == t.at(19).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(20).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(20).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(21).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(21).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(22).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(22).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(23).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(23).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(24).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(24).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(25).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(25).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(26).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(26).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(27).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(27).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(28).getTipoToken());
    CPPUNIT_ASSERT("return" == t.at(28).getLexema());

    
    CPPUNIT_ASSERT("ID" == t.at(29).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(29).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(30).getTipoToken());
    CPPUNIT_ASSERT("-" == t.at(30).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(31).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(31).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(32).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(32).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(33).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(33).getLexema());

    //div

    CPPUNIT_ASSERT("KEY" == t.at(34).getTipoToken());
    CPPUNIT_ASSERT("def" == t.at(34).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(35).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(35).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(36).getTipoToken());
    CPPUNIT_ASSERT("div" == t.at(36).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(37).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(37).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(38).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(38).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(39).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(39).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(40).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(40).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(41).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(41).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(42).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(42).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(43).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(43).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(44).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(44).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(45).getTipoToken());
    CPPUNIT_ASSERT("return" == t.at(45).getLexema());

    
    CPPUNIT_ASSERT("ID" == t.at(46).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(46).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(47).getTipoToken());
    CPPUNIT_ASSERT("/" == t.at(47).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(48).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(48).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(49).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(49).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(50).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(50).getLexema());    

    //mul

    CPPUNIT_ASSERT("KEY" == t.at(51).getTipoToken());
    CPPUNIT_ASSERT("def" == t.at(51).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(52).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(52).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(53).getTipoToken());
    CPPUNIT_ASSERT("mul" == t.at(53).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(54).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(54).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(55).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(55).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(56).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(56).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(57).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(57).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(58).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(58).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(59).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(59).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(60).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(60).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(61).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(61).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(62).getTipoToken());
    CPPUNIT_ASSERT("return" == t.at(62).getLexema());

    
    CPPUNIT_ASSERT("ID" == t.at(63).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(63).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(64).getTipoToken());
    CPPUNIT_ASSERT("*" == t.at(64).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(65).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(65).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(66).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(66).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(67).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(67).getLexema());
    //int i = 67
    //main
    CPPUNIT_ASSERT("KEY" == t.at(68).getTipoToken());
    CPPUNIT_ASSERT("def" == t.at(68).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(69).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(69).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(70).getTipoToken());
    CPPUNIT_ASSERT("main" == t.at(70).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(71).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(71).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(72).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(72).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(73).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(73).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(74).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(74).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(75).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(75).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(76).getTipoToken());
    CPPUNIT_ASSERT("=" == t.at(76).getLexema());

    CPPUNIT_ASSERT("DEC" == t.at(77).getTipoToken());
    CPPUNIT_ASSERT("1" == t.at(77).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(78).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(78).getLexema());

    // error
    CPPUNIT_ASSERT("ID" == t.at(79).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(79).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(80).getTipoToken());
    CPPUNIT_ASSERT("=" == t.at(80).getLexema());

    CPPUNIT_ASSERT("DEC" == t.at(81).getTipoToken());
    CPPUNIT_ASSERT("2" == t.at(81).getLexema());



    //refazer
    CPPUNIT_ASSERT("SYM" == t.at(82).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(82).getLexema());

    
    ///// add
 
    
    CPPUNIT_ASSERT("ID" == t.at(83).getTipoToken());
    CPPUNIT_ASSERT("add" == t.at(83).getLexema()); 
    

    CPPUNIT_ASSERT("SYM" == t.at(84).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(84).getLexema()); 
    CPPUNIT_ASSERT("ID" == t.at(85).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(85).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(86).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(86).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(87).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(87).getLexema()); 
    CPPUNIT_ASSERT("SYM" == t.at(88).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(88).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(89).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(89).getLexema()); 


    //sub

    CPPUNIT_ASSERT("ID" == t.at(90).getTipoToken());
    CPPUNIT_ASSERT("dif" == t.at(90).getLexema()); 
    

    CPPUNIT_ASSERT("SYM" == t.at(91).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(91).getLexema()); 
    CPPUNIT_ASSERT("ID" == t.at(92).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(92).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(93).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(93).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(94).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(94).getLexema()); 
    CPPUNIT_ASSERT("SYM" == t.at(95).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(95).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(96).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(96).getLexema()); 

    //div

    CPPUNIT_ASSERT("ID" == t.at(97).getTipoToken());
    CPPUNIT_ASSERT("div" == t.at(97).getLexema()); 
    

    CPPUNIT_ASSERT("SYM" == t.at(98).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(98).getLexema()); 
    CPPUNIT_ASSERT("ID" == t.at(99).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(99).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(100).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(100).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(101).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(101).getLexema()); 
    CPPUNIT_ASSERT("SYM" == t.at(102).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(102).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(103).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(103).getLexema()); 

    //mul

    CPPUNIT_ASSERT("ID" == t.at(104).getTipoToken());
    CPPUNIT_ASSERT("mul" == t.at(104).getLexema()); 
    

    CPPUNIT_ASSERT("SYM" == t.at(105).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(105).getLexema()); 
    CPPUNIT_ASSERT("ID" == t.at(106).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(106).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(107).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(107).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(108).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(108).getLexema()); 
    CPPUNIT_ASSERT("SYM" == t.at(109).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(109).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(110).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(110).getLexema()); 


    //// while

    CPPUNIT_ASSERT("KEY" == t.at(111).getTipoToken());
    CPPUNIT_ASSERT("while" == t.at(111).getLexema()); 
    

    CPPUNIT_ASSERT("SYM" == t.at(112).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(112).getLexema()); 
    CPPUNIT_ASSERT("ID" == t.at(113).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(113).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(114).getTipoToken());
    CPPUNIT_ASSERT("/" == t.at(114).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(115).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(115).getLexema()); 
    CPPUNIT_ASSERT("SYM" == t.at(116).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(116).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(117).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(117).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(118).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(118).getLexema()); 
    
    CPPUNIT_ASSERT("SYM" == t.at(119).getTipoToken()); 
    CPPUNIT_ASSERT("(" == t.at(119).getLexema()); 
    CPPUNIT_ASSERT("ID" == t.at(120).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(120).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(121).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(121).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(122).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(122).getLexema()); 
    CPPUNIT_ASSERT("SYM" == t.at(123).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(123).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(124).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(124).getLexema()); 

    CPPUNIT_ASSERT("KEY" == t.at(125).getTipoToken());
    CPPUNIT_ASSERT("break" == t.at(125).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(126).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(126).getLexema()); 

    CPPUNIT_ASSERT("SYM" == t.at(127).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(127).getLexema()); 

    // if

    CPPUNIT_ASSERT("KEY" == t.at(128).getTipoToken());
    CPPUNIT_ASSERT("if" == t.at(128).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(129).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(129).getLexema());
    
    CPPUNIT_ASSERT("ID" == t.at(130).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(130).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(131).getTipoToken());
    CPPUNIT_ASSERT("<" == t.at(131).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(132).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(132).getLexema());   
    
    CPPUNIT_ASSERT("SYM" == t.at(133).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(133).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(134).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(134).getLexema());


    CPPUNIT_ASSERT("ID" == t.at(135).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(135).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(136).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(136).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(137).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(137).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(138).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(138).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(139).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(139).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(140).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(140).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(141).getTipoToken());
    CPPUNIT_ASSERT("else" == t.at(141).getLexema());

    //if else

    CPPUNIT_ASSERT("KEY" == t.at(142).getTipoToken());
    CPPUNIT_ASSERT("if" == t.at(142).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(143).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(143).getLexema());
    
    CPPUNIT_ASSERT("ID" == t.at(144).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(144).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(145).getTipoToken());
    CPPUNIT_ASSERT(">" == t.at(145).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(146).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(146).getLexema());   
    
    CPPUNIT_ASSERT("SYM" == t.at(147).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(147).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(148).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(148).getLexema());

    //

    CPPUNIT_ASSERT("ID" == t.at(149).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(149).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(150).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(150).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(151).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(151).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(152).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(152).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(153).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(153).getLexema());

    //
    CPPUNIT_ASSERT("SYM" == t.at(154).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(154).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(155).getTipoToken());
    CPPUNIT_ASSERT("else" == t.at(155).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(156).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(156).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(157).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(157).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(158).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(158).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(159).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(159).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(160).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(160).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(161).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(161).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(162).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(162).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(163).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(163).getLexema());

    //
    CPPUNIT_ASSERT("SYM" == t.at(164).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(164).getLexema());


    CPPUNIT_ASSERT("KEY" == t.at(165).getTipoToken());
    CPPUNIT_ASSERT("continue" == t.at(165).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(166).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(166).getLexema());


    CPPUNIT_ASSERT("KEY" == t.at(167).getTipoToken());
    CPPUNIT_ASSERT("return" == t.at(167).getLexema());
    CPPUNIT_ASSERT("DEC" == t.at(168).getTipoToken());
    CPPUNIT_ASSERT("0" == t.at(168).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(169).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(169).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(170).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(170).getLexema());
}


void TestTeste30::setUp(void)
{   
    //char *f[] = "../../testes/teste30.lucas" ;
    //f->push_back("../../testes/teste30.lucas");
    //scanner = new Scanner(1, f);
    scanner = new Scanner();
}

void TestTeste30::tearDown(void)
{
    delete scanner;
}

//-----------------------------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( TestTeste30 );

int main(int argc, char* argv[])
{
    //this.f = argv;
    // informs test-listener about testresults    
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    ofstream xmlFileOut("cppTestBasicMathResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
