#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "CBasicMath.hpp"
#include "../../Scanner.cpp"
#include "../../Token/Token.h"

using namespace CppUnit;
using namespace std;

//-----------------------------------------------------------------------------

class TestTeste19 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestTeste19);
    CPPUNIT_TEST(TestTeste19Lexico);
    //CPPUNIT_TEST(testMultiply);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void TestTeste19Lexico(void);    

private:
    
    Scanner *scanner;
};

//-----------------------------------------------------------------------------

void
TestTeste19::TestTeste19Lexico(void)
{
    const char* fe[2];
    fe[0] = "comando"; 
    fe[1] = "../../testes/teste19.lucas"; 
    scanner->executar(1,fe);
    vector<Token> t = scanner->mostrar();
    
    CPPUNIT_ASSERT(1 == t.size());
    CPPUNIT_ASSERT("KEY" == t.at(0).getTipoToken());
    CPPUNIT_ASSERT("def" == t.at(0).getLexema());
    
}


void TestTeste19::setUp(void)
{   
    //char *f[] = "../../testes/teste19.lucas" ;
    //f->push_back("../../testes/teste19.lucas");
    //scanner = new Scanner(1, f);
    scanner = new Scanner();
}

void TestTeste19::tearDown(void)
{
    delete scanner;
}

//-----------------------------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( TestTeste19 );

int main(int argc, char* argv[])
{
    //this.f = argv;
    // informs test-listener about testresults    
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    ofstream xmlFileOut("cppTestBasicMathResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
