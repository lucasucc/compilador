#ifndef FABRICANO_H_INCLUDED
#define FABRICANO_H_INCLUDED

#include "No.h"
#include "NoRaiz.h"

class FabricaNo{

	private:
		std::vector<std::string> producoesDecvar;		
		std::vector<std::string> naoTerminais;
		std::vector<std::string> producoesType;		

	public:		

		No solicitarNo(std::string nome){
			//No no;
			if( nome.compare("decvar") == 0 ){
				producoesDecvar.push_back("type id ;");
				producoesDecvar.push_back("type id = expr ;");				
				No no("decvar",this->producoesDecvar);
				return no;
			}else if( nome.compare("type") == 0 ){
				producoesType.push_back("int");
				producoesType.push_back("void");
				No no("type",this->producoesType);
				return no;
			}else if( nome.compare("int") == 0 ){
				producoesType.push_back("int");
				//producoesType.push_back(" ");
				No no("int",this->producoesType);
				return no;
			}else if( nome.compare("void") == 0 ){
				producoesType.push_back("void");
				//producoesType.push_back(" ");
				No no("void",this->producoesType);
				return no;
			}else if( nome.compare("id") == 0 ){
				//producoesType.push_back("void");
				//producoesType.push_back(" ");
				No no("id",this->producoesType);
				return no;
			}else if( nome.compare("=") == 0 ){
				//producoesType.push_back("void");
				//producoesType.push_back(" ");
				No no("=",this->producoesType);
				return no;
			}else if(nome.compare("expr") == 0){
				producoesType.push_back("expr1 binop expr1");
				producoesType.push_back("expr1");
				producoesType.push_back("expr1 * expr");
				producoesType.push_back("expr1 / expr1");
				producoesType.push_back("expr1 && expr");
				No no("expr",this->producoesType);
				return no;
			}else if(nome.compare("expr1") == 0){
				producoesType.push_back("- decimal");
				producoesType.push_back("- id");
				producoesType.push_back("unop decimal");
				producoesType.push_back("unop id");
				producoesType.push_back("( id )");
				producoesType.push_back("( decimal )");
				producoesType.push_back("( funcall )"); //teste				
				No no("expr1",this->producoesType);
				return no;
			}else if(nome.compare("funcall") == 0){
				producoesType.push_back("id ( arglist )");				
				No no("funcall",this->producoesType);
				return no;
			}else if(nome.compare("argList") == 0){
				producoesType.push_back("expr , expr");								
				No no("argList",this->producoesType);
				return no;
			}else if(nome.compare("decimal") == 0){
				producoesType.push_back("");								
				No no("decimal",this->producoesType);
				return no;
			}else if(nome.compare("-") == 0){
				producoesType.push_back("");								
				No no("-",this->producoesType);
				return no;
			}else if(nome.compare("unop") == 0){
				producoesType.push_back("!");								
				No no("unop",this->producoesType);
				return no;
			}else if(nome.compare("binop") == 0){
				producoesType.push_back("+");								
				producoesType.push_back("-");
				producoesType.push_back("<");
				producoesType.push_back(">");
				producoesType.push_back(">=");
				producoesType.push_back("<=");
				producoesType.push_back("==");
				producoesType.push_back("!=");
				producoesType.push_back("||");
				No no("binop",this->producoesType);
				return no;
			}else if(nome.compare("(") == 0){
				producoesType.push_back("");												
				No no("(",this->producoesType);
				return no;
			}else if(nome.compare(")") == 0){
				producoesType.push_back("");												
				No no(")",this->producoesType);
				return no;
			}else if(nome.compare(",") == 0){
				producoesType.push_back("");												
				No no(",",this->producoesType);
				return no;
			}else if(nome.compare("+") == 0){
				producoesType.push_back("");												
				No no("+",this->producoesType);
				return no;
			}else if(nome.compare("/") == 0){
				producoesType.push_back("");												
				No no("/",this->producoesType);
				return no;
			}else if(nome.compare("*") == 0){
				producoesType.push_back("");												
				No no("*",this->producoesType);
				return no;
			}else if(nome.compare("<") == 0){
				producoesType.push_back("");												
				No no("<",this->producoesType);
				return no;
			}else if(nome.compare(">") == 0){
				producoesType.push_back("");												
				No no(">",this->producoesType);
				return no;
			}else if(nome.compare("<=") == 0){
				producoesType.push_back("");												
				No no("<=",this->producoesType);
				return no;
			}else if(nome.compare(">=") == 0){
				producoesType.push_back("");												
				No no(">=",this->producoesType);
				return no;
			}else if(nome.compare("==") == 0){
				producoesType.push_back("");												
				No no("==",this->producoesType);
				return no;
			}else if(nome.compare("!=") == 0){
				producoesType.push_back("");												
				No no("!=",this->producoesType);
				return no;
			}else if(nome.compare("||") == 0){
				producoesType.push_back("");												
				No no("||",this->producoesType);
				return no;
			}else if(nome.compare("&&") == 0){
				producoesType.push_back("");												
				No no("&&",this->producoesType);
				return no;
			}else if(nome.compare(";") == 0){
				producoesType.push_back("");												
				No no(";",this->producoesType);
				return no;
			}else if(nome.compare("decfunc") == 0 ){
				producoesType.push_back("");												
				No no("decfunc",this->producoesType);
				return no;
			}else if(nome.compare("block") == 0 ){
				producoesType.push_back("");												
				No no("block",this->producoesType);
				return no;
			}else if(nome.compare("paramList") == 0 ){
				producoesType.push_back("");												
				No no("paramList",this->producoesType);
				return no;
			}else if(nome.compare("def") == 0 ){
				producoesType.push_back("");												
				No no("def",this->producoesType);
				return no;
			}else if(nome.compare("{") == 0){
				producoesType.push_back("");												
				No no("{",this->producoesType);
				return no;
			}else if(nome.compare("}") == 0){
				producoesType.push_back("");												
				No no("}",this->producoesType);
				return no;
			}else if(nome.compare("if") == 0){
				producoesType.push_back("");												
				No no("if",this->producoesType);
				return no;
			}else if(nome.compare("else") == 0){
				producoesType.push_back("");												
				No no("else",this->producoesType);
				return no;
			}else if(nome.compare("while") == 0){
				producoesType.push_back("");												
				No no("while",this->producoesType);
				return no;
			}else if(nome.compare("return") == 0){
				producoesType.push_back("");												
				No no("return",this->producoesType);
				return no;
			}else if(nome.compare("break") == 0){
				producoesType.push_back("");												
				No no("break",this->producoesType);
				return no;
			}else if(nome.compare("continue") == 0){
				producoesType.push_back("");												
				No no("continue",this->producoesType);
				return no;
			}else if(nome.compare("stmt") == 0){
				producoesType.push_back("");												
				No no("stmt",this->producoesType);
				return no;
			}else if(nome.compare("--") == 0){
				producoesType.push_back("");												
				No no("--",this->producoesType);
				return no;
			}else if(nome.compare("!") == 0){
				producoesType.push_back("!");												
				No no("!",this->producoesType);
				return no;
			}else if(nome.compare("assign") == 0){
				producoesType.push_back("assign");												
				No no("assign",this->producoesType);
				return no;
			}

			//return no;
		};
		
		No noRaiz(){
			vector<std::string> producoes;
			producoes.push_back("decvar");
			producoes.push_back("decfunc");
			No noRaiz("program",producoes);
			return noRaiz;
		};

		FabricaNo(){
			this->naoTerminais.push_back("decvar");
			this->naoTerminais.push_back("program");
			this->naoTerminais.push_back("type");
			this->naoTerminais.push_back("expr");
			this->naoTerminais.push_back("binop");
			this->naoTerminais.push_back("unop");
			this->naoTerminais.push_back("expr1");
			this->naoTerminais.push_back("funcall");
			this->naoTerminais.push_back("argList");
			this->naoTerminais.push_back("stmt");
			this->naoTerminais.push_back("block");
			this->naoTerminais.push_back("paramList");
			this->naoTerminais.push_back("decfunc");
			this->naoTerminais.push_back("assign"); 
		};

		bool simboloNaoTerminal(std::string simbolo){
			//cout << "simbolo =" << simbolo << endl;
			//cout << "quantidade de naoTerminais " << this->naoTerminais.size() << endl;
			for (int i = 0; i < this->naoTerminais.size(); ++i)
			{
				//cout << this->naoTerminais.at(i) << endl;
				if(this->naoTerminais.at(i) == simbolo){
					//cout << "este simbolo é nao terminal " << simbolo << endl;
					return true;
				}
			}
			//cout << "este simbolo é terminal = " << simbolo << endl;
			return false;
		}

};
#endif // FABRICANO_H_INCLUDED