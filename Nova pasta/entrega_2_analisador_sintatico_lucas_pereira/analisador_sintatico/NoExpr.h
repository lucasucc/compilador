#ifndef NOEXPR_H_INCLUDED
#define NOEXPR_H_INCLUDED

#include <vector>
#include <iostream>
#include <sstream>


class NoExpr{

	private:
		std::string valor; 
		std::string tipo;
		std::vector<NoExpr> filhos;
		//NoExpr noEsquerda;
		int ordem;
		NoExpr *noEsquerda;
		NoExpr *noDireita;
		std::string nome;

	public:

		NoExpr(std::string valor,std::string tipo, int ordem){
			//NoExpr noExprEsquerda,noExprDireita;

			//this->filhos.push_back(noExprEsquerda);
			//this->filhos.push_back(noExprDireita);	
			this->valor = valor;
			this->tipo = tipo;
			this->noDireita = 0;
			this->noEsquerda = 0;
			this->ordem = ordem;
			this->nome = "";
			//prepararFilhos();	
			//this->filhos(2);
			//NoExpr expr1;
			//NoExpr expr2;
			//this->filhos.push_back(expr1);
			//this->filhos.push_back(expr2);
		};
		
		NoExpr(){
			//this->noDireita = 0;
			//this->noEsquerda = 0;
			//NoExpr noExprEsquerda,noExprDireita;

			//this->filhos.push_back(noExprEsquerda);
			//this->filhos.push_back(noExprDireita);
			this->tipo = "vazio";
			this->valor = "vazio";
			this->noDireita = 0;
			this->noEsquerda = 0;
			this->ordem = -1;
			this->nome = "";
			//prepararFilhos();	
			//this->filhos(2);
			//NoExpr expr1("vazio","vazio"); 
			//NoExpr expr2;
			//this->filhos.push_back(expr1);
			//this->filhos.push_back(expr2);
		};

		void setAlias(std::string nome){
			this->nome = nome;
		};
		
		std::string getAlias(){
			return this->nome;
		};	
		
		void setOrdem(int ordem){
			this->ordem;
		};

		int getOrdem(){
			return this->ordem;
		}

		std::string getValor(){
			return this->valor;
		};

		void setValor(std::string valor){
			this->valor = valor;
		};

		std::string getTipo(){
			return this->tipo;
		};

		/*
		NoExpr getFilhoEsquerda(){
			return this->filhos.at(0);
		};
		*/
	/*
		NoExpr getFilhoDireita(){
			return this->filhos.at(1);
		};
	*/	
		NoExpr* getFilhoEsquerda(){
			return this->noEsquerda;
		};
		
		NoExpr* getFilhoDireita(){
			return this->noDireita;
		};


		void prepararFilhos(){
			//NoExpr exp1, expr2;
			
			//if(this->filhos.size() == 0){
			//this->filhos.push_back(exp1);
			//this->filhos.push_back(expr2);
			//}
			
		}

		/*
		void setNoEsquerda(NoExpr noEsquerda){
			this->filhos.at(0) = noEsquerda;
			
		};
		*/
		/*
		void setNoDireita(NoExpr noDireita){
			this->filhos.at(1) = noDireita;
		};
		*/
		void setNoEsquerda(NoExpr *noEsquerda){			
			this->noEsquerda = noEsquerda;			
		};

		void setNoDireita(NoExpr *noDireita){
			this->noDireita = noDireita;
		};


		vector<NoExpr> getFilhos(){
			return this->filhos;
		}

		void setFilhos(vector<NoExpr> filhos){
			this->filhos = filhos;
		}

		void print(){
			cout << "valor: " << this->valor << " ordem: " << this->ordem << " getFilhoEsquerda: " << this->noEsquerda->getValor() << " getFilhoDireita: " << this->noDireita->getValor() << endl; 
		}

};
#endif // NOEXPR_H_INCLUDED