#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <locale>
#include "analisador_lexico/AnalisadorLexico.h"


using namespace std;

class Scanner{
private: AnalisadorLexico analisadorLexico;
		 ofstream arquivo;
public:
	bool avaliarLexema(vector<string> caracteresParada,string *lexema ){
		for (int i = 0; i < caracteresParada.size(); ++i)
		{
			std::size_t posicao = lexema->find(caracteresParada.at(i));
			if(posicao != std::string::npos){				
					//lexema->erase();
					return true;
			}
		}
		return false;
	}

	void carregarCaracterParada(vector<char> *caracteresParada){
		caracteresParada->push_back(' ');
		caracteresParada->push_back('\t');
		caracteresParada->push_back('\r');
		//caracteresParada->push_back("(");
		//caracteresParada->push_back(";");
		caracteresParada->push_back('\n');

	}

	bool ehCaracterParada(char caractere, vector<char> caracteresParada ){
		for (int i = 0; i < caracteresParada.size(); ++i){
			//cout << caractere << endl;
			if( caractere == caracteresParada.at(i) ){
				return true;
			}	
		}
		return false;
	}

	/*
	vector<Token> mostrar(){
		return analisadorLexico.getTokens2();
	}
	*/

	int executar(int argc, char const *argv[]){
		
		//int xif = 10;
		//cout << xif << endl;
		string lexema;
		ifstream arquivo;
		arquivo.open(argv[1]);
		char caractere;
		//AnalisadorLexico analisadorLexico; 	
		vector<char> caracteresParada;	
		//setlocale(LC_ALL, "portuguese");
		string lexemaAc;
		bool apagarLexema = false;
		bool comentario = false;
		carregarCaracterParada(&caracteresParada);
		int linha=1;
		int coluna=0;

		if(arquivo.is_open()){
			while( arquivo.get(caractere) ){								
				coluna++;
				if( !comentario ){				
					size_t numeroE = std::count( lexemaAc.begin(),lexemaAc.end(),'&');
					size_t numeroPipe = std::count( lexemaAc.begin(),lexemaAc.end(),'|');
					//cout << "numeroE" << numeroE << endl;
					//cout << "numeroPipe" << numeroPipe << endl;
					//cout << "lexema" << lexemaAc <<endl;
					if( !lexemaAc.empty() && caractere == '/' && lexemaAc.substr(lexemaAc.length()-1,lexemaAc.length()) == "/" ){
						//cout << "/ erro " << caractere << " : " << lexemaAc << endl;
						if(lexemaAc.length() > 1){
							lexemaAc = lexemaAc.substr(0,lexemaAc.length()-1);
							analisadorLexico.definirToken(lexemaAc,coluna,linha);						
						}
						lexemaAc.erase();
						comentario = true;					
					}else if( !lexemaAc.empty() && ((caractere != '&' && lexemaAc.substr(lexemaAc.length()-1,lexemaAc.length()) == "&" && numeroE % 2 != 0) || (caractere != '|' && lexemaAc.substr(lexemaAc.length()-1,lexemaAc.length()) == "|" && numeroPipe % 2 != 0 )) ){					
						cout << "erro caractere:" <<"\""<<lexemaAc.substr(lexemaAc.length()-1,lexemaAc.length())<<"\" "<<linha<<":"<<--coluna <<endl;
						return 0;
					}else if( std::isalpha(caractere) || caractere == '_' ){
						if( !lexemaAc.empty() ){												
							if( !analisadorLexico.podeSerTokenKey(lexemaAc) && !analisadorLexico.ehTokenId(lexemaAc) && !analisadorLexico.ehTokenDecimal(lexemaAc) ){
								analisadorLexico.definirToken(lexemaAc,coluna,linha);
								apagarLexema = true;
							}else{
								lexemaAc.push_back(caractere);							
							}
						}else{
							lexemaAc.push_back(caractere);
						}
						//cout << "caractere" << endl;					
					}else if(std::isdigit(caractere)){
						if( !lexemaAc.empty() ){
							if( !analisadorLexico.ehTokenDecimal(lexemaAc) && !analisadorLexico.ehTokenId(lexemaAc) ){
								//if( !analisadorLexico.ehTokenId(lexemaAc) ){
									//cout << "apagou?2" << endl;
									
									analisadorLexico.definirToken(lexemaAc,coluna,linha);
									apagarLexema = true;
								//}
								
							}else{
								lexemaAc.push_back(caractere);		
							}

						}else{
							lexemaAc.push_back(caractere);		
						}
						//cout << "digito" << endl;
						
					}else if( analisadorLexico.podeSerTokenSym(caractere)){
						//cout << "token sym"<<caractere << endl;
						bool jaAddCaractere = false;
						if( !lexemaAc.empty() ){
							if( (caractere == '&' && lexemaAc == "&") || (caractere == '|' && lexemaAc == "|") ){
								lexemaAc.push_back(caractere);
								jaAddCaractere = true;
							}		
						
							if( !analisadorLexico.ehTokenSym(lexemaAc) ){
								analisadorLexico.definirToken(lexemaAc,coluna,linha);
								apagarLexema = true;
							}else{
								if(!jaAddCaractere){
									lexemaAc.push_back(caractere);		
								}
							}
						}else{
							lexemaAc.push_back(caractere);
						}
						//cout << "sym" << endl;
					}else if( ehCaracterParada(caractere,caracteresParada) ){
						if(caractere == '\n'){
							coluna=0;
							linha++;
						}
						if(!lexemaAc.empty()){
							//cout << "aqui" << endl;
							analisadorLexico.definirToken(lexemaAc,coluna,linha);
							//cout <<"leu aqui " << endl;
							//apagarLexema = true;
							lexemaAc.erase();
						}
					}else{
						cout << "erro caractere:" <<"\""<<caractere<<"\" "<<linha<<":"<<coluna <<endl;
						return 0;
					}
								
					if(apagarLexema){
						lexemaAc.erase();
						lexemaAc.push_back(caractere);
						//cout <<"caractere"<<caractere<< endl;
						apagarLexema = false;
					}
					
				}else if(caractere == '\n'){
					coluna=0;
					linha++;
					comentario = false;
				}

				//lexema.push_back(caractere);							
				//if(avaliarLexema(caracteresParada,&lexema)){
					//lexema = std::remove(lexema.begin(),lexema.end()," ");
					//analisadorLexico.executar(lexema);
				//	lexema.erase();
				//}						
			}

			if(!lexemaAc.empty()){
				//cout << "ultimo " <<endl;
				size_t numeroE = std::count( lexemaAc.begin(),lexemaAc.end(),'&');
				size_t numeroPipe = std::count( lexemaAc.begin(),lexemaAc.end(),'|');
				if( numeroE % 2 != 0 || numeroPipe % 2 != 0 ){
					cout << "erro caractere:" <<"\""<<caractere<<"\" "<<linha<<":"<<coluna <<endl;
					return 0;
				}else{
					analisadorLexico.definirToken(lexemaAc,coluna,linha);
				}
			}	
			arquivo.close();
			//analisadorLexico.imprimirTokens();
		}else{
			cout << "problemas na abertura do arquivo";
		}
		//analisadorLexico.imprimirTokens();
		//mostrar();
		return 0;
	};

	vector<Token> getStreamToken(){
		return analisadorLexico.getTokens2();
	};
	
	void gerarArquivo(char const *argv[]){
		//cout << argv[2] << endl;
		//char result[100]
		//char const extencao[] = ".out";
		//cout << argv[2] << extencao << endl;
		//string buff;
		//strcpy(result,argv[2]);
		//strcpy(result,extencao);
		//buff.append(argv[2]);
		//buff.append(extencao);
		//cout << buff<< endl;
		
		arquivo.open(argv[2]);
		vector<Token> tokens2 = analisadorLexico.getTokens2();
		for (int i = 0; i < tokens2.size(); ++i){
			if( tokens2.at(i).getTipoToken() == "ID"){
				arquivo <<tokens2.at(i).getTipoToken()<<"   \""<<tokens2.at(i).getLexema()<<"\""<<endl;	
			}else{
				arquivo <<tokens2.at(i).getTipoToken()<<"  \""<<tokens2.at(i).getLexema()<<"\""<<endl;
			}			
		}
		arquivo.close();
		
	}

};






