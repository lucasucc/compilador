#ifndef ANALISADORSINTATICO_H_INCLUDED
#define ANALISADORSINTATICO_H_INCLUDED

#include <stack>
#include <map>
#include <list>
#include <queue>
#include "../token/Token.h"
#include "FabricaNo.h"
#include "NoRaiz.h"
#include "No.h"
#include "Retorno.h"
#include "NoExpr.h"
#include "ConversorExpr.h"

class AnalisadorSintatico{
	private:		
		std::vector<Token> streamTokens;
		FabricaNo fabricaNo;
		No noRaiz;
      std::stack<std::string> pilhaOperacoes;
      std::stack<std::string> pilhaNumeros;
	   std::vector<NoExpr> listaExpressoes;  
      std::vector<std::string> numeros;
      NoExpr *superRaiz;
      NoExpr *novoNo;
      NoExpr *ultimoNo;
      NoExpr *noAnteriorNumero;
      NoExpr *noAnteriorSinal;
      NoExpr *noAtualSinal;
      std::vector<NoExpr*> vetorMultiplicacao;
      std::vector<NoExpr*> vetorSoma;   
      std::string subArvorePrefixa ;
      int qtdFolhasVisitadas;
      int qtdFolhasParaMostrar;
      ConversorExpr conversorExpr;
      std::string arvoreAst;

   public:				
		AnalisadorSintatico(std::vector<Token> streamTokens){
			this->streamTokens = streamTokens;			
			this->noRaiz = this->fabricaNo.noRaiz();
		   superRaiz = 0;
         ultimoNo = 0;
         noAnteriorNumero = 0;
         noAnteriorSinal = 0;
         noAtualSinal = 0;
         subArvorePrefixa = "";
         qtdFolhasVisitadas = 0;
      };
      

      void printPilha(std::stack<NoExpr> pilha){
         std::string subArvore = "";
         while(!pilha.empty()){
            //cout << "chave:" << pilha.top().getValor() << endl;
            subArvore = pilha.top().getValor() + subArvore;
            pilha.pop();
         }
         //cout << "subArvore da pilha " << subArvore << endl;
      }

      void inserirArvoreExpr(std::string op,std::string tipo){
         NoExpr noExpr(op,tipo,listaExpressoes.size());
         noExpr.setAlias("["+ op +"]");
         //NoExpr expr1,expr2;
         //NoExpr *noExprP;
         //noExprP = &noExpr; 
         //cout <<"valor " <<  noExprP->getValor() << endl;
         //vector<NoExpr> filhos;
         //filhos.push_back(expr1);
         //filhos.push_back(expr2); 
         //noExpr.setFilhos(filhos);
         listaExpressoes.push_back(noExpr);
      };

      void inserirNovoNo2(NoExpr noExpr){
         NoExpr *novoNo = new NoExpr(noExpr.getValor(),noExpr.getTipo(),noExpr.getOrdem());;          
         //NoExpr *noVazio = new NoExpr();
         //noAnteriniiorNumero;
         //noAnteriorSinal;
         NoExpr *aux;
         //NoExpr *aux;
         int id;
         //cout<< "no: " << noExpr.getValor() << endl; 
         //cout<< "novoNo tipo: " << novoNo->getValor() << " valor " << novoNo->getTipo() << endl; 
         if(noAnteriorNumero == 0 && novoNo->getTipo() == "numero" ){
            //cout <<"numero" << endl;            
            noAnteriorNumero = novoNo;
            
         }else if(noAtualSinal == 0 && novoNo->getTipo() == "sinal"){
            //cout << "aqui vai o sinal "<< endl;
            //noAnteriorSinal = novoNo;
            novoNo->setNoDireita(noAnteriorNumero);
            if(novoNo->getValor() == "+" || novoNo->getValor() == "-"){
               vetorSoma.push_back(novoNo);               
               noAnteriorSinal = vetorSoma.at(vetorSoma.size() -1 );                  
               noAtualSinal = noAnteriorSinal;
            }else{
               vetorMultiplicacao.push_back(novoNo);               
               noAnteriorSinal = vetorMultiplicacao.at(vetorMultiplicacao.size() -1 );
               noAtualSinal = noAnteriorSinal;
            }

         }else if(novoNo->getTipo() == "sinal" ){                        
               //cout << "entrou aquiii " << noAnteriorSinal->getValor()  << endl;
               if(noAtualSinal->getValor() == "+" && novoNo->getValor() == "*" || noAtualSinal->getValor() == "-" && novoNo->getValor() == "*"){
                  //cout << "noAnterio numero esquerda " << endl;
                  if( noAtualSinal->getFilhoEsquerda() == noAnteriorNumero){
                    //cout << " noAnteriorNumero " << noAnteriorNumero->getOrdem() << " valor e tipo: " << noAnteriorNumero->getValor() << noAnteriorNumero->getTipo() << endl;
                    // if(noAnteriorSinal->getFilhoEsquerda() != 0 && noAnteriorSinal->getFilhoEsquerda()->getOrdem() + 1 ){

                     //}
                     aux = noAtualSinal->getFilhoEsquerda();
                     noAtualSinal->setNoEsquerda(0);                                    
                     if(aux->getOrdem() > novoNo->getOrdem() ){
                        novoNo->setNoDireita(aux);   
                     }else{
                        novoNo->setNoEsquerda(aux);
                     }
                     
                  }else{
                     aux = noAtualSinal->getFilhoDireita();
                     noAtualSinal->setNoDireita(0);                                    
                     if(aux->getOrdem() < novoNo->getOrdem()){
                        novoNo->setNoDireita(aux);   
                     }else{
                        novoNo->setNoEsquerda(aux);
                     }
                     novoNo->setNoDireita(aux);   
                  }
                  noAnteriorSinal = noAtualSinal;                  
                  vetorMultiplicacao.push_back(novoNo);               
                  noAtualSinal = vetorMultiplicacao.at(vetorMultiplicacao.size() -1 );

               }else if(noAtualSinal->getValor() == "+" && novoNo->getValor() == "+" || noAtualSinal->getValor() == "*" && novoNo->getValor() == "+" || noAtualSinal->getValor() == "-" && novoNo->getValor() == "-" || noAtualSinal->getValor() == "*" && novoNo->getValor() == "-" ){                  
                  
                  vetorSoma.push_back(novoNo);               
                  noAnteriorSinal = noAtualSinal;
                  noAtualSinal = vetorSoma.at(vetorSoma.size() -1 );                  
                 
                  //noAnteriorSinal = noAtualSinal;
                                 
               }else if(noAtualSinal->getValor() == "+" && novoNo->getValor() == "-" || noAtualSinal->getValor() == "-" && novoNo->getValor() == "+" ){
                  if(noAnteriorNumero->getOrdem() == novoNo->getOrdem() + 1){
                     if(noAtualSinal->getFilhoEsquerda() != 0 && noAtualSinal->getFilhoEsquerda()->getOrdem() == novoNo->getOrdem() + 1){
                        aux = noAtualSinal->getFilhoEsquerda();
                        noAtualSinal->setNoEsquerda(0);
                        novoNo->setNoDireita(aux);                        
                     }else{
                        aux = noAtualSinal->getFilhoDireita();
                        noAtualSinal->setNoDireita(0);
                        novoNo->setNoDireita(aux);
                     }
                     
                  }
                  vetorSoma.push_back(novoNo);               
                  //noAnteriorSinal = noAtualSinal;
                  noAtualSinal = vetorSoma.at(vetorSoma.size() -1 );                  
             
               }else if(noAtualSinal->getValor() == "*" && novoNo->getValor() == "*" ){
                 vetorMultiplicacao.push_back(novoNo);               
                 noAnteriorSinal = noAtualSinal;
                 noAtualSinal = vetorMultiplicacao.at(vetorMultiplicacao.size() -1 );                  
                  
               }  
            
                        
         }else if(novoNo->getTipo() == "numero"){
            //cout << "no direita:" << noAtualSinal->getFilhoDireita() << "noAtualSinal->tipo()"<< noAtualSinal->getTipo() << " valor " << noAtualSinal->getValor() << endl;
            /*
            if(noAtualSinal->getValor() == "+" ){
               noAtualSinal = vetorSoma.at(vetorSoma.size() -1 );
               if(noAtualSinal->getFilhoDireita() == 0 ){
                  noAtualSinal->setNoDireita(novoNo);                  
               }else{
                  noAtualSinal->setNoEsquerda(novoNo);                  
               }               
               vetorSoma.at(vetorSoma.size() -1 ) = noAtualSinal;
            }else if(noAtualSinal->getValor() == "*" ){
               noAtualSinal = vetorMultiplicacao.at(vetorMultiplicacao.size() -1 );
               cout << "noAtualSinal direita" << noAtualSinal->getFilhoDireita() << endl;

               if(noAtualSinal->getFilhoDireita() == 0 ){
                  noAtualSinal->setNoDireita(novoNo);                  
               }else{
                  cout << "multiplicacao com vazio "<< endl;
                  //vetorMultiplicacao.at(vetorMultiplicacao.size() -1 )->setNoEsquerda(novoNo);
                  noAtualSinal->setNoEsquerda(novoNo);                  
               }               
               vetorMultiplicacao.at(vetorMultiplicacao.size() -1 ) = noAtualSinal;
            }
            */
            
            if(noAtualSinal->getValor() == "+" || noAtualSinal->getValor() == "-"  ){
               noAtualSinal = vetorSoma.at(vetorSoma.size() -1 );
               if(noAtualSinal->getFilhoDireita() == 0 && noAtualSinal->getOrdem() < novoNo->getOrdem() ){
                  noAtualSinal->setNoDireita(novoNo);                  
               }else{
                  noAtualSinal->setNoEsquerda(novoNo);                  
               }               
               vetorSoma.at(vetorSoma.size() -1 ) = noAtualSinal;
            }else if(noAtualSinal->getValor() == "*" ){
               noAtualSinal = vetorMultiplicacao.at(vetorMultiplicacao.size() -1 );
               //cout << "noAtualSinal direita" << noAtualSinal->getFilhoDireita() << endl;

               if(noAtualSinal->getFilhoDireita() == 0  && noAtualSinal->getOrdem() > novoNo->getOrdem()){
                  noAtualSinal->setNoDireita(novoNo);                  
               }else{
                  //cout << "multiplicacao com vazio "<< endl;
                  //vetorMultiplicacao.at(vetorMultiplicacao.size() -1 )->setNoEsquerda(novoNo);
                  noAtualSinal->setNoEsquerda(novoNo);                  
               }               
               vetorMultiplicacao.at(vetorMultiplicacao.size() -1 ) = noAtualSinal;
            }

            noAnteriorNumero = novoNo;            
         }
         
      };

      void inserirNovoNo(NoExpr noExpr){
         NoExpr *novoNo = new NoExpr(noExpr.getValor(),noExpr.getTipo(),noExpr.getOrdem());;          
         NoExpr *aux;
         //cout<< "no: " << noExpr.getValor() << endl; 
         if(superRaiz == 0){
            superRaiz = novoNo;
            ultimoNo = superRaiz;
         }else if(ultimoNo == superRaiz && ultimoNo->getTipo() == "numero"){
            novoNo->setNoDireita(superRaiz);
            superRaiz = novoNo;
            ultimoNo = novoNo;
         }else if(ultimoNo->getFilhoEsquerda() == 0 ){
            ultimoNo->setNoEsquerda(novoNo);            
            //cout << "entrou aqui uma vez" << endl;
         }else if(ultimoNo->getFilhoDireita() == 0){
            ultimoNo->setNoDireita(novoNo);            
         }else if(superRaiz->getTipo() == "sinal" && ((superRaiz->getValor() == "+" && novoNo->getValor() == "+")  || ( superRaiz->getValor() == "*" && novoNo->getValor() == "*") || (superRaiz->getValor() == "*" && novoNo->getValor() == "+") ) ){
            novoNo->setNoEsquerda(superRaiz);
            superRaiz = novoNo;
            ultimoNo = novoNo;
         }else if( superRaiz->getValor() == "+" &&  novoNo->getValor() == "*" && superRaiz->getFilhoDireita()->getOrdem() == novoNo->getOrdem() + 1){            
            aux = superRaiz->getFilhoDireita();
            novoNo->setNoDireita(aux);            
            superRaiz->setNoDireita(novoNo);
            ultimoNo = novoNo;
            aux = superRaiz->getFilhoDireita();
            superRaiz->setNoDireita(superRaiz->getFilhoEsquerda());
            superRaiz->setNoEsquerda(aux);
         }else{   
            encontrarLocal(novoNo,superRaiz);
         }
      };

      void encontrarLocal(NoExpr *novoNo, NoExpr *pNoExpr){
         NoExpr *aux;
         
         if(pNoExpr != 0){
            if(novoNo->getTipo() == "sinal" && ( (novoNo->getValor() == "*" && pNoExpr->getValor() == "+" ) || (novoNo->getValor() == "*" && pNoExpr->getValor() == "*" ) ) && pNoExpr->getFilhoEsquerda()->getTipo() == "numero" ){
               //cout << "novoNo " << novoNo->getValor() << " ordem " << novoNo->getOrdem() << endl;
               aux = pNoExpr->getFilhoEsquerda();
               novoNo->setNoDireita(aux);
               pNoExpr->setNoEsquerda(novoNo);
               ultimoNo = novoNo;                     
               
               return;
            }else if(novoNo->getTipo() == "sinal" && novoNo->getValor() == "+" && pNoExpr->getValor() == "*"){
               novoNo->setNoEsquerda(pNoExpr);            
               ultimoNo = novoNo;                     
               if(pNoExpr == superRaiz){
                  superRaiz = novoNo;
               }
               return;
            }

           // if(pNoExpr->getFilhoEsquerda() != 0 && pNoExpr->getFilhoDireita() != 0 && pNoExpr->getFilhoEsquerda()->getOrdem() - novoNo->getOrdem() + 1 < pNoExpr->getFilhoDireita()->getOrdem() - novoNo->getOrdem() + 1  ){
               encontrarLocal(novoNo, pNoExpr->getFilhoDireita());      
            //}else{               
               encontrarLocal(novoNo, pNoExpr->getFilhoEsquerda());
            //}
            
            
            
            //cout << "break point3" << endl;
         }
         
      };

      std::string arvoreExpr(){
         std::string subArvore = "";         
         int qtdNumeros = 0;
         std::string numero = "",sinal = "";
         std::stack<std::string> novaPilhaNumeros;
         std::stack<std::string> novaPilhaOperacoes;
         //cout << "valor =" << this->pilhaOperacoes.size() << endl; 
         std::string ultimoOp;
         while(!this->pilhaNumeros.empty()){
            novaPilhaNumeros.push(this->pilhaNumeros.top());
            this->pilhaNumeros.pop();            
         }
         
         while(!this->pilhaOperacoes.empty()){
            novaPilhaOperacoes.push(this->pilhaOperacoes.top());
            this->pilhaOperacoes.pop();
         }    

         this->pilhaNumeros = novaPilhaNumeros;
         this->pilhaOperacoes = novaPilhaOperacoes;
         bool primeira = true;
         std::size_t found;
         while(!this->pilhaNumeros.empty() || !this->pilhaOperacoes.empty() ){
            if(this->pilhaOperacoes.top() == "+" && primeira ){
               //cout << "this->pilhaNumeros.top()" << this->pilhaNumeros.top() << endl;               
               subArvore = this->pilhaNumeros.top() ;
               this->pilhaNumeros.pop();
               subArvore = subArvore + this->pilhaNumeros.top() ; 
               this->pilhaNumeros.pop();
               sinal = this->pilhaOperacoes.top();
               this->pilhaOperacoes.pop();
               subArvore = sinal + subArvore;
               ultimoOp = sinal;
               primeira = false;
            
            }else if(primeira == false){
               if(!this->pilhaNumeros.empty()){
                  subArvore = subArvore + this->pilhaNumeros.top();
                  this->pilhaNumeros.pop();
               }
               if(!this->pilhaOperacoes.empty()){
                  sinal = this->pilhaOperacoes.top();
                  if( sinal == "+" && (ultimoOp == "*" || ultimoOp == "/" ) ){
                     std::string op1,op2,tmp,op3;
                     bool op = true;
                     found = subArvore.find_last_of("<");   
                     op1 = subArvore.substr(found,subArvore.size());
                     found = subArvore.find_last_of("<");   
                     op2 = subArvore.substr(found,subArvore.size());
                     tmp = sinal + op1 + op2; 
                     for (int i = 0; i < subArvore.size(); ++i)
                     {
                        if(subArvore.at(i) != '<' && subArvore.at(i) != '>' && !op){
                           if(subArvore.at(i) == '+'){
                              tmp = subArvore.at(i) + tmp;
                           }
                        }else if(subArvore.at(i) == '<'){
                           op = false;
                           //op3 = op3 + 
                        }
                     }

                  }
                  subArvore = this->pilhaOperacoes.top() + subArvore;
                  this->pilhaOperacoes.pop();  
               }
            }else if(primeira == true){

            }
            
         }
         
         return subArvore;
      };

      /*
      std::string arvoreExpr2(){
         std::string subArvore = "";
         while(!this->pilhaNumeros.empty()){
            subArvore = this->pilhaNumeros.top() + subArvore;
            cout << "pilha " << this->pilhaNumeros.top() << endl;
            this->pilhaNumeros.pop();
         }
         return subArvore;
      };
      */
      NoExpr inserirNovaRaiz(NoExpr noRaizAntigo,NoExpr novoNoRaiz ){
           /*       
         if(noRaizAntigo != NULL ){
            noRaizAntigo = novoNoRaiz;
            return noRaizAntigo; 
         }
         
         novoNoRaiz->setNoDireita(noRaizAntigo);
         return novoNoRaiz;
         */
      };

      NoExpr montarNoExpr(std::vector<NoExpr> listaNoExpr){
         /*
         NoExpr noExprNumero,noExprSinal;
      
         if(listaNoExpr.size() == 2){
            if(listaNoExpr.at(0).getTipo() == "numero"){
               noExprNumero = listaExpressoes.at(0);
               noExprSinal = listaExpressoes.at(1);
            }else if(listaNoExpr.at(1).getTipo() == "sinal"){
               noExprNumero = listaExpressoes.at(0);
               noExprSinal = listaExpressoes.at(1);
            }
            
         }
         noExprSinal.setNoEsquerda(noExprNumero);
         return noExprSinal;
         */
      };

      NoExpr noInicialExpr(std::vector<NoExpr> listaNoExpr){
         
         NoExpr noExprNumero1,noExprSinal,noExprNumero2;   
      // NoExpr *noExprNumero1P =0 ,*noExprNumero2P =0;
         //noExprNumero1 = listaNoExpr.at(0);
         //noExprNumero1P = &noExprNumero1;
         //listaNoExpr.pop_front();
         //cout << "sinal ? " << listaNoExpr.at(1).getValor() << endl;
         noExprSinal = listaNoExpr.at(1);
         //cout << "sinal2 ? " << noExprSinal.getValor() << endl;
         //listaNoExpr.pop_front();
         
         //noExprNumero2 = listaNoExpr.at(2);
         //noExprNumero2P = &noExprNumero2;
         //listaNoExpr.pop_front();
         
         //noExprSinal.setNoEsquerda(noExprNumero1P);

         //noExprSinal.setNoDireita(noExprNumero2P);
         return noExprSinal;
         
      };

      NoExpr listaParaArvore2(){
         /*
         bool primeiraPart = true;
         NoExpr noExprRaiz;
         int tamanho;
         */
         //if(primeiraPart && listaExpressoes.size() >= 3 ){
            /*
            std::vector<NoExpr> listaNoExpr;
            listaNoExpr.push_back(listaExpressoes.at(0));
            //listaExpressoes.pop_front();            
            listaNoExpr.push_back(listaExpressoes.at(1));
            //listaExpressoes.pop_front();
            listaNoExpr.push_back(listaExpressoes.at(2));
            //listaExpressoes.pop_front();
            noExprRaiz = noInicialExpr(listaNoExpr); 
            */
            //cout << "RESULTADO DO TESTE INICIO";
            //cout << "valor puro " << noExprRaiz.getValor() << endl;            
            //cout << "RESULTADO DO TESTE FINAL";
         //}
         
         /*
         tamanho = listaExpressoes.size();
         for (int i = 0; i < tamanho; ++i)
         {
            noExprRaiz = montarNoExpr(); 
         }
         */
         /*
         arvoreExpr4(noExprRaiz);

         return noExprRaiz;
         */
      };
      

      void arvoreExpr4(NoExpr noRaiz){
         /*
         if(!noRaiz->getNoDireita() ){
            arvoreExpr4(noRaiz->getNoDireita());   
         }
         
         if(!noRaiz->getNoEsquerda() ){
            arvoreExpr4(noRaiz->getNoEsquerda());   
         }

         cout << "valor " << noRaiz->getValor() << endl;
         */
      };

   
      void inserirAoFim(std::string numero){
         /*
         stack<std::string> pilhaAux;
         bool inserirAoFimV = true;
         while(!this->pilhaNumeros.empty()){
            if(this->pilhaNumeros.top() == "%" ){
               //pilhaAux.push(numero);
               inserirAoFimV = false;
               //pilhaAux.push(this->pilhaNumeros.top());
            }
            pilhaAux.push(this->pilhaNumeros.top());
            this->pilhaNumeros.pop();
         }
         if(inserirAoFimV){
            //pilhaAux.push(numero);
         }
         std::string ope;
         while(!pilhaAux.empty()){
            ope = pilhaAux.top();
            if(ope == "%"){
               this->pilhaNumeros.push(numero);
            }else{
               this->pilhaNumeros.push(ope);
            }
            pilhaAux.pop();
         }
      
         if(inserirAoFimV){
            this->pilhaNumeros.push(numero);   
         }
         */
      }

      void arvore(NoExpr novoNo){
         NoExpr* aux,aux2;
         //NoExpr aux1;
         //cout << "valor aquiii " << novoNo.getValor() << endl; 
         //cout << "tipo " << novoNo.getTipo() << endl; 
         if(superRaiz == 0){
            //cout << "tipo da raiz " << novoNo.getTipo() << endl; 
            superRaiz = new NoExpr(novoNo.getValor(),novoNo.getTipo(),novoNo.getOrdem());
            //superRaiz = aux;
            //aux2 = *superRaiz;
            //cout << "super raiz igual = " << superRaiz->getValor() << endl;
         }else if(superRaiz->getTipo() == "sinal"){
            //aux = superRaiz;
            if(novoNo.getTipo() == "numero" && superRaiz->getFilhoEsquerda() == 0 ){               
               aux = new NoExpr(novoNo.getValor(),novoNo.getTipo(),novoNo.getOrdem());
               superRaiz->setNoEsquerda(aux);
               
            }else if(novoNo.getTipo() == "numero" && superRaiz->getFilhoDireita() == 0){
               aux = new NoExpr(novoNo.getValor(),novoNo.getTipo(),novoNo.getOrdem());
               superRaiz->setNoDireita(aux);
            }else if(novoNo.getTipo() == "sinal"){
               aux = new NoExpr(novoNo.getValor(),novoNo.getTipo(),novoNo.getOrdem());
               //superRaiz.setNoEsquerda(aux);
               aux->setNoDireita(superRaiz);
               superRaiz = aux;               
            }
                     
         }else if(superRaiz->getTipo() == "numero"){
            
            if(novoNo.getTipo() == "sinal"){
               aux = new NoExpr(novoNo.getValor(),novoNo.getTipo(),novoNo.getOrdem());
               aux->setNoEsquerda(superRaiz);
               superRaiz = aux;
            }
            //aux = new NoExpr(novoNo.getValor(),novoNo.getTipo());
            //superRaiz->setNoDireita(aux);
            //aux2->setNoDireita(aux);
            //superRaiz = aux;
            
         }
         
      }


      void iniciarArvore4(){
                           
         for (int i = this->listaExpressoes.size()-1; i > -1 ; --i)
         {
         //for (int i = 0; i < this->listaExpressoes.size() ; ++i)
         //{
            //this->listaExpressoes.at(i);
            //inserirNovoNo(this->listaExpressoes.at(i));
            inserirNovoNo2(this->listaExpressoes.at(i));
         }
         //cout << "prefixa 4 "<< endl;
         //printarArvorePrefixa4(superRaiz);
         //printarArvorePrefixa6();
         printarArvorePrefixa7();
      };

      std::string iniciarArvore5(){
         //this->listaExpressoes;                  
         std::string stringRetorno = "";
         if(this->listaExpressoes.size() > 0){
            stringRetorno = this->conversorExpr.executar(this->listaExpressoes);   
            this->listaExpressoes.clear();
            return stringRetorno; 
         }
         return "";
      };
   

      void iniciarArvore2(){
         
         NoExpr *raiz = 0;
         
         //cout << "tamanho " << this->listaExpressoes.size() << endl;
         int qtdSinal = 0;
         int qtdNumero = 0;         
         bool primeiroCaso = false;
         int j = 4;
         
         for (int i = 0; i < this->listaExpressoes.size(); ++i)
         {
            
            arvore(this->listaExpressoes.at(i));
            //cout << "chave da raiz=> " << raiz->getValor() << endl;
            printarArvorePrefixa3(superRaiz);
            if(i == j){
               //cout << "vai pra avaliacao = "<< endl;
               avaliacao();
               j = j + 2;
            } 
            //cout << "depois da avaliacao " << endl;
            printarArvorePrefixa3(superRaiz);

         }
         //cout << "inicio do print" << endl;
         printarArvorePrefixa3(superRaiz);
         //cout << "final do print" << endl;
      };

      void avaliacao(){
         //NoExpr
         if(superRaiz->getTipo() == "sinal" ){
          //cout << "super raiz vai ser do tipo sinal" << endl;
          /*
            if(raiz.getFilhoEsquerda().getTipo() == "sinal" ){
               //raiz = analisarNos(raiz,raiz.getFilhoEsquerda());               
            }
            */
            //cout <<"superRaiz->getFilhoDireita()->getTipo() =" << superRaiz->getValor() << endl;
            if(superRaiz->getFilhoDireita()->getTipo() == "sinal"){
               //cout << "vai pra analisar2" << endl;
               analisarNos2(superRaiz,superRaiz->getFilhoDireita());
            }
         }
         //return raiz;
      }

      std::list<NoExpr*> buscarNoMaisFraco3(NoExpr* noPai, NoExpr* noFilho,NoExpr* novoNoRaiz ,std::list<NoExpr*> nosAntigos,NoExpr* noEsquerdaRaiz){
         NoExpr noEscolhido,*aux,*aux1,*aux2,*aux3,*auxVazio;
         //std::vector<NoExpr*> nosAntigos;
         //cout<< "buscarNoMaisFraco3 =" << novoNoRaiz->getValor() << endl; 
         
         if(noPai->getTipo() == "sinal" && noFilho->getTipo() == "sinal" && noFilho->getValor() == "+" && noPai->getValor() == "+" && novoNoRaiz->getValor() == "*" ){
            //cout << "no filho" << endl;
            noFilho->print();
            //cout << "no pai" << endl;
            noPai->print();
            if(noFilho->getFilhoEsquerda()->getOrdem() != noEsquerdaRaiz->getOrdem()){
               if(nosAntigos.size()!= 0 ){
                  nosAntigos.push_back(noFilho->getFilhoEsquerda());
               }
            }
            
            nosAntigos = buscarNoMaisFraco3(noFilho,noFilho->getFilhoDireita() ,novoNoRaiz,nosAntigos,noEsquerdaRaiz);
            if(nosAntigos.size() != 0 ){
               noFilho->setNoEsquerda(nosAntigos.front());
               nosAntigos.pop_front();
            }
            
            //nosAntigos.erase(nosAntigos.begin()+0);

            return nosAntigos;
         }else if(noPai->getTipo() == "sinal" && noFilho->getTipo() == "numero"){
            aux = novoNoRaiz->getFilhoEsquerda();   
            if(noFilho->getOrdem() > noEsquerdaRaiz->getOrdem() ){                              
               novoNoRaiz->setNoEsquerda(noFilho);
               novoNoRaiz->setNoDireita(aux);               
            }else{
               nosAntigos.pop_front();
               if(noPai->getFilhoEsquerda() == noFilho){
                  nosAntigos.push_back(noFilho);
                  novoNo = new NoExpr(noFilho->getValor(),noFilho->getTipo(),noFilho->getOrdem());  
               }else{
                  nosAntigos.push_back(noPai->getFilhoDireita());
                  nosAntigos.push_back(noPai->getFilhoEsquerda());                  
               }
               novoNoRaiz->setNoEsquerda(noEsquerdaRaiz);
               novoNoRaiz->setNoDireita(aux);
            }
            novoNoRaiz->print(); 
            if(noPai->getFilhoEsquerda() == noFilho){
               //cout<< "tentei inserir a esquerda" << endl;
               //nosAntigos.push_back(noFilho);
               noPai->setNoEsquerda(novoNoRaiz);
               //noEsquerdaRaiz->setValor(noFilho->getValor());
               //noEsquerdaRaiz->setOrdem(noFilho->getOrdem());
            }else{
               noPai->setNoDireita(novoNoRaiz);
            }
            return nosAntigos;
         }else if(noPai->getTipo() == "sinal" && noFilho->getTipo() == "sinal" && noFilho->getValor() == "*" && noPai->getValor() == "+"  && novoNoRaiz->getValor() == "*"){
            
            //cout << "nao deve entrar 1aqui" << endl;
            nosAntigos = buscarNoMaisFraco3(noPai,noPai->getFilhoEsquerda() ,novoNoRaiz,nosAntigos,noEsquerdaRaiz);   
            nosAntigos.clear();
            return nosAntigos;            
         }
      };


      std::vector<NoExpr*> buscarNoMaisFraco2(NoExpr* noPai, NoExpr* noFilho,NoExpr* novoNoRaiz ,std::vector<NoExpr*> nosAntigos){
         NoExpr noEscolhido,*aux,*aux1,*aux2,*aux3;
         //cout<< "buscarNoMaisFraco2 =" << novoNoRaiz->getValor() << endl; 
         //std::vector<NoExpr*> nosAntigos;
         if(noFilho->getTipo() == "sinal" && noFilho->getValor() == "+" && novoNoRaiz->getValor() == "*" ){
            //cout<< "buscarNoMaisFraco2 3teste" << endl; 
            aux = new NoExpr();
            if(noFilho->getFilhoEsquerda()->getTipo() == "vazio"){
               nosAntigos.push_back(novoNoRaiz->getFilhoEsquerda());
            }else{
               nosAntigos.push_back(noFilho->getFilhoEsquerda());
            }
            
            
            noFilho->setNoEsquerda(aux);
            //cout << "novo no aquii antes "<< novoNoRaiz->getValor() << endl;
            nosAntigos = buscarNoMaisFraco2(noFilho, noFilho->getFilhoDireita(),novoNoRaiz,nosAntigos);
            
            //if(noFilho->getFilhoEsquerda()->getTipo() != "vazio"){
               aux = nosAntigos.front();
               noFilho->setNoEsquerda(aux);
               nosAntigos.erase(nosAntigos.begin() + 0);
            //}
            return nosAntigos;
         }else if(noFilho->getTipo() == "numero"){
            //cout << "operou ? " << noFilho->getValor() << endl;
            //aux = noEscolhido.getFilhoDireita();
                        
            aux1 = noPai->getFilhoDireita();
            aux2 = noPai->getFilhoEsquerda();
            if(aux2->getTipo() == "vazio"){
               //cout << "aux2 eh vazio "<< endl;
               aux2 = nosAntigos.front();
               nosAntigos.erase(nosAntigos.begin()+0);
            }
            if(aux1->getOrdem() > aux2->getOrdem()){
               if(novoNoRaiz->getFilhoEsquerda()->getOrdem() < aux1->getOrdem() ){
                  nosAntigos.push_back(novoNoRaiz->getFilhoEsquerda());
                  novoNoRaiz->setNoEsquerda(aux1);
                  nosAntigos.push_back(aux2);               
               }else{
                  nosAntigos.push_back(aux1);               
                  nosAntigos.push_back(aux2);               
               }   
            }else{
               if(novoNoRaiz->getFilhoEsquerda()->getOrdem() < aux2->getOrdem() ){
                  nosAntigos.push_back(novoNoRaiz->getFilhoEsquerda());
                  novoNoRaiz->setNoEsquerda(aux2);
                  nosAntigos.push_back(aux1);
               }else{
                  nosAntigos.push_back(aux2);               
                  nosAntigos.push_back(aux1);               
               }
            }
                        
            noPai->setNoDireita(novoNoRaiz);
            //cout <<"no pai aqui " << noPai->getValor() << endl;
            //cout <<"no novo aqui " << novoNoRaiz->getValor() << endl;
            
            noPai->setNoEsquerda(nosAntigos.front());
            //cout << "front" << nosAntigos.front()->getValor() << endl;
            nosAntigos.erase(nosAntigos.begin()+0);
            //aux3 = novoNoRaiz.getFilhoEsquerda();
            //novoNoRaiz.setNoEsquerda(aux1);
            //novoNoRaiz.setNoDireita(aux3);
            //noPai.setNoDireita(novoNoRaiz);

            return nosAntigos;
         }else if(noFilho->getTipo() == "sinal" && noFilho->getValor() == "*" && novoNoRaiz->getValor() == "*" ){
            //cout << "deveria funcionar antes do if" << endl;
            //cout << "noPai->getValor()" << noPai->getValor() << endl;
            if(noPai->getFilhoEsquerda()->getTipo() == "numero"){
               //cout << "deveria funcionar" << endl;
               aux = noPai->getFilhoEsquerda();
               //cout << "ordem " << aux->getOrdem() << endl;
               //cout << "2ordem =" << novoNoRaiz->getFilhoEsquerda()->getOrdem() << endl;
               if(aux->getOrdem() > novoNoRaiz->getFilhoEsquerda()->getOrdem()){
                  nosAntigos.push_back(novoNoRaiz->getFilhoEsquerda());
                  novoNoRaiz->setNoEsquerda(aux);
               }else{
                  aux2 = new NoExpr();
                  noPai->setNoEsquerda(aux2);
                  nosAntigos.push_back(aux);   
               }               
               noPai->setNoEsquerda(novoNoRaiz);
            }else if(noPai->getFilhoEsquerda()->getTipo() == "vazio"){               
               //cout << "deveria funcioar no vazio"<< endl;
               //cout<< "nosAntigos.size()" << nosAntigos.size() << endl;
               noPai->setNoEsquerda(novoNoRaiz);   
            }else{
               //cout << "tem q passar aqui " << endl;
               nosAntigos = buscarNoMaisFraco2(noPai->getFilhoEsquerda(), noPai->getFilhoDireita(),novoNoRaiz,nosAntigos);   
               /*
               aux = nosAntigos.front();
               noFilho->setNoEsquerda(aux);
               nosAntigos.erase(nosAntigos.begin() + 0);
               */
               return nosAntigos;
            
            }

            return nosAntigos;
         }
      };

      void analisarNos2(NoExpr* noPai,NoExpr* noFilho){
         NoExpr *aux,*aux1,noEscolhido,*aux2,*aux3,*aux4;
         NoExpr *noTeste;
         std::list<NoExpr*> vetor;
         if(noPai->getTipo() == "sinal"){
            if(noFilho->getTipo() == "sinal"){                              
               if(noPai->getValor() == "*" && noFilho->getValor() == "+" ){
                  if(noFilho->getFilhoDireita()->getTipo() == "numero"){
                     //cout << "mais aqui";
                     aux = noPai->getFilhoEsquerda();
                     aux1 = noFilho->getFilhoDireita();                     
                     noFilho->setNoDireita(superRaiz);                     
                     superRaiz = noFilho;
                     noPai->setNoEsquerda(aux1);
                     noPai->setNoDireita(aux);
                     
                  }else {
                     //cout << "chegou aqui ?" << endl;
                    //cout << "superRaiz " << superRaiz->getValor() << endl;
                     //cout << "superRaiz filho esquerda " << superRaiz->getFilhoEsquerda()->getValor() << endl;
                     //cout << "nopai 11aqui = " << noPai->getValor() << endl;
                     //aux4 = superRaiz;
                     //aux = superRaiz->getFilhoEsquerda();                  
                     //aux3 = noFilho->getFilhoEsquerda();
                     
                     //superRaiz->setNoEsquerda(aux); //alterar,erro
                     //superRaiz->setNoDireita(aux);
                     //aux2 = new NoExpr();
                     //noFilho->setNoEsquerda(aux2);
                     aux4 = superRaiz;
                     superRaiz = noFilho;
                     //cout << "raiz "<< endl;
                     superRaiz->print();
                     //cout << "nopai 11aqui = " << noPai->getValor() << endl;
                     //cout << "nopai 11aqui no esquerda = " << noPai->getFilhoEsquerda()->getValor() << endl;
                     vetor.push_back(superRaiz->getFilhoEsquerda());
                     
                     //cout << "1size =" << vetor.size() << endl;
                     //vetor.erase(vetor.begin() + 0);
                     //cout << "2size =" << vetor.size() << endl;
                     vetor = buscarNoMaisFraco3(superRaiz , superRaiz->getFilhoDireita() ,aux4,vetor,superRaiz->getFilhoEsquerda());
                     //cout << "vetor size= " << vetor.size() << endl;
                     //aux = vetor.front();
                     //aux->print();
                     if(vetor.size()!= 0){
                        superRaiz->setNoEsquerda(vetor.back());
                        vetor.pop_front();
                     }else{
                        superRaiz->setNoEsquerda(novoNo);
                     }
                  }
               }
            }
         }

         
         //return raiz;
      };   

      void printarArvorePrefixa3(NoExpr *novo){
         NoExpr *no;
         if(novo != 0 ){
            //no = novo;
            //cout << " chave " << novo->getValor() << endl;   
            printarArvorePrefixa3(novo->getFilhoDireita());
            printarArvorePrefixa3(novo->getFilhoEsquerda());  

         }
               
      };

      void printarArvorePrefixa4(NoExpr *novo){         
         if(novo != 0 ){
            //no = novo;
            //cout << " chave " << novo->getValor() << endl;   
            printarArvorePrefixa4(novo->getFilhoEsquerda());
            printarArvorePrefixa4(novo->getFilhoDireita());  

         }
               
      };
      

      void printarArvorePrefixa7(){         
         std::string subArvore = "";
         subArvorePrefixa = "";
         
         prefixaMultiplicacao2(subArvore);            
                  
         prefixaSoma2(subArvore);   
           
         
         //cout << " arvore toda prefixa "<< subArvore << endl;
         //cout << " subArvorePrefixa toda prefixa "<< subArvorePrefixa << endl;
      };

      void prefixaSoma2(std::string subArvore){
         bool fecharColchete = false;
         bool adicionarMaisUmOperador = false;
         bool adicionarDoisOperadores = false;
         for (int i = 0; i < vetorSoma.size(); ++i)
         {
            if(subArvorePrefixa.empty()){
               if(vetorSoma.at(i)->getFilhoDireita() != 0 && vetorSoma.at(i)->getFilhoEsquerda() != 0){
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + vetorSoma.at(i)->getFilhoEsquerda()->getValor() + vetorSoma.at(i)->getFilhoDireita()->getValor() +"]";   
               }else if(vetorSoma.at(i)->getFilhoDireita() != 0){
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + vetorSoma.at(i)->getFilhoDireita()->getValor() +"]";   
                  adicionarMaisUmOperador = true;
               }else if(vetorSoma.at(i)->getFilhoEsquerda() != 0){
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + vetorSoma.at(i)->getFilhoEsquerda()->getValor() +"]";   
                  adicionarMaisUmOperador = true;
               }
            }else if(vetorSoma.at(i)->getFilhoDireita() != 0 && vetorSoma.at(i)->getFilhoEsquerda() != 0){
               
               if(adicionarMaisUmOperador){
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + subArvorePrefixa + vetorSoma.at(i)->getFilhoEsquerda()->getValor() +"]" + vetorSoma.at(i)->getFilhoDireita()->getValor() + "]";      
                  adicionarMaisUmOperador = false;
               }else{
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + subArvorePrefixa + vetorSoma.at(i)->getFilhoEsquerda()->getValor() +"]" + vetorSoma.at(i)->getFilhoDireita()->getValor() ;      
               }
               
            }else if(vetorSoma.at(i)->getFilhoEsquerda() != 0){
               if(adicionarMaisUmOperador){
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + subArvorePrefixa + vetorSoma.at(i)->getFilhoEsquerda()->getValor() +"]" ;      
                  adicionarMaisUmOperador = false;
               }else{
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + subArvorePrefixa + vetorSoma.at(i)->getFilhoEsquerda()->getValor() +"]" ;      
               }
            }else if(vetorSoma.at(i)->getFilhoDireita() != 0){
               if(adicionarMaisUmOperador){
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + subArvorePrefixa + vetorSoma.at(i)->getFilhoDireita()->getValor() +"]" ;      
                  adicionarMaisUmOperador = false;
               }else{
                  subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + subArvorePrefixa + vetorSoma.at(i)->getFilhoDireita()->getValor() +"]" ;      
               }
            }else{
               subArvorePrefixa = "["+ vetorSoma.at(i)->getValor() + subArvorePrefixa +"]" ;      
            }
            

         }
         
      };


      void prefixaMultiplicacao2(std::string subArvore){         
         //cout << "size prefixa multiplicacao "<< vetorMultiplicacao.size() << endl;
         string fator;
         bool raizCompleta = true;
         for (int i = 0; i < vetorMultiplicacao.size(); ++i)
         {
           
            if(vetorMultiplicacao.at(i)->getFilhoDireita() !=  0 && vetorMultiplicacao.at(i)->getFilhoEsquerda() ){
               subArvorePrefixa = subArvorePrefixa + "[" + vetorMultiplicacao.at(i)->getValor() + vetorMultiplicacao.at(i)->getFilhoEsquerda()->getValor() + vetorMultiplicacao.at(i)->getFilhoDireita()->getValor() + "]";
            }else if(vetorMultiplicacao.at(i)->getFilhoEsquerda() ==  0 && vetorMultiplicacao.at(i)->getFilhoDireita() !=  0 ){
               subArvorePrefixa =  + "[" + vetorMultiplicacao.at(i)->getValor() + subArvorePrefixa+ vetorMultiplicacao.at(i)->getFilhoDireita()->getValor() + "]";       
            }else if(vetorMultiplicacao.at(i)->getFilhoEsquerda() !=  0 && vetorMultiplicacao.at(i)->getFilhoDireita() !=  0 ){
               subArvorePrefixa =  + "[" + vetorMultiplicacao.at(i)->getValor() + subArvorePrefixa+ vetorMultiplicacao.at(i)->getFilhoEsquerda()->getValor() + "]";       
            }
         
         }   
         //return subArvore;
      };


      void printarArvorePrefixa6(){         
         std::string subArvore = "";
         subArvorePrefixa = "";
         if(vetorMultiplicacao.size() > 0){
            subArvore = prefixaMultiplicacao(subArvore);
            qtdFolhasVisitadas = 1;
         }
         
         if(vetorSoma.size() > 0){
            subArvore = prefixaSoma(subArvore);   
            qtdFolhasVisitadas = 0;
         }
         
         //cout << " arvore toda prefixa "<< subArvore << endl;
         //cout << " subArvorePrefixa toda prefixa "<< subArvorePrefixa << endl;
      };

      std::string prefixaSoma(std::string subArvore){
         
         //cout << "size prefixa soma "<< vetorSoma.size() << endl;
         
         if( vetorMultiplicacao.size() > 0 ){
               for (int i = 0; i < vetorSoma.size(); ++i)
            //for (int i = vetorSoma.size() - 1; i > -1; --i)
               {                              
                  if(vetorSoma.at(i)->getFilhoEsquerda() == 0 && vetorSoma.at(i)->getFilhoDireita() == 0){
                     
                     
                     if( i == 0  ){
                        subArvorePrefixa = "[" + vetorSoma.at(i)->getValor() + subArvorePrefixa + "]";
                        qtdFolhasVisitadas = 1;
                     }else if(vetorSoma.at(i-1)->getFilhoEsquerda() == 0 || vetorSoma.at(i-1)->getFilhoDireita() == 0 ){
                        subArvorePrefixa = "[" + vetorSoma.at(i)->getValor() + subArvorePrefixa ;
                        qtdFolhasVisitadas = 1;
                     }else{ 
                        subArvorePrefixa = "[" + vetorSoma.at(i)->getValor() + subArvorePrefixa + "]";
                        qtdFolhasVisitadas = 1;
                     
                     }
                  }else{
                     subArvore = subArvore + percurso(vetorSoma.at(i));
                  }

               //subArvore = subArvore + percurso(vetorSoma.at(i));                  
               ////subArvorePrefixa =  subArvorePrefixa + "]";
               }  
         }else{
            //for (int i = 0; i < vetorSoma.size(); ++i)
            for (int i = vetorSoma.size() - 1; i > -1; --i)
               {            
                  
                  if(vetorSoma.at(i)->getFilhoEsquerda() == 0 && vetorSoma.at(i)->getFilhoDireita() == 0){
                     subArvorePrefixa = "[" + vetorSoma.at(i)->getValor() + subArvorePrefixa + "]";
                     qtdFolhasVisitadas = 1;
                  }else{
                     subArvore = subArvore + percurso(vetorSoma.at(i));
                  }

               //subArvore = subArvore + percurso(vetorSoma.at(i));                  
               ////subArvorePrefixa =  subArvorePrefixa + "]";
               }
         }
         
         if(qtdFolhasVisitadas < 2){
            //subArvorePrefixa =  subArvorePrefixa + "]";  
         }
         //cout << "da prefixa soma subArvore=>" << subArvore << endl;
         
         return subArvore;
      };



      std::string prefixaMultiplicacao(std::string subArvore){         
         //cout << "size prefixa multiplicacao "<< vetorMultiplicacao.size() << endl;
         string fator;
         bool raizCompleta = true;
         for (int i = vetorMultiplicacao.size()- 1; i > -1; --i)
         {
            //subArvorePrefixa = "[" + subArvorePrefixa;
            //
            //subArvore = subArvore + percurso(vetorMultiplicacao.at(i));

            if( i == vetorMultiplicacao.size()- 1 && vetorMultiplicacao.at(i)->getFilhoEsquerda() != 0 && vetorMultiplicacao.at(i)->getFilhoDireita() != 0 ){
               raizCompleta = true;
            }else if( raizCompleta && vetorMultiplicacao.at(i)->getFilhoEsquerda() != 0 && vetorMultiplicacao.at(i)->getFilhoDireita() != 0 ){
               raizCompleta = true;
            }else if(raizCompleta && (vetorMultiplicacao.at(i)->getFilhoEsquerda() == 0 || vetorMultiplicacao.at(i)->getFilhoDireita() == 0) ){
               raizCompleta = false;
            }
            
            if(raizCompleta){
               subArvorePrefixa = subArvorePrefixa +  "[" + vetorMultiplicacao.at(i)->getValor() + vetorMultiplicacao.at(i)->getFilhoEsquerda()->getValor() + vetorMultiplicacao.at(i)->getFilhoDireita()->getValor()  + "]";
               qtdFolhasVisitadas = 1;
            }else{
               subArvore = subArvore + percurso(vetorMultiplicacao.at(i));
            }

            //subArvorePrefixa =  subArvorePrefixa + "]";         
         }
         if(qtdFolhasVisitadas < 2){
            //subArvorePrefixa =  subArvorePrefixa + "]";  
         }
         //cout << "da prefixa multiplicacao subArvore=>" << subArvore << endl;
         
         return subArvore;
      };

      std::string percurso(NoExpr *novo){
         std::string subArvore;
         if(novo != 0 ){
            //no = novo;
            subArvore = novo->getValor();
            //cout << " chave " << novo->getValor() << endl;   
            
            if(novo->getTipo() == "sinal"){
               
               subArvorePrefixa = subArvore + subArvorePrefixa;
               subArvorePrefixa = "[" + subArvorePrefixa;
            }else{
               subArvorePrefixa = subArvorePrefixa + subArvore;
               qtdFolhasVisitadas++;
               if(qtdFolhasVisitadas == 2){
                  subArvorePrefixa =  subArvorePrefixa + "]";  
                  qtdFolhasVisitadas = 1;
               }

               //subArvore = percurso(novo->getFilhoEsquerda()) + subArvore;
               //subArvore = percurso(novo->getFilhoDireita()) + subArvore;              
            }

            percurso(novo->getFilhoEsquerda());
            percurso(novo->getFilhoDireita());                 

            //subArvorePrefixa = subArvorePrefixa + "]";
            
            
            return subArvore;
         }
         return "";
      };
      
      void printarArvorePrefixa5(NoExpr *novo){         
         if(novo != 0 ){
            //no = novo;
            //cout << " chave " << novo->getValor() << endl;   
            printarArvorePrefixa5(novo->getFilhoEsquerda());
            printarArvorePrefixa5(novo->getFilhoDireita());  

         }
               
      };

         /*
      void iniciarArvore(){
         
         NoExpr raiz;
         
         cout << "tamanho " << this->listaExpressoes.size() << endl;
         int qtdSinal = 0;
         int qtdNumero = 0;         
         bool primeiroCaso = false;
         int j = 4;
         
         for (int i = 0; i < this->listaExpressoes.size(); ++i)
         {
            
            raiz = arvore(raiz,this->listaExpressoes.at(i));
            if(i == j){
               cout << "chave raiz = " << raiz.getValor() << endl;
               raiz = avaliacao(raiz);
               j = j + 2;
            } 
            
         }
         //printarArvorePrefixa2(raiz);
      };
      */
      
      /*
      NoExpr avaliacao(NoExpr raiz){
         if(raiz.getTipo() == "sinal" ){
        */    /*
            if(raiz.getFilhoEsquerda().getTipo() == "sinal" ){
               //raiz = analisarNos(raiz,raiz.getFilhoEsquerda());               
            }
   */
          /*  if(raiz.getFilhoDireita().getTipo() == "sinal"){
               raiz = analisarNos(raiz,raiz.getFilhoDireita());
            }
         }
         return raiz;
      }
*/
      /*
      NoExpr buscarNoMaisFraco(NoExpr noPai, NoExpr noFilho,NoExpr novoNoRaiz){
         NoExpr noEscolhido,aux,aux1,aux3;
         if(noFilho.getTipo() == "sinal" && noFilho.getValor() == "+" && novoNoRaiz.getValor() == "*" ){
            noEscolhido = buscarNoMaisFraco(noFilho, noFilho.getFilhoDireita(),novoNoRaiz);
            if(noEscolhido.getTipo() == "numero"){
               noPai.setNoEsquerda(noEscolhido);
            }
            return noPai;
         }else if(noFilho.getTipo() == "numero"){
            //aux = noEscolhido.getFilhoDireita();
            aux1 = noPai.getFilhoDireita();
            aux3 = novoNoRaiz.getFilhoEsquerda();
            novoNoRaiz.setNoEsquerda(aux1);
            novoNoRaiz.setNoDireita(aux3);
            noPai.setNoDireita(novoNoRaiz);

            return aux1;
         }else if(noFilho.getTipo() == "sinal" && noFilho.getValor() == "*" && novoNoRaiz.getValor() == "*" ){
            return noPai;
         }
      };
      */
      /*
      NoExpr buscarNoMaisFraco2(NoExpr &noPai, NoExpr &noFilho,NoExpr &novoNoRaiz){
         NoExpr noEscolhido,aux,aux1,aux3;
         if(noFilho.getTipo() == "sinal" && noFilho.getValor() == "+" && novoNoRaiz.getValor() == "*" ){
            aux = noFilho.getFilhoDireita();
            return buscarNoMaisFraco2(noFilho,aux,novoNoRaiz);            
         }else if(noFilho.getTipo() == "numero"){            
            return noPai;
         }else if(noFilho.getTipo() == "sinal" && noFilho.getValor() == "*" && novoNoRaiz.getValor() == "*" ){
            return noPai;
         }
      };
      */
      /*
      NoExpr analisarNos(NoExpr raiz,NoExpr noFilho){
         NoExpr aux,aux1,noEscolhido,aux3,aux4;
         NoExpr *noTeste;
         if(raiz.getTipo() == "sinal"){
            if(noFilho.getTipo() == "sinal"){                              
               if(raiz.getValor() == "*" && noFilho.getValor() == "+" ){
                  if(noFilho.getFilhoDireita().getTipo() == "numero"){
                     aux = raiz.getFilhoEsquerda();
                     aux1 = noFilho.getFilhoDireita();
                     raiz.setNoEsquerda(aux1);
                     raiz.setNoDireita(aux);  
                     noFilho.setNoDireita(raiz);
                     cout << "raiz aqui é novo" << endl;
                     return noFilho;
                  }else if(noFilho.getFilhoDireita().getTipo() == "sinal"){
                     cout << "no raiz =" << raiz.getValor() << endl;  
                     cout << "filho esquerda" << raiz.getFilhoEsquerda().getValor() << endl;
                     std::string valor,valor1;
                     aux = raiz.getFilhoEsquerda();
                     cout << "auxi tipo" << aux.getTipo() << endl;
                     aux1 = noFilho.getFilhoEsquerda();
                     //raiz.setNoEsquerda(aux1);
                     //raiz.setNoEsquerda(aux1);
                     //raiz.setNoDireita(aux);
                     aux = noFilho.getFilhoDireita();
                     noEscolhido = buscarNoMaisFraco2(noFilho,aux,raiz);
                     cout << "valor escolhido " << noEscolhido.getValor() << endl;
                     noTeste = &noEscolhido;
                     //noTeste->setNoDireita(raiz);

                     //cout << valor escolhido filho a esquerda " << noEscolhido.getFilhoEsquerda().getValor() << endl;
                     //aux3 = noEscolhido.getFilhoDireita();
                     //aux4 = noEscolhido.getFilhoEsquerda();
                     //valor = noEscolhido.getValor();
                     //valor1 = raiz.getValor();
                     //raiz.setValor(valor);
                     //raiz.setNoEsquerda(aux4);
                     //raiz.setNoEsquerda(aux4);
                     //raiz.setNoDireita(noFilho);
                     //noFilho.setNoEsquerda(aux4);
                     //noEscolhido.setValor("*");
                     ///noEscolhido.setNoEsquerda(aux1);
                     //noEscolhido.setNoDireita(aux);
                     //noFilho.setNoDireita(noEscolhido);
                     //noFilho.setNoEsquerda(aux3);
                     //noEscolhido.setNoDireita(raiz);
                     //noEscolhido.setNoDireita(raiz);
                     //cout << "2no raiz =" << raiz.getValor() << endl;  
                     //cout << "2filho esquerda" << raiz.getFilhoEsquerda().getValor() << endl;
                     //cout << "2filho direita" << raiz.getFilhoDireita().getValor() << endl;
                     //cout <<"size no Escolhido = " << noEscolhido.getFilhos().size() << endl;
                     //raiz.setValor("novo");
                     //raiz = noFilho;
                     return noFilho;
                  }
               }            
            }
         }
         return raiz;
      };
      */
      /*
      NoExpr arvore(NoExpr raiz, NoExpr novoNo){
         cout << "valor aquiii " << novoNo.getValor() << endl; 
         if(raiz.getTipo() == "vazio"){
            return novoNo;
         }else if(novoNo.getTipo() == "sinal"){
            if(raiz.getTipo() == "numero"){
               novoNo.setNoEsquerda(raiz); 
               return novoNo;            
            }else if(raiz.getTipo() == "sinal"){
               novoNo.setNoDireita(raiz);
               return novoNo;
            }
            
         }else if(novoNo.getTipo() == "numero"){
            if(raiz.getTipo() == "sinal" && raiz.getFilhoEsquerda().getTipo() == "vazio" ){
               raiz.setNoEsquerda(novoNo);
               return raiz;
            }

            raiz.setNoDireita(novoNo);
            return raiz;
         }
         
      };
         */
      
      /*
      void printarArvorePrefixa2(NoExpr raiz){
         
         if(raiz.getTipo() != "vazio"){
            cout << " chave " << raiz.getValor() << endl;   
            printarArvorePrefixa2(raiz.getFilhoDireita());
            printarArvorePrefixa2(raiz.getFilhoEsquerda());  

         }
               
      };   
   */

      /*
      void printarArvorePrefixa(NoExpr raiz){
         
         if(raiz.getTipo() != "vazio"){
            
            

            if(raiz.getFilhoDireita().getTipo() == "sinal"){
               cout << " chave " << raiz.getValor() << endl;   
               printarArvorePrefixa(raiz.getFilhoDireita());
               printarArvorePrefixa(raiz.getFilhoEsquerda());  
            }else{
               cout << " chave " << raiz.getValor() << endl;   
               printarArvorePrefixa(raiz.getFilhoEsquerda()); 
               printarArvorePrefixa(raiz.getFilhoDireita());
            }
            
            
                      
            
         }
               
      }
      */
      std::string arvoreExpr5(){
        // NoExpr noExprSinal;
         //NoExpr noExprNumero1;
         //NoExpr noExprNumero2;
         iniciarArvore5();
         //iniciarArvore4();
         //iniciarArvore3();
         //prefixoSimbolos();

         //NoExpr *noExprNumero1P = 0 , *noExprNumero2P = 0;
         //cout <<"sizee "<< this->listaExpressoes.size() << endl;
         //noExprSinal = this->listaExpressoes.at(1);
         //noExprNumero1 = this->listaExpressoes.at(0);
         //noExprNumero2 = this->listaExpressoes.at(2);
         //noExprSinal.setNoEsquerda(noExprNumero1);
         //noExprSinal.setNoDireita(noExprNumero2);
         
         //cout << "filho esquerda " << noExprSinal.getFilhoEsquerda().getValor() << "  chave " << noExprSinal.getValor() << " filho direita " << noExprSinal.getFilhoDireita().getValor()  << endl;

         return "acabou aqui";
      };


      void avaliarPilha(std::string sinal){
         /*
         stack<std::string> pilhaAux;
         std::string op;
         bool inserirFalse = false , eliminarUltimo = false,inverterPilha = false,primeiroSinal=false,acharLocalSinal = false;
         std::string top0 = this->pilhaNumeros.top();
         

         while(!this->pilhaNumeros.empty()){
            op = this->pilhaNumeros.top();
            if(op == "+" || op == "*"){
               if(!primeiroSinal){
                  primeiroSinal = true;
                  if(op == "+" && sinal == "*"){
                     inverterPilha = true;
                  }else if(op == "*" && sinal == "+"){
                     acharLocalSinal = true;
                  }
               }
               if(sinal == "+" || sinal == "*"){
                  this->pilhaNumeros.push(sinal);
                  if(sinal == "*"){
                     inserirFalse = true;   
                  }
                  
                  break;
               }
            }else{
               pilhaAux.push(op);
               this->pilhaNumeros.pop();
            }
         }

         if(this->pilhaNumeros.empty()){
            cout << "vazioooo" << endl;
            this->pilhaNumeros.push(sinal);
         }
         
         if(acharLocalSinal){
            stack<std::string> pilhaAux3;
            std::string top;
            while(!this->pilhaNumeros.empty()){
               top = this->pilhaNumeros.top();      
               if(sinal == "+" && top == "*"){
                  pilhaAux3.push(this->pilhaNumeros.top());
               }else if(){
                  
               }
               
               this->pilhaNumeros.pop();
            }         
         }

         if(inverterPilha){
            stack<std::string> pilhaAux2;         
            while(!pilhaAux.empty()){
               pilhaAux2.push(pilhaAux.top());
               pilhaAux.pop();
            }
            pilhaAux = pilhaAux2;
         }
         
         while(!pilhaAux.empty()){
            
            if(inserirFalse){
               inserirFalse = false;
               this->pilhaNumeros.push("%");
               //this->pilhaNumeros.push(top0);
               //pilhaAux.pop();   
               eliminarUltimo = true;
            }
            this->pilhaNumeros.push(pilhaAux.top());
            pilhaAux.pop();
         }
         if(eliminarUltimo){
            //this->pilhaNumeros.pop();   
         }
      */
      };

      std::string substituir(std::string subArvore){
         std::string simbolo;
         int tamanho = 0 ;
         //cout << "tamanho$ " << this->numeros.size() << endl;
         for (int i = 0; i < this->numeros.size(); ++i)
         {
            std::stringstream Resultado;      
            Resultado << i;   
            simbolo = "$"+Resultado.str();
            tamanho = simbolo.length();
            //Resultado.erase();
            //cout << "tamanho$ " << tamanho << endl;  
            //cout << "simbolo$ " << simbolo << endl;  
            std::size_t found = subArvore.find(simbolo);
            if (found!=std::string::npos){
               //std::string teste = subArvore;
               subArvore.replace(found,2,this->numeros.at(i));
               //retornoDecimalExpr1.setSubArvore(teste);   
            }

         }
         return subArvore;
      }

   	void executar(){
   			
            //cout << this->noRaiz.getNome() << endl;
   			//cout << "t =" << this->streamTokens.size() << endl;
   			Retorno retorno;
   			std::vector<std::map<std::string, std::string> > escopos;
   			if(this->streamTokens.size() > 1){
   				//noRaiz2 = this->fabricaNo.noRaiz();
   				retorno = proximaProducao(this->noRaiz,this->noRaiz,0,0,escopos);
   				if(retorno.getResultado() == 1){
   					retorno.setSubArvore("[program "+retorno.getSubArvore()+"]");
   				}
   			}else{
   				retorno.setSubArvore("[program ]");
   			}
   			//fabricaNo.decvar();
   			//fabricaNo.decfunc()
   			//cout << "subArvore do expr3 = " << arvoreExpr5() << endl;
            arvoreAst = retorno.getSubArvore();
            cout << "subArvore final " << retorno.getSubArvore() << endl;
   		};

   		// 0 = falso
   		// 1 = verdade
   		// 2 = nao sei ainda
   		Retorno proximaProducao(No no, No noPai, int indiceStream, int indiceToken, std::vector<std::map<std::string, std::string> > escopos){
   			Retorno retorno("0",0,"escopo",indiceStream,"",indiceToken);
   			//Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
            std::string subArvore = "";   			
   			//cout << "proximaProducao" << endl;
   			//cout << "no.getNome() " << no.getNome() << endl;
   			if( this->fabricaNo.simboloNaoTerminal( no.getNome() )){   				
   				//cout << "if" << endl;
   				if( no.getNome() == "program" ){   					
   					bool bDecvar = true,bDecfunc = true;
   					//std::cout << "aquii=" << this->streamTokens.size() << std::endl;
   					std::vector<Retorno> retornosDecVar;
   					std::vector<Retorno> retornosDecFunc;
   					Retorno retornoDecVar,retornoDecFunc;

   					while( (indiceStream < this->streamTokens.size() ) && ( bDecvar == true || bDecfunc == true ) ){
   						//std::cout << "loop" << std::endl;
   						
   						retornoDecVar = proximaProducao(this->fabricaNo.solicitarNo("decvar"),no,indiceStream,indiceToken,escopos);
   						
   						/*
   						cout << "resultado aux " << retornoDecVar.getResultado() << endl;
   						int e;
   						cout << "digite aqui o numero1" <<" indiceStream "<< indiceStream << endl;
   						cin >> e;
						*/

   						if(retornoDecVar.getResultado() == 1){
   							bDecvar = true;
   							indiceStream = retornoDecVar.getUltimoIndice() ;
   							   							
   							retornoDecVar.setSubArvore("[decvar "+ retornoDecVar.getSubArvore()+"]");
   							retornosDecVar.push_back(retornoDecVar);
   						}else{
   							bDecvar = false;
   						}

   						retornoDecFunc = proximaProducao(this->fabricaNo.solicitarNo("decfunc"),no,indiceStream,indiceToken,escopos);
   						if( retornoDecFunc.getResultado() == 1 ){
   							bDecfunc = true;
   							indiceStream = retornoDecFunc.getUltimoIndice() ;
   							//cout << "allaaaaaaaaa" << endl;
                        retornoDecFunc.setSubArvore("[decfunc "+ retornoDecFunc.getSubArvore()+"]");
   							retornosDecFunc.push_back(retornoDecFunc);
   						}else{
							  bDecfunc = false;   							
   						}


//bDecfunc = false;
//bDecvar = false;   							
   					}
   					
   					std::string subArvoreDecFunc = "";	
   					for (int i = 0; i < retornosDecFunc.size(); ++i){
 						subArvoreDecFunc = subArvoreDecFunc +retornosDecFunc.at(i).getSubArvore();  						
   						retorno = retornosDecFunc.at(i);
   					}

   					std::string subArvoreDecVar = "";	
   					for (int i = 0; i < retornosDecVar.size(); ++i){
 						subArvoreDecVar = subArvoreDecVar + retornosDecVar.at(i).getSubArvore();  						
   						retorno = retornosDecVar.at(i);
   					}

   					//retornoDecVar.setSubArvore(subArvore);
   					retorno.setSubArvore(subArvoreDecFunc + subArvoreDecVar );
   					
   					//return retornoAux;
   				}else if( no.getNome() == "type" ){
   					Retorno retornoInt,retornoVoid;
   					//cout << "tipo type" << " indiceStream "  <<indiceStream<< endl;
   					
   					retornoInt = proximaProducao(this->fabricaNo.solicitarNo("int"), no, indiceStream,0,escopos);
   					if( retornoInt.getResultado() == 1 ){   										
   						indiceStream = retornoInt.getUltimoIndice();
   						//return retorno;
   					}else{
   						retornoVoid = proximaProducao(this->fabricaNo.solicitarNo("void"), no, indiceStream,1,escopos);
   						if(retornoVoid.getResultado() == 1){
   							indiceStream = retornoVoid.getUltimoIndice();
   						}
   						//return retornoVoid;
   					}	
   					if(retornoInt.getResultado() == 1){
   						retorno = retornoInt;
   					}else if(retornoVoid.getResultado() == 1){
   						retorno = retornoVoid;
   					}	   							
   				}else if( no.getNome() == "decvar" ){
   					//cout << "chegou no decvar2" << endl;
   					Retorno retornoType,retornoId,retornoIgual,retornoExpr,retornoPontoV,retornoExprPontoV;
   					
   					//int j = 3;
   					//int i;
   					
   					//cout << "j" << j << endl;
   					//for(i=0;i<3;i++){
   						
   						//cout << "i" << i << endl;
					retornoType = proximaProducao(fabricaNo.solicitarNo("type"), no, indiceStream, indiceToken, escopos);
					
					
					if(retornoType.getResultado() == 1){
						

						indiceStream = retornoType.getUltimoIndice();						
						retornoId = proximaProducao(fabricaNo.solicitarNo("id"), no, indiceStream, indiceToken, escopos);
						

						if(retornoId.getResultado() == 1){	
							
                     indiceStream = retornoId.getUltimoIndice();
							retornoPontoV = proximaProducao(fabricaNo.solicitarNo(";"), no, indiceStream, indiceToken, escopos);
							//cout << "xx indiceStream = " << indiceStream << endl;
							
                     if(retornoPontoV.getResultado() == 1) {
								indiceStream = retornoPontoV.getUltimoIndice();
				
							}else{

								retornoIgual = proximaProducao(fabricaNo.solicitarNo("="), no, indiceStream, indiceToken, escopos);				
								
								if(retornoIgual.getResultado() == 1){
									indiceStream = retornoIgual.getUltimoIndice();
									retornoExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);	
									//cout <<"este retorno eh muito importante " << retornoExpr.getResultado() << endl;
                           if(retornoExpr.getResultado() == 1){										
                              //retornoExpr.setSubArvore( substituir(retornoExpr.getSubArvore()) );
                              indiceStream = retornoExpr.getUltimoIndice();		
										retornoExprPontoV = proximaProducao(fabricaNo.solicitarNo(";"), no, indiceStream, indiceToken, escopos);		
										//cout << "retornoExprPontoV: " <<retornoExprPontoV.getResultado() << endl;
                              if(retornoExprPontoV.getResultado() == 1){
											indiceStream = retornoExprPontoV.getUltimoIndice();
										}
									}
								}
							}
						}
					}

					if(retornoType.getResultado() == 1 && retornoId.getResultado() == 1 && retornoPontoV.getResultado() == 1){
						retornoPontoV.setSubArvore(retornoType.getSubArvore() + retornoId.getSubArvore() + retornoPontoV.getSubArvore());
						//cout <<" 123retornoPontoV.getUltimoIndice()= "<< retornoPontoV.getUltimoIndice() << endl;
						retorno = retornoPontoV;					
					}else if(retornoType.getResultado() == 1 && retornoId.getResultado() == 1 && retornoIgual.getResultado() == 1 && retornoExpr.getResultado() == 1 && retornoExprPontoV.getResultado() == 1){
						//cout << "auto lááá " << retornoExpr.getSubArvore() << endl;
						retornoExprPontoV.setSubArvore( retornoType.getSubArvore() + retornoId.getSubArvore() +" "+ retornoExpr.getSubArvore() + retornoExprPontoV.getSubArvore());
						retorno = retornoExprPontoV;
						
						//return retorno;
					}

					   						   					
   				}else if( no.getNome() == "expr" ){
   					//cout << "to no expr" << endl;
                  subArvore = "";
   					Retorno retornoMenos,retornoExpr,retornoExpr1;
                  Retorno retornoUnop,retornoUnopExpr,RetornoUnopExpr1;
                  Retorno retornoPaAberto,retornoPaAbertoExpr,retornoPaFechado,retornoPaAbertoExprExpr1;
                  Retorno retornoFuncall,retornoFuncallExpr1;
                  Retorno retornoDecimal,retornoDecimalExpr1;
                  Retorno retornoId,retornoIdExpr1;
                  retornoMenos = proximaProducao(fabricaNo.solicitarNo("--"), no, indiceStream, indiceToken, escopos);
                  if(retornoMenos.getResultado() == 1){
                     indiceStream = retornoMenos.getUltimoIndice();
                     retornoExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);
                     if(retornoExpr.getResultado() == 1){
                        indiceStream = retornoExpr.getUltimoIndice();
                        retornoExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                        if(retornoExpr1.getResultado() == 1){
                           indiceStream = retornoExpr1.getUltimoIndice();
                        }
                     }
                  }else{
                     retornoUnop = proximaProducao(fabricaNo.solicitarNo("unop"), no, indiceStream, indiceToken, escopos);
                     //cout << "resultado unop " << retornoUnop.getResultado() << endl;
                     if(retornoUnop.getResultado() == 1){
                        indiceStream = retornoUnop.getUltimoIndice();
                        retornoUnopExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);  
                        if(retornoUnopExpr.getResultado() == 1){
                           indiceStream = retornoUnopExpr.getUltimoIndice();   
                           RetornoUnopExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);  
                           if(RetornoUnopExpr1.getResultado() == 1){
                              indiceStream = RetornoUnopExpr1.getUltimoIndice();         
                           }
                        }
                     }else{
                        retornoPaAberto = proximaProducao(fabricaNo.solicitarNo("("), no, indiceStream, indiceToken, escopos);  
                        if(retornoPaAberto.getResultado() == 1){
                           indiceStream = retornoPaAberto.getUltimoIndice();   
                           retornoPaAbertoExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);
                           if(retornoPaAbertoExpr.getResultado() == 1){
                              indiceStream = retornoPaAbertoExpr.getUltimoIndice();      
                              retornoPaFechado = proximaProducao(fabricaNo.solicitarNo(")"), no, indiceStream, indiceToken, escopos);
                              if(retornoPaFechado.getResultado() == 1){
                                 indiceStream = retornoPaFechado.getUltimoIndice();         
                                 retornoPaAbertoExprExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                                 if(retornoPaAbertoExprExpr1.getResultado() == 1){
                                    indiceStream = retornoPaAbertoExprExpr1.getUltimoIndice();         
                                 }
                              }
                           }
                        }else{
                           //cout << "teste do funcall" << endl;
                           //retornoFuncall,retornoFuncallExpr1
                           retornoFuncall = proximaProducao(fabricaNo.solicitarNo("funcall"), no, indiceStream, indiceToken, escopos);
                           //cout << "retorno teste do funcall: " << retornoFuncall.getResultado() << endl;
                           if(retornoFuncall.getResultado() == 1){
                              indiceStream = retornoFuncall.getUltimoIndice();            
                              retornoFuncallExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                              //cout << "retornoFuncallExpr1.getResultado(): " << retornoFuncallExpr1.getResultado() << endl; 
                              if(retornoFuncallExpr1.getResultado() == 1){ 
                                 indiceStream = retornoFuncallExpr1.getUltimoIndice();
                              }
                           }else{
                              //retornoDecimal,retornoDecimalExpr1
                              retornoDecimal = proximaProducao(fabricaNo.solicitarNo("decimal"), no, indiceStream, indiceToken, escopos);
                              if(retornoDecimal.getResultado() == 1){
                                 indiceStream = retornoDecimal.getUltimoIndice();   
                                 retornoDecimalExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                                 if(retornoDecimalExpr1.getResultado() == 1){
                                    indiceStream = retornoDecimalExpr1.getUltimoIndice();
                                 }
                              }else{
                                 //retornoId,retornoIdExpr1;
                                 retornoId = proximaProducao(fabricaNo.solicitarNo("id"), no, indiceStream, indiceToken, escopos);
                                 if(retornoId.getResultado() == 1){
                                    indiceStream = retornoId.getUltimoIndice();      
                                    retornoIdExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                                    if(retornoIdExpr1.getResultado() == 1){
                                       indiceStream = retornoIdExpr1.getUltimoIndice();      
                                    }
                                 }
                              }
                           }
                        }

                     }
                  }                  

   					if(retornoMenos.getResultado() == 1 && retornoExpr.getResultado() == 1 ){
   						if(retornoExpr1.getResultado() == 1){
                        retornoExpr1.setSubArvore(retornoMenos.getSubArvore() + retornoExpr.getSubArvore() + retornoExpr1.getSubArvore() );
                        retorno = retornoExpr1;
                     }else{
                        retornoExpr.setSubArvore(retornoMenos.getSubArvore() + retornoExpr.getSubArvore());
                        retorno = retornoExpr;
                     }
                     //cout << "primeira producao" << endl;
   						//retornoExpr.setSubArvore(retornoExpr1.getSubArvore() + retornoBinop.getSubArvore() + retornoExpr.getSubArvore());
   						//retorno = retornoExpr;
   					}else if(retornoUnop.getResultado() == 1 && retornoUnopExpr.getResultado() == 1 ){
   						//cout << "segunda producao" << endl;
   						if(RetornoUnopExpr1.getResultado() == 1){
                        RetornoUnopExpr1.setSubArvore(retornoUnop.getSubArvore() + retornoUnopExpr.getSubArvore() + RetornoUnopExpr1.getSubArvore() );
                        retorno = RetornoUnopExpr1;
                     }else{
                        retornoUnopExpr.setSubArvore(retornoUnop.getSubArvore() + retornoUnopExpr.getSubArvore());
                        retorno = retornoUnopExpr;
                     }   

                     //retornoMultExpr.setSubArvore(retornoExpr1.getSubArvore() + retornoMult.getSubArvore() + retornoMultExpr.getSubArvore());
   						//retorno = retornoMultExpr;
   					}else if(retornoPaAberto.getResultado() == 1 && retornoPaAbertoExpr.getResultado() == 1 && retornoPaFechado.getResultado() == 1 ){
   						//cout << "terceira producao" << endl;
                     if(retornoPaAbertoExprExpr1.getResultado() == 1){
                        retornoPaAbertoExprExpr1.setSubArvore(retornoPaAbertoExpr.getSubArvore() + retornoPaAbertoExprExpr1.getSubArvore() );
                        retorno = retornoPaAbertoExprExpr1;
                     }else{
                        retornoPaFechado.setSubArvore(retornoPaAbertoExpr.getSubArvore());
                        retorno = retornoPaFechado;
                     }   						

                     //retornoDivExpr.setSubArvore(retornoExpr1.getSubArvore() + retornoDiv.getSubArvore() + retornoDivExpr.getSubArvore());
   						//retorno = retornoDivExpr;
   					 //cout << "funcall" << retornoFuncall.getResultado() << endl;
                  }else if(retornoFuncall.getResultado() == 1 ){
   						//cout << "quarta producao" << endl;
   						//Retorno retornoFuncall,retornoFuncallExpr1;
                     if(retornoFuncallExpr1.getResultado() == 1){
                        retornoFuncallExpr1.setSubArvore(retornoFuncallExpr1.getSubArvore() + retornoFuncall.getSubArvore());
                        retorno = retornoFuncallExpr1;
                     }else{
                        retorno = retornoFuncall;
                     }
                     //retornoAndExpr.setSubArvore(retornoExpr1.getSubArvore() + retornoAnd.getSubArvore() + retornoAndExpr.getSubArvore());
   						//retorno = retornoAndExpr;
   					}else if(retornoDecimal.getResultado() == 1 ){
   						if(retornoDecimalExpr1.getResultado() == 1){
                        std::size_t found = retornoDecimalExpr1.getSubArvore().find("%");
                        if (found!=std::string::npos && false){
                           std::string teste = retornoDecimalExpr1.getSubArvore();
                           teste.replace(found,1,retornoDecimal.getSubArvore());
                           retornoDecimalExpr1.setSubArvore(teste);   
                        }else{
                           retornoDecimalExpr1.setSubArvore( retornoDecimalExpr1.getSubArvore()  + retornoDecimal.getSubArvore() );   
                        }
                        retorno = retornoDecimalExpr1;
                     }else{
                        retorno = retornoDecimal;
                     }
                     //cout << "quinta producao" << endl;
   						//retorno = retornoExpr1;
   					}else if(retornoId.getResultado() == 1 ){
                     if(retornoIdExpr1.getResultado() == 1){
                        retornoIdExpr1.setSubArvore(retornoId.getSubArvore() + retornoIdExpr1.getSubArvore());   
                        retorno = retornoIdExpr1;
                     }else{
                        retorno = retornoId;
                     }  
                     //cout << "sexta producao" << endl;
                     //retorno = retornoId;
                  }
   				}else if( no.getNome() == "unop" ){	
   					Retorno retornoNegacao;
   					retornoNegacao = proximaProducao(fabricaNo.solicitarNo("!"), no, indiceStream, indiceToken, escopos);	 
   					//cout << "retornoNegacao " << retornoNegacao.getResultado() << endl;
                  if(retornoNegacao.getResultado() == 1){
   						indiceStream = retornoNegacao.getUltimoIndice();
   						retorno = retornoNegacao;
   					}
   				}else if(no.getNome() == "assign"){
                  Retorno retornoId,retornoIgual,retornoExpr;
                  retornoId = proximaProducao(this->fabricaNo.solicitarNo("id"),no,indiceStream,indiceToken,escopos);   
                  if(retornoId.getResultado() == 1){
                     indiceStream = retornoId.getUltimoIndice();
                     retornoIgual = proximaProducao(this->fabricaNo.solicitarNo("="),no,indiceStream,indiceToken,escopos);
                     if(retornoIgual.getResultado() == 1){
                        indiceStream = retornoIgual.getUltimoIndice();
                        retornoExpr = proximaProducao(this->fabricaNo.solicitarNo("expr"),no,indiceStream,indiceToken,escopos);
                        if(retornoExpr.getResultado() == 1){
                           indiceStream = retornoExpr.getUltimoIndice();                        
                        }
                      }
                   }
                   if(retornoId.getResultado() == 1 && retornoIgual.getResultado() == 1 && retornoExpr.getResultado() == 1 ){
                     retornoExpr.setSubArvore("[assign "+ retornoId.getSubArvore() + iniciarArvore5() + "]");
                     subArvorePrefixa = "";
                     retorno = retornoExpr;
                   }   

               }else if( no.getNome() == "expr1" ){
                  Retorno retornoMult,retornoMultExpr,retornoMultExprExpr1;
                  Retorno retornoDiv,retornoDivExpr,retornoDivExprExpr1;
                  Retorno retornoAnd,retornoAndExpr,retornoAndExprExpr1;
                  Retorno retornoBinop,retornoBinopExpr,retornoBinopExprExpr1;
                  Retorno retornoVazioExpr1;

                  retornoMult = proximaProducao(fabricaNo.solicitarNo("*"), no, indiceStream, indiceToken, escopos); 
                  if(retornoMult.getResultado() == 1){
                     indiceStream = retornoMult.getUltimoIndice();
                     retornoMultExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos); 
                     if(retornoMultExpr.getResultado() == 1){
                        indiceStream = retornoMultExpr.getUltimoIndice();   
                        retornoMultExprExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos); 
                        if(retornoMultExprExpr1.getResultado() == 1 || retornoMultExprExpr1.getResultado() == 2 ){
                           if(retornoMultExprExpr1.getResultado() == 1){
                              indiceStream = retornoMultExprExpr1.getUltimoIndice();      
                           }   
                        }
                     }
                  }else{
                     retornoDiv = proximaProducao(fabricaNo.solicitarNo("/"), no, indiceStream, indiceToken, escopos); 
                     if(retornoDiv.getResultado() == 1){
                        indiceStream = retornoDiv.getUltimoIndice();
                        retornoDivExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);    
                        if(retornoDivExpr.getResultado() == 1){
                           indiceStream = retornoDivExpr.getUltimoIndice();
                           retornoDivExprExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                           if(retornoDivExprExpr1.getResultado() == 1){
                              indiceStream = retornoDivExprExpr1.getUltimoIndice();   
                           }
                        }
                     }else{
                        //retornoAnd,retornoAndExpr,retornoAndExprExpr1
                        retornoAnd = proximaProducao(fabricaNo.solicitarNo("&&"), no, indiceStream, indiceToken, escopos);
                        if(retornoAnd.getResultado() == 1){
                           indiceStream = retornoAnd.getUltimoIndice();   
                           retornoAndExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);
                           if(retornoAndExpr.getResultado() == 1){
                              indiceStream = retornoAndExpr.getUltimoIndice();      
                              retornoAndExprExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                              if(retornoAndExprExpr1.getResultado() == 1){
                                 indiceStream = retornoAndExprExpr1.getUltimoIndice();      
                              }
                           }
                        }else{
                           //retornoBinop,retornoBinopExpr,retornoBinopExprExpr1;
                           retornoBinop = proximaProducao(fabricaNo.solicitarNo("binop"), no, indiceStream, indiceToken, escopos);;
                           if(retornoBinop.getResultado() == 1){
                              indiceStream = retornoBinop.getUltimoIndice();   
                              retornoBinopExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos); ;
                              if(retornoBinopExpr.getResultado() == 1){
                                 indiceStream = retornoBinopExpr.getUltimoIndice();
                                 retornoBinopExprExpr1 = proximaProducao(fabricaNo.solicitarNo("expr1"), no, indiceStream, indiceToken, escopos);
                                 if(retornoBinopExprExpr1.getResultado() == 1){
                                    indiceStream = retornoBinopExprExpr1.getUltimoIndice();
                                 }
                              }
                           }else{
                              //retornoVazioExpr1.setResultado(2); 
                              
                           }
                        }
                     }   
                  }

                  //retornoMult,retornoMultExpr,retornoMultExprExpr1
                  if(retornoMult.getResultado() == 1 && retornoMultExpr.getResultado() == 1 ){
                     if(retornoMultExprExpr1.getResultado() == 1){
                        retornoMultExprExpr1.setSubArvore(retornoMult.getSubArvore() + retornoMultExpr.getSubArvore() + retornoMultExprExpr1.getSubArvore() );
                        retorno = retornoMultExprExpr1;
                     }else{
                        retornoMultExpr.setSubArvore( retornoMult.getSubArvore() + retornoMultExpr.getSubArvore());
                        retorno = retornoMultExpr;
                     }
                  }else if(retornoDiv.getResultado() == 1 && retornoDivExpr.getResultado() == 1){
                     //retornoDiv,retornoDivExpr,retornoDivExprExpr1;
                     if(retornoDivExprExpr1.getResultado() == 1){
                        retornoDivExprExpr1.setSubArvore(retornoDiv.getSubArvore() + retornoDivExpr.getSubArvore() + retornoDivExprExpr1.getSubArvore() );
                        retorno = retornoDivExprExpr1;
                     }else{
                        retornoDivExpr.setSubArvore( retornoDiv.getSubArvore() + retornoDivExpr.getSubArvore());
                        retorno = retornoDivExpr;
                     }
                     
                  }else if(retornoAnd.getResultado() == 1 && retornoAndExpr.getResultado() == 1){
                     
                     if(retornoAndExprExpr1.getResultado() == 1){
                        retornoAndExprExpr1.setSubArvore(retornoAnd.getSubArvore() + retornoAndExpr.getSubArvore() + retornoAndExprExpr1.getSubArvore());
                        retorno = retornoAndExprExpr1;
                     }else{
                       retornoAndExpr.setSubArvore(retornoAnd.getSubArvore() + retornoAndExpr.getSubArvore() ); 
                       retorno = retornoAndExpr; 
                     }

                  }else if(retornoBinop.getResultado() == 1 && retornoBinopExpr.getResultado() == 1){
                     
                     if(retornoBinopExprExpr1.getResultado() == 1){
                        //cout << "entrou aquiii  11111" << endl;
                        retornoBinopExprExpr1.setSubArvore( retornoBinop.getSubArvore() + retornoBinopExprExpr1.getSubArvore() + retornoBinopExpr.getSubArvore()  );
                        retorno = retornoBinopExprExpr1;
                     }else{
                        //cout << "entrou aquiii  222222" << endl;
                        retornoBinopExpr.setSubArvore( retornoBinop.getSubArvore() + retornoBinopExpr.getSubArvore()  );
                        retorno = retornoBinopExpr;
                     }                                     

                  }else{
                     retorno.setResultado(2); //= retornoVazioExpr1;
                  }

   				}else if( no.getNome() == "binop" ){
   					//cout << "leu binop" << endl;
   					Retorno retornoMais,retornoMenos,retornoMenorQ,retornoMenorIgualQ,retornoMaiorQ,retornoMaiorIgual,retornoIgualIgual,retornoDifIgual,retornoOu;
   					retornoMais = proximaProducao(fabricaNo.solicitarNo("+"), no, indiceStream, indiceToken, escopos);	
   					if(retornoMais.getResultado() == 1){
   						indiceStream = retornoMais.getUltimoIndice();
						//cout << "decimal = " << retorno.getSubArvore() << endl;
						//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
   					}else{
	   					//retornoAux = retorno;

	   					retornoMenos = proximaProducao(fabricaNo.solicitarNo("-"), no, indiceStream, indiceToken, escopos);	
	   					if(retornoMenos.getResultado() == 1){
	   						indiceStream = retornoMenos.getUltimoIndice();
							//cout << "decimal = " << retorno.getSubArvore() << endl;
							//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
	   					}else{
		   					//retornoAux = retorno;
		   					retornoMenorQ = proximaProducao(fabricaNo.solicitarNo("<"), no, indiceStream, indiceToken, escopos);	
		   					if(retornoMenorQ.getResultado() == 1){
		   						indiceStream = retornoMenorQ.getUltimoIndice();
								//cout << "decimal = " << retorno.getSubArvore() << endl;
								//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
		   					}else{		   					
			   					//retornoAux = retorno;
			   					retornoMenorIgualQ = proximaProducao(fabricaNo.solicitarNo("<="), no, indiceStream, indiceToken, escopos);	
			   					if(retornoMenorIgualQ.getResultado() == 1){
			   						indiceStream = retornoMenorIgualQ.getUltimoIndice();
									//cout << "decimal = " << retorno.getSubArvore() << endl;
									//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
			   					}else{			   					
				   					//retornoAux = retorno;
				   					retornoMaiorQ = proximaProducao(fabricaNo.solicitarNo(">"), no, indiceStream, indiceToken, escopos);	
				   					if(retornoMaiorQ.getResultado() == 1){
				   						indiceStream = retornoMaiorQ.getUltimoIndice();
										//cout << "decimal = " << retorno.getSubArvore() << endl;
										//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
				   					}else{				   					
				   						//retornoAux = retorno;
				   						retornoMaiorIgual = proximaProducao(fabricaNo.solicitarNo(">="), no, indiceStream, indiceToken, escopos);	
					   					if(retornoMaiorIgual.getResultado() == 1){
					   						indiceStream = retornoMaiorIgual.getUltimoIndice();
											//cout << "decimal = " << retorno.getSubArvore() << endl;
											//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
					   					}else{
					   						//retornoAux = retorno;
					   						retornoIgualIgual = proximaProducao(fabricaNo.solicitarNo("=="), no, indiceStream, indiceToken, escopos);	
						   					if(retornoIgualIgual.getResultado() == 1){
						   						indiceStream = retornoIgualIgual.getUltimoIndice();
												//cout << "decimal = " << retorno.getSubArvore() << endl;
												   retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
						   					}else{
						   						//retornoAux = retorno;
						   						retornoDifIgual = proximaProducao(fabricaNo.solicitarNo("!="), no, indiceStream, indiceToken, escopos);	
							   					if(retornoDifIgual.getResultado() == 1){
							   						indiceStream = retornoDifIgual.getUltimoIndice();
													//cout << "decimal = " << retorno.getSubArvore() << endl;
													//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
							   					}else{
							   						//retornoAux = retorno;
							   						retornoOu = proximaProducao(fabricaNo.solicitarNo("||"), no, indiceStream, indiceToken, escopos);	
								   					if(retornoOu.getResultado() == 1){
								   						indiceStream = retornoOu.getUltimoIndice();
														//cout << "decimal = " << retorno.getSubArvore() << endl;
														//retorno.setSubArvore( subArvore +retorno.getSubArvore() );	
								   					}else{
								   						
								   					}

							   					}

						   					}


					   					}

				   					}
			   					}	

		   					}	
	   					}		
   					}   					
   					if(retornoMais.getResultado() == 1){
   						//retornoMais.setSubArvore("+");
   						//cout << "subArvore aqui " << retornoMais.getSubArvore() << endl;
                     retorno = retornoMais;
   					}else if(retornoMenos.getResultado() == 1){
   						//retornoMenos.setSubArvore("-");
                     retorno = retornoMenos;
   					}else if(retornoMenorQ.getResultado() == 1){
   						//retornoMenorQ.setSubArvore("<");
                     retorno = retornoMenorQ;
   					}else if(retornoMenorIgualQ.getResultado() == 1){
   						//retornoMenorIgualQ.setSubArvore("<=");
                     retorno = retornoMenorIgualQ;
   					}else if(retornoMaiorQ.getResultado() == 1){
   						//retornoMaiorQ.setSubArvore(">");
                     retorno = retornoMaiorQ;
   					}else if(retornoMaiorIgual.getResultado() == 1){
                     //retornoMaiorIgual.setSubArvore(">=");
                     retorno = retornoMaiorIgual;
                  }else if(retornoIgualIgual.getResultado() == 1){
   						//retornoIgualIgual.setSubArvore("==");
                     retorno = retornoIgualIgual;
   					}else if(retornoDifIgual.getResultado() == 1){
   						//retornoDifIgual.setSubArvore("!=");
                     retorno = retornoDifIgual;
   					}else if(retornoOu.getResultado() == 1){
   						//retornoOu.setSubArvore("||");
                     retorno = retornoOu;
   					}	
   				
   				}else if( no.getNome() == "decfunc" ){
   					Retorno retornoDef,retornoType,retornoId,retornoPaberto,retornoParamList,retornoPaFechado,retornoBlock, retornoPaFechadoS,retornoBlockS;
   					retornoDef = proximaProducao(fabricaNo.solicitarNo("def"), no, indiceStream, indiceToken, escopos);	
   					if(retornoDef.getResultado() == 1){
   						indiceStream = retornoDef.getUltimoIndice();
   						retornoType = proximaProducao(fabricaNo.solicitarNo("type"), no, indiceStream, indiceToken, escopos);	
   						if(retornoType.getResultado() == 1){
   							indiceStream = retornoType.getUltimoIndice();
   							retornoId = proximaProducao(fabricaNo.solicitarNo("id"), no, indiceStream, indiceToken, escopos);	
   							if(retornoId.getResultado() == 1){
   								indiceStream = retornoId.getUltimoIndice();
   								retornoPaberto = proximaProducao(fabricaNo.solicitarNo("("), no, indiceStream, indiceToken, escopos);
   								
   								if(retornoPaberto.getResultado() == 1){
   									indiceStream = retornoPaberto.getUltimoIndice();
   									retornoParamList = proximaProducao(fabricaNo.solicitarNo("paramList"), no, indiceStream, indiceToken, escopos);
   									
   									if(retornoParamList.getResultado() == 1 || retornoParamList.getResultado() == 2 ){
   										if(retornoParamList.getResultado() == 1){
                                    indiceStream = retornoParamList.getUltimoIndice();
   										}
                                 retornoPaFechado = proximaProducao(fabricaNo.solicitarNo(")"), no, indiceStream, indiceToken, escopos);
   										if(retornoPaFechado.getResultado() == 1){
   											indiceStream = retornoPaFechado.getUltimoIndice();
   											retornoBlock = proximaProducao(fabricaNo.solicitarNo("block"), no, indiceStream, indiceToken, escopos);
   											if(retornoBlock.getResultado() == 1){
   												indiceStream = retornoBlock.getUltimoIndice();
   											}
   										}else{
                                    retornoBlock.setResultado(0);
                                 }
   									}
   								}
   							}
   						}
   					}
   				   

                  //Retorno retornoDef,retornoType,retornoId,retornoPaberto,retornoParamList,retornoPaFechado,retornoBlock, retornoPaFechadoS,retornoBlockS;  
                  //cout << "inicio block" << endl;
                  //cout << "retornoBlock.getResultado() " <<retornoBlock.getResultado() << endl;
                  if(retornoDef.getResultado() == 1 && retornoType.getResultado() == 1 && retornoId.getResultado() == 1 && retornoPaberto.getResultado() == 1 && (retornoParamList.getResultado() == 1 || retornoParamList.getResultado() == 2 ) && retornoPaFechado.getResultado() == 1 && retornoBlock.getResultado() == 1 ){
                     //cout << "inscricao block" << endl;
                     retornoBlock.setSubArvore(retornoId.getSubArvore() + retornoParamList.getSubArvore() + "[block "+ retornoBlock.getSubArvore() + "]" );
                     retorno = retornoBlock;
                  }

               }else if(no.getNome() == "paramList"){
   					Retorno retornoType, retornoId, retornoVirgula,retornoVirgulaType,retornoVirgulaTypeId;
   					std::vector<Retorno> retornosVirgula,retornosVirgulaType,retornosVirgulaTypeId;
   					bool bVirgula =  true, bVirgulaType = true, bVirgulaTypeId = true;

   					retornoType = proximaProducao(fabricaNo.solicitarNo("type"), no, indiceStream, indiceToken, escopos);
   					if(retornoType.getResultado() == 1){
   						indiceStream = retornoType.getUltimoIndice();
   						retornoId = proximaProducao(fabricaNo.solicitarNo("id"), no, indiceStream, indiceToken, escopos);
   						if(retornoId.getResultado() == 1){   							   						
   							indiceStream = retornoId.getUltimoIndice();
   							while(bVirgula == true && bVirgulaType == true && bVirgulaTypeId == true){
   								retornoVirgula = proximaProducao(fabricaNo.solicitarNo(","), no, indiceStream, indiceToken, escopos);
   								if(retornoVirgula.getResultado() == 1){
   									indiceStream = retornoVirgula.getUltimoIndice();
   									retornosVirgula.push_back(retornoVirgula);
   									retornoVirgulaType = proximaProducao(fabricaNo.solicitarNo("type"), no, indiceStream, indiceToken, escopos);	
   									if(retornoVirgulaType.getResultado() == 1){
   										indiceStream = retornoVirgulaType.getUltimoIndice();
   										retornosVirgulaType.push_back(retornoVirgulaType);
   										retornoVirgulaTypeId = proximaProducao(fabricaNo.solicitarNo("id"), no, indiceStream, indiceToken, escopos);
   										if(retornoVirgulaTypeId.getResultado() == 1){
   											indiceStream = retornoVirgulaTypeId.getUltimoIndice();
   											retornosVirgulaTypeId.push_back(retornoVirgulaTypeId);
   										}else{
   											bVirgulaTypeId = false;
   										}
   									}else{
   										bVirgulaType = false;
   									}
   								}else{
   									bVirgula = false;
   								}
   							

   							}
                        int size = 0;
      						if(retornosVirgula.size() == retornosVirgulaType.size() && retornosVirgulaType.size() == retornosVirgulaTypeId.size() && retornosVirgulaTypeId.size() != 0 ){  
          						std::string subArvoreVirgulaTypeId = "";
          						for (int i = 0; i < retornosVirgulaTypeId.size(); ++i)
          						{
          							subArvoreVirgulaTypeId = subArvoreVirgulaTypeId +" "+ retornosVirgulaTypeId.at(i).getSubArvore();    									
          						}
          						retornoVirgulaTypeId.setSubArvore(subArvoreVirgulaTypeId);
          					   size = retornosVirgula.size();   
                        }else{
                           retornoVirgulaTypeId.setResultado(0);
                        }      			
   						}
   				  }
                  //cout << "paramList aquiiii" << "retornoType.getResultado()= " << retornoType.getResultado() << " retornosVirgulaTypeId.size() " << retornosVirgulaTypeId.size() << endl;
                  if(retornoType.getResultado() == 0 && retornosVirgula.size() == retornosVirgulaType.size() && retornosVirgulaType.size() == retornosVirgulaTypeId.size() ){
                     //cout <<"saiu 1" << endl; 
                     retornoId.setResultado(2);
                     retornoId.setSubArvore("[paramlist "+ retornoId.getSubArvore()+"]");
                     retorno = retornoId; 
                  }else if(retornoType.getResultado() == 1 && retornoId.getResultado() == 1 && retornosVirgula.size() == retornosVirgulaType.size() && retornosVirgulaType.size() == retornosVirgulaTypeId.size() && retornoVirgulaTypeId.getResultado() == 0){
                     retornoId.setSubArvore("[paramlist "+ retornoId.getSubArvore()+"]");
                     retorno = retornoId;                     
                     //cout <<"saiu 2" << endl;
                  }else if(retornoType.getResultado() == 1 && retornoId.getResultado() == 1 &&  retornosVirgula.size() == retornosVirgulaType.size() && retornosVirgulaType.size() == retornosVirgulaTypeId.size() && retornoVirgulaTypeId.getResultado() == 1){
                     //cout <<"saiu 3" << endl;
                     retornoVirgulaTypeId.setSubArvore("[paramlist "+ retornoId.getSubArvore() + retornoVirgulaTypeId.getSubArvore()+ "]");
                     retorno = retornoVirgulaTypeId;
                  } 


   				}else if(no.getNome() == "block"){
                  bool bDecvar = true,bStmt = true;                  
                  std::vector<Retorno> retornosDecVar;
                  std::vector<Retorno> retornosStmt;
                  Retorno retornoDecVar,retornoStmt,retornoChaveAberta,retornoChaveFechada;   				  

                  retornoChaveAberta = proximaProducao(this->fabricaNo.solicitarNo("{"),no,indiceStream,indiceToken,escopos);
                  if(retornoChaveAberta.getResultado() == 1){
                     indiceStream = retornoChaveAberta.getUltimoIndice() ;

                     while( (indiceStream < this->streamTokens.size() ) && ( bDecvar == true)  ){
                        retornoDecVar = proximaProducao(this->fabricaNo.solicitarNo("decvar"),no,indiceStream,indiceToken,escopos);                                             
                        if(retornoDecVar.getResultado() == 1){
                           bDecvar = true;
                           indiceStream = retornoDecVar.getUltimoIndice() ;
                                                   
                           retornoDecVar.setSubArvore("[decvar "+ retornoDecVar.getSubArvore()+"]");
                           retornosDecVar.push_back(retornoDecVar);
                        }else{
                           bDecvar = false;
                        }
                     }
                  
                     while( (indiceStream < this->streamTokens.size() ) && ( bStmt == true)   ){                                          
                        retornoStmt = proximaProducao(this->fabricaNo.solicitarNo("stmt"),no,indiceStream,indiceToken,escopos);
                        if( retornoStmt.getResultado() == 1 ){
                           bStmt = true;
                           indiceStream = retornoStmt.getUltimoIndice() ;
                           //retornoStmt.setSubArvore( retornoStmt.getSubArvore());
                           retornosStmt.push_back(retornoStmt);
                        }else{
                           bStmt = false;                      
                           //cout << "saida falsa stmt indice = " << indiceStream << " this->streamTokens.at(indiceStream).getTipoToken() " << this->streamTokens.at(indiceStream).getLexema() << endl; 
                        }

                     }

                  }
                     
                  retornoChaveFechada = proximaProducao(this->fabricaNo.solicitarNo("}"),no,indiceStream,indiceToken,escopos);
                  if(retornoChaveFechada.getResultado() == 1){
                     indiceStream = retornoChaveFechada.getUltimoIndice() ;
                  }
                              
                  std::string subArvoreStmt = "";  
                  for (int i = 0; i < retornosStmt.size(); ++i){
                     subArvoreStmt = subArvoreStmt  + retornosStmt.at(i).getSubArvore();                   
                     retorno = retornosStmt.at(i);
                  }

                  std::string subArvoreDecVar = "";   
                  for (int i = 0; i < retornosDecVar.size(); ++i){
                     subArvoreDecVar = subArvoreDecVar + retornosDecVar.at(i).getSubArvore();                   
                     retorno = retornosDecVar.at(i);
                  }
                  
                  retorno.setSubArvore( subArvoreDecVar + subArvoreStmt );
                     
                  //cout << "aquiiiiiii retornoChaveFechada.getResultado() " << retornoChaveFechada.getResultado() << "retorno.getResultado() " <<retorno.getResultado() << endl;
                  if(retornoChaveAberta.getResultado() == 1 && retornoChaveFechada.getResultado() == 1){
                     retornoChaveFechada.setSubArvore(retorno.getSubArvore());   
                     retorno = retornoChaveFechada;
                  }else{
                     retorno.setSubArvore("");   
                     retorno.setResultado(0);   
                  }
                  

   				}else if(no.getNome() == "stmt"){
                  //cout << "stmt2" << endl;
                  Retorno retornoAssign,retornoIgual,retornoExpr,retornoPontoVirgula,retornoFuncall,retornoFuncallPontoVirgula,retornoIf,retornoIfPaberto,retornoIfExpr,retornoIfPaFechado,retornoIfBlock,retornoIfElse,retornoIfElseBlock,retornoWhile,retornoPabertoWhile,retornoWhileExpr,retornoPaFechadoWhile,retornoWhileBlock,retornoReturn,retornoReturnExpr,retornoReturnPontoVirgula,retornoBreak,retornoBreakPontoVirgula,retornoContinue,retornoContinuePontoVirgula;                  
                  retornoFuncall = proximaProducao(this->fabricaNo.solicitarNo("funcall"),no,indiceStream,indiceToken,escopos);
                  if(retornoFuncall.getResultado() == 1){
                     indiceStream = retornoFuncall.getUltimoIndice();   
                     retornoFuncallPontoVirgula = proximaProducao(this->fabricaNo.solicitarNo(";"),no,indiceStream,indiceToken,escopos);
                     if(retornoFuncallPontoVirgula.getResultado() == 1){
                        indiceStream = retornoFuncallPontoVirgula.getUltimoIndice();   
                     }

                  }else{
                     //ajustar aqui
                     //retornoId = proximaProducao(this->fabricaNo.solicitarNo("id"),no,indiceStream,indiceToken,escopos);   
                     retornoAssign = proximaProducao(this->fabricaNo.solicitarNo("assign"),no,indiceStream,indiceToken,escopos);    
                     if(retornoAssign.getResultado() == 1){
                        indiceStream = retornoAssign.getUltimoIndice();                        
                        retornoPontoVirgula = proximaProducao(this->fabricaNo.solicitarNo(";"),no,indiceStream,indiceToken,escopos); 
                        if(retornoPontoVirgula.getResultado() == 1){
                           indiceStream = retornoPontoVirgula.getUltimoIndice();   
                        }
                                                   
                     }else{
                        //retornoIf,retornoIfPaberto,retornoIfPaFechado,retornoIfBlock,retornoIfElse,retornoIfElseBlock
                        retornoIf = proximaProducao(this->fabricaNo.solicitarNo("if"),no,indiceStream,indiceToken,escopos);
                        if(retornoIf.getResultado() == 1){
                           indiceStream = retornoIf.getUltimoIndice();   
                           retornoIfPaberto = proximaProducao(this->fabricaNo.solicitarNo("("),no,indiceStream,indiceToken,escopos);
                           if(retornoIfPaberto.getResultado() == 1){
                              indiceStream = retornoIfPaberto.getUltimoIndice();   
                              retornoIfExpr = proximaProducao(this->fabricaNo.solicitarNo("expr"),no,indiceStream,indiceToken,escopos);
                              if(retornoIfExpr.getResultado() == 1){
                                 indiceStream = retornoIfExpr.getUltimoIndice();      
                                 retornoIfPaFechado = proximaProducao(this->fabricaNo.solicitarNo(")"),no,indiceStream,indiceToken,escopos);
                                 if(retornoIfPaFechado.getResultado() == 1){
                                    indiceStream = retornoIfPaFechado.getUltimoIndice();      
                                    retornoIfBlock = proximaProducao(this->fabricaNo.solicitarNo("block"),no,indiceStream,indiceToken,escopos);
                                    if(retornoIfBlock.getResultado() == 1){
                                       indiceStream = retornoIfBlock.getUltimoIndice();
                                    }
                                    retornoIfElse = proximaProducao(this->fabricaNo.solicitarNo("else"),no,indiceStream,indiceToken,escopos);
                                    if(retornoIfElse.getResultado() == 1){
                                       indiceStream = retornoIfElse.getUltimoIndice();   
                                       retornoIfElseBlock = proximaProducao(this->fabricaNo.solicitarNo("block"),no,indiceStream,indiceToken,escopos);
                                       if(retornoIfElseBlock.getResultado() == 1){
                                          indiceStream = retornoIfElseBlock.getUltimoIndice();   
                                       }
                                    }
                                 }
                              }
                           }

                        }else{
                           //retornoWhile,retornoPabertoWhile,retornoWhileExpr,retornoPaFechadoWhile,retornoWhileBlock
                           retornoWhile = proximaProducao(this->fabricaNo.solicitarNo("while"),no,indiceStream,indiceToken,escopos);
                           if(retornoWhile.getResultado() == 1){
                              indiceStream = retornoWhile.getUltimoIndice();
                              retornoPabertoWhile = proximaProducao(this->fabricaNo.solicitarNo("("),no,indiceStream,indiceToken,escopos);
                              if(retornoPabertoWhile.getResultado() == 1){
                                 indiceStream = retornoPabertoWhile.getUltimoIndice();
                                 retornoWhileExpr = proximaProducao(this->fabricaNo.solicitarNo("expr"),no,indiceStream,indiceToken,escopos);
                                 if(retornoWhileExpr.getResultado() == 1){
                                    indiceStream = retornoWhileExpr.getUltimoIndice();
                                    retornoPaFechadoWhile = proximaProducao(this->fabricaNo.solicitarNo(")"),no,indiceStream,indiceToken,escopos); 
                                    if(retornoPaFechadoWhile.getResultado() == 1){
                                       indiceStream = retornoPaFechadoWhile.getUltimoIndice();
                                       retornoWhileBlock = proximaProducao(this->fabricaNo.solicitarNo("block"),no,indiceStream,indiceToken,escopos); 
                                       if(retornoWhileBlock.getResultado() == 1){
                                          indiceStream = retornoWhileBlock.getUltimoIndice();
                                       }
                                    }
                                 }
                              }   
                           }else{
                              //retornoReturn,retornoReturnExpr,retornoReturnPontoVirgula
                              retornoReturn = proximaProducao(this->fabricaNo.solicitarNo("return"),no,indiceStream,indiceToken,escopos); 
                              if(retornoReturn.getResultado() == 1){
                                 indiceStream = retornoReturn.getUltimoIndice();
                                 retornoReturnExpr = proximaProducao(this->fabricaNo.solicitarNo("expr"),no,indiceStream,indiceToken,escopos);
                                 if(retornoReturnExpr.getResultado() == 1){
                                    indiceStream = retornoReturnExpr.getUltimoIndice();
                                    retornoReturnPontoVirgula = proximaProducao(this->fabricaNo.solicitarNo(";"),no,indiceStream,indiceToken,escopos);   
                                    if(retornoReturnPontoVirgula.getResultado() == 1){
                                       indiceStream = retornoReturnPontoVirgula.getUltimoIndice();      
                                    }
                                 }else{
                                    retornoReturnPontoVirgula = proximaProducao(this->fabricaNo.solicitarNo(";"),no,indiceStream,indiceToken,escopos);   
                                    if(retornoReturnPontoVirgula.getResultado() == 1){
                                       indiceStream = retornoReturnPontoVirgula.getUltimoIndice();      
                                    }
                                 }
                              }else{
                                 //,retornoBreakPontoVirgula
                                 retornoBreak = proximaProducao(this->fabricaNo.solicitarNo("break"),no,indiceStream,indiceToken,escopos);
                                 if(retornoBreak.getResultado() == 1){
                                    indiceStream = retornoBreak.getUltimoIndice();
                                    retornoBreakPontoVirgula = proximaProducao(this->fabricaNo.solicitarNo(";"),no,indiceStream,indiceToken,escopos);
                                    if(retornoBreakPontoVirgula.getResultado() == 1){
                                       indiceStream = retornoBreakPontoVirgula.getUltimoIndice();
                                    }
                                 }else{
                                    //retornoContinue,retornoContinuePontoVirgula
                                    retornoContinue = proximaProducao(this->fabricaNo.solicitarNo("continue"),no,indiceStream,indiceToken,escopos);
                                    if(retornoContinue.getResultado() == 1){
                                       indiceStream = retornoContinue.getUltimoIndice();
                                       retornoContinuePontoVirgula = proximaProducao(this->fabricaNo.solicitarNo(";"),no,indiceStream,indiceToken,escopos);
                                       if(retornoContinuePontoVirgula.getResultado() == 1){
                                          indiceStream = retornoContinuePontoVirgula.getUltimoIndice();
                                       }
                                    }
                                 }

                              }

                           }
                        }

                     }

                  } 
                  //cout << "stmt" << endl;
                  //cout << "hora da vez retornoIfPaberto.getResultado() " << retornoIfPaberto.getResultado() << endl; //&& retornoIfPaberto.getResultado() == 1 && retornoIfExpr.getResultado() == 1 && retornoIfPaFechado.getResultado() == 1 && retornoIfBlock.getResultado() == 1 && retornoIfElse.getResultado() == retornoIfElseBlock.getResultado()"
                  //cout << "retornoIfExpr.getResultado()= " << retornoIfExpr.getResultado() << " retornoIfPaFechado.getResultado() " << retornoIfPaFechado.getResultado() << "retornoIfBlock.getResultado() " << retornoIfBlock.getResultado() << endl;
                  //cout << "retornoIfElse.getResultado() == retornoIfElseBlock.getResultado() = " << retornoIfElse.getResultado() << " " <<retornoIfElseBlock.getResultado() << endl;
                  if(retornoAssign.getResultado() == 1 && retornoPontoVirgula.getResultado() == 1){
                     retorno = retornoPontoVirgula;
                     retorno.setSubArvore( retornoAssign.getSubArvore() + "]" );
                  }else if(retornoFuncall.getResultado() == 1 && retornoFuncallPontoVirgula.getResultado() == 1){
                     retorno = retornoFuncallPontoVirgula;
                     retorno.setSubArvore( "[funcall "+retornoFuncall.getSubArvore() + retornoFuncallPontoVirgula.getSubArvore() + "]" );
                  }else if(retornoIf.getResultado() == 1 && retornoIfPaberto.getResultado() == 1 && retornoIfExpr.getResultado() == 1 && retornoIfPaFechado.getResultado() == 1 && retornoIfBlock.getResultado() == 1 && retornoIfElse.getResultado() == retornoIfElseBlock.getResultado() ){

                     if(retornoIfElseBlock.getResultado() == 1){
                        retorno = retornoIfElseBlock;
                        retorno.setSubArvore("[if "+retornoIf.getSubArvore() + retornoIfExpr.getSubArvore() + retornoIfBlock.getSubArvore() + "[else "+retornoIfElse.getSubArvore() + retornoIfElseBlock.getSubArvore() +"] ]");
                     }else{
                        retorno = retornoIfBlock;
                        retorno.setSubArvore("[if "+retornoIf.getSubArvore() + retornoIfExpr.getSubArvore() + retornoIfBlock.getSubArvore() +"]");
                     }

                  }else if(retornoWhile.getResultado() == 1 && retornoPabertoWhile.getResultado() == 1 && retornoWhileExpr.getResultado() == 1 && retornoPaFechadoWhile.getResultado() == 1 && retornoWhileBlock.getResultado() == 1 ){
                     retorno = retornoWhileBlock;
                     retorno.setSubArvore( "[while "+retornoWhile.getSubArvore() + " " + retornoWhileExpr.getSubArvore() +" "+ retornoWhileBlock.getSubArvore() +"]" );
                  }else if(retornoReturn.getResultado() == 1 && retornoReturnPontoVirgula.getResultado() == 1){ 
                     retorno = retornoReturnPontoVirgula;
                     if(retornoReturnExpr.getResultado() == 1){
                        retorno.setSubArvore( "[return "+ retornoReturn.getSubArvore() + retornoReturnExpr.getSubArvore() + retornoReturnPontoVirgula.getSubArvore() +"]" );
                     }else{
                        retorno.setSubArvore( "[return "+ retornoReturn.getSubArvore() + retornoReturnPontoVirgula.getSubArvore() +"]" );
                     }
                  }else if(retornoBreak.getResultado() == 1 && retornoBreakPontoVirgula.getResultado() == 1){
                     retorno = retornoBreakPontoVirgula;
                     retorno.setSubArvore( "[break" + retornoBreak.getSubArvore() + retornoBreakPontoVirgula.getSubArvore() + "]" );
                  }else if(retornoContinue.getResultado() == 1 && retornoContinuePontoVirgula.getResultado() == 1){
                     retorno = retornoContinuePontoVirgula;
                     retorno.setSubArvore( "[continue" + retornoContinue.getSubArvore() + retornoContinuePontoVirgula.getSubArvore() + "]" );
                  }

               }else if(no.getNome() == "funcall"){
                  //cout <<"estou em funcall" << endl;
                  Retorno retornoId,retornoPabertoFuncall,retornoArglist,retornoPaFechadoFuncall;
                  retornoId = proximaProducao(this->fabricaNo.solicitarNo("id"),no,indiceStream,indiceToken,escopos);
                  //cout << "retorno id: " << retornoId.getResultado() << endl;
                  if(retornoId.getResultado() == 1){
                     indiceStream = retornoId.getUltimoIndice();
                     retornoPabertoFuncall = proximaProducao(this->fabricaNo.solicitarNo("("),no,indiceStream,indiceToken,escopos);
                     if(retornoPabertoFuncall.getResultado() == 1){
                        indiceStream = retornoPabertoFuncall.getUltimoIndice();
                        retornoArglist = proximaProducao(this->fabricaNo.solicitarNo("argList"),no,indiceStream,indiceToken,escopos);
                        cout <<"retorno arglist: " << retornoArglist.getResultado() << endl;
                        if(retornoArglist.getResultado() == 1 || retornoArglist.getResultado() == 2 ){
                           if(retornoArglist.getResultado() == 1){
                              indiceStream = retornoArglist.getUltimoIndice();
                           }
                           
                           retornoPaFechadoFuncall = proximaProducao(this->fabricaNo.solicitarNo(")"),no,indiceStream,indiceToken,escopos);                          
                           cout << "retornoPaFechadoFuncall.getResultado():" << retornoPaFechadoFuncall.getResultado() << endl;
                           if(retornoPaFechadoFuncall.getResultado() == 1){
                              indiceStream = retornoPaFechadoFuncall.getUltimoIndice();
                           } 
                        }else{
                           retornoPaFechadoFuncall = proximaProducao(this->fabricaNo.solicitarNo(")"),no,indiceStream,indiceToken,escopos);                          
                           if(retornoPaFechadoFuncall.getResultado() == 1){
                              indiceStream = retornoPaFechadoFuncall.getUltimoIndice();
                           } 
                        }
                     }
                  }
                  
                  //retornoId,retornoPabertoFuncall,retornoArglist,retornoPaFechadoFuncall;
                  if(retornoId.getResultado() == 1 && retornoPabertoFuncall.getResultado() == 1 && retornoPaFechadoFuncall.getResultado() == 1 ){
                     retorno = retornoPaFechadoFuncall;
                     //cout << "retorno do funcall " << endl;
                     if(retornoArglist.getResultado() == 1){
                        retorno.setSubArvore(retornoId.getSubArvore() + "[arglist "+retornoArglist.getSubArvore() + "]" ); 
                     }else{
                        retorno.setSubArvore(retornoId.getSubArvore() + "[arglist ]" );    
                     }
                  }


               }else if( no.getNome() == "argList"){
                  //cout << "estou em argList" << endl;
                  Retorno retornoExpr, retornoExprVirgula, retornoExprVirgulaExpr;
                  std::vector<Retorno> retornosExprVirgula,retornosExprVirgulaExpr;
                  bool bExprVirgula =  true, bExprVirgulaExpr = true;
                  retornoExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);
                  cout << "retornoExpr.getResultado() :" << retornoExpr.getResultado() << endl;
                  if(retornoExpr.getResultado() == 1){
                     indiceStream = retornoExpr.getUltimoIndice();                     
                     while(bExprVirgula == true && bExprVirgulaExpr == true){
                        retornoExprVirgula = proximaProducao(fabricaNo.solicitarNo(","), no, indiceStream, indiceToken, escopos);
                        if(retornoExprVirgula.getResultado() == 1){
                           indiceStream = retornoExprVirgula.getUltimoIndice();
                           retornosExprVirgula.push_back(retornoExprVirgula);
                           retornoExprVirgulaExpr = proximaProducao(fabricaNo.solicitarNo("expr"), no, indiceStream, indiceToken, escopos);   
                           if(retornoExprVirgulaExpr.getResultado() == 1){
                              indiceStream = retornoExprVirgulaExpr.getUltimoIndice();
                              retornosExprVirgulaExpr.push_back(retornoExprVirgulaExpr);                              
                           }else{
                              bExprVirgulaExpr = false;
                           }
                        }else{
                           bExprVirgula = false;
                        }                        
                     }   
                  }else{
                     retornoExpr.setResultado(2);
                  }

                  std::string subArvoreExprVirgulaAndExprVirgulaExpr = "";
                  if(retornosExprVirgula.size() == retornosExprVirgulaExpr.size() && retornosExprVirgulaExpr.size() != 0 ){
                     subArvoreExprVirgulaAndExprVirgulaExpr = retornoExpr.getSubArvore();
                     for (int i = 0; i < retornosExprVirgula.size(); ++i)
                     {
                        subArvoreExprVirgulaAndExprVirgulaExpr = subArvoreExprVirgulaAndExprVirgulaExpr + retornosExprVirgulaExpr.at(i).getSubArvore() + retornosExprVirgula.at(i).getSubArvore(); 
                        //subArvoreExprVirgulaAndExprVirgulaExpr = 
                     }
                     
                     retorno = retornoExprVirgulaExpr;
                     retorno.setSubArvore(subArvoreExprVirgulaAndExprVirgulaExpr);
                  }else{
                     retorno = retornoExpr;
                  }

                                 
               }
  				
   				return retorno;
   			}else if (indiceStream < this->streamTokens.size() ) {
   				//cout << "indice de teste = " << indiceStream << endl; 
               //cout << "at" << this->streamTokens.at(indiceStream).getTipoToken() << endl;
   				//cout << "testar os terminais" <<endl;
   				if( noPai.getNome() == "type" ){
   					//cout << " noPai.getProducao(0) " << noPai.getProducao(0) << " traduzirTokenStream(this->streamTokens.at(indiceStream)) " <<  traduzirTokenStream(this->streamTokens.at(indiceStream)) << endl;
   					//cout << "indiceStream" << indiceStream << endl;
   					if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "int" &&  no.getNome() == "int"){
   						//cout << "achei um int" <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
   						return retornoF;
   					}else if( this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "void" &&  no.getNome() == "void"){
   						//cout << "achei um void" <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
   						return retornoF;
   					}	
   					//cout << "ocorre erro3?" <<endl;
   				}else if( noPai.getNome() == "decvar" ){
   					//cout << "ALAAAAAA " << " this->streamTokens.size() " << this->streamTokens.size() << " indiceStream " << indiceStream << endl;
   					//cout << "this->streamTokens.at(indiceStream).getTipoToken() " << this->streamTokens.at(indiceStream).getTipoToken() << endl;
   					if( this->streamTokens.size() > indiceStream +1 && this->streamTokens.at(indiceStream).getTipoToken() == "ID" && this->streamTokens.at(indiceStream+1).getLexema() != "(" && no.getNome() == "id" ){   						
   						//cout << "achei um key id" <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"["+this->streamTokens.at(indiceStream).getLexema()+"]",indiceToken);
   						return retornoF;
   					}else if( this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == ";" &&  no.getNome() == ";"){
   						//cout << "achei um key ;" <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
   						return retornoF;
   					}else if( this->streamTokens.at(indiceStream).getTipoToken() == "ID" &&  no.getNome() == "id"){
   						//cout << "achei um ID sem ; " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"["+this->streamTokens.at(indiceStream).getLexema()+"]",indiceToken);
   						return retornoF;
   					}else if( this->streamTokens.at(indiceStream).getLexema() == "=" &&  no.getNome() == "="){
   						//cout << "achei um = sem ; " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
   						return retornoF;
   					}
   					
   				}else if( noPai.getNome() == "expr1" ){   					
   					if( this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "*" &&  no.getNome() == "*"){
   						//this->pilhaOperacoes.push(this->streamTokens.at(indiceStream).getLexema());
                     //avaliarPilha(this->streamTokens.at(indiceStream).getLexema());
                     inserirArvoreExpr(this->streamTokens.at(indiceStream).getLexema(),"sinal");
                     //cout << "achei um * " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,this->streamTokens.at(indiceStream).getLexema(),indiceToken);
   						return retornoF;
   					}else if( this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "/" &&  no.getNome() == "/"){
   						//cout << "achei um / " <<endl;
   						inserirArvoreExpr(this->streamTokens.at(indiceStream).getLexema(),"sinal");
                     Retorno retornoF("0",1,"escopo",++indiceStream,this->streamTokens.at(indiceStream).getLexema(),indiceToken);
   						return retornoF;
   					}else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "&&" && no.getNome() == "&&"){
   						//cout << "achei um && " <<endl;
   						inserirArvoreExpr(this->streamTokens.at(indiceStream).getLexema(),"sinal");
                     Retorno retornoF("0",1,"escopo",++indiceStream,this->streamTokens.at(indiceStream).getLexema(),indiceToken);
   						return retornoF;
   					}else{
                     Retorno retornoF("0",2,"escopo",indiceStream,"",indiceToken);   
                  }
   				}else if(noPai.getNome() == "unop" ){
   					if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "!" &&  no.getNome() == "!"){
   						//cout << "achei um ! " <<endl;
   						inserirArvoreExpr(this->streamTokens.at(indiceStream).getLexema(),"sinal");
                     Retorno retornoF("0",1,"escopo",++indiceStream,"!",indiceToken);
   						return retornoF;
   					}
   				}else if(noPai.getNome() == "binop"){
   					if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && (this->streamTokens.at(indiceStream).getLexema() == "+" || this->streamTokens.at(indiceStream).getLexema() == "-" || this->streamTokens.at(indiceStream).getLexema() == "<" || this->streamTokens.at(indiceStream).getLexema() == "<=" || this->streamTokens.at(indiceStream).getLexema() == ">" || this->streamTokens.at(indiceStream).getLexema() == ">=" || this->streamTokens.at(indiceStream).getLexema() == "==" || this->streamTokens.at(indiceStream).getLexema() == "!=" || this->streamTokens.at(indiceStream).getLexema() == "||" ) ){
   						inserirArvoreExpr(this->streamTokens.at(indiceStream).getLexema(),"sinal");
                     //avaliarPilha(this->streamTokens.at(indiceStream).getLexema());
                     //this->pilhaOperacoes.push(this->streamTokens.at(indiceStream).getLexema());
                     //cout << "achei um SYMMMMM " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,this->streamTokens.at(indiceStream).getLexema(),indiceToken);
   						return retornoF;
   					}	
   				}else if(noPai.getNome() == "decfunc"){
   					if( this->streamTokens.at(indiceStream).getTipoToken() == "ID" && no.getNome() == "id" ){
   						//cout << "achei um id em decfunc " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"["+ this->streamTokens.at(indiceStream).getLexema() + "]",indiceToken);
   						return retornoF;
   					}else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "(" &&  no.getNome() == "(" ){
   						// testar  cout << "achei um ( em decfunc " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
   						return retornoF;
   					}else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == ")" && no.getNome() == ")" ){
   						//cout << "achei um ) em decfunc " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
   						return retornoF;
   					}else if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "def" && no.getNome() == "def" ){
   						//cout << "achei um def em decfunc " <<endl;
   						Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
   						return retornoF;
   					}
   				}else if(noPai.getNome() == "block"){
                  if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "{" && no.getNome() == "{" ){
                     //cout << "achei um { em block " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "}" && no.getNome() == "}" ){
                     //cout << "achei um } em block " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }

               }else if(noPai.getNome() == "paramList"){

                  if(this->streamTokens.at(indiceStream).getTipoToken() == "ID" && no.getNome() == "id" ){
                     //cout << "achei um { em block " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"["+ this->streamTokens.at(indiceStream).getLexema() +"]",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "," && no.getNome() == "," ){
                     //cout << "achei um } em block " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }

               }else if(noPai.getNome() == "stmt"){
                  if(this->streamTokens.at(indiceStream).getTipoToken() == "ID" && no.getNome() == "id" ){
                     //cout << "achei um id em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"["+ this->streamTokens.at(indiceStream).getLexema() +"]",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "=" && no.getNome() == "=" ){
                     //cout << "achei um = em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == ";" && no.getNome() == ";" ){
                     //cout << "achei um ; em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "return" && no.getNome() == "return" ){
                     //cout << "achei um ; em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "break" && no.getNome() == "break" ){
                     //cout << "achei um ; em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "continue" && no.getNome() == "continue" ){
                     //cout << "achei um continue em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "if" && no.getNome() == "if" ){
                     //cout << "achei um ; em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "else" && no.getNome() == "else" ){
                     //cout << "achei um else em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "KEY" && this->streamTokens.at(indiceStream).getLexema() == "while" && no.getNome() == "while" ){
                     //cout << "achei um ; em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "(" && no.getNome() == "(" ){
                     //cout << "achei um ; em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == ")" && no.getNome() == ")" ){
                     //cout << "achei um ; em stmt " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;
                  }
               
               }else if(noPai.getNome() == "expr"){
                  if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && (this->streamTokens.at(indiceStream).getLexema() == "--" || this->streamTokens.at(indiceStream).getLexema() == "-" ) && no.getNome() == "--" ){
                     inserirArvoreExpr("%","sinal");
                     //cout << "achei um -- em expr " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,this->streamTokens.at(indiceStream).getLexema(),indiceToken);
                     return retornoF;   
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "DEC" &&  no.getNome() == "decimal" ){
                     //this->pilhaNumeros.push("<"+this->streamTokens.at(indiceStream).getLexema()+ ">");
                     inserirArvoreExpr(this->streamTokens.at(indiceStream).getLexema(),"numero");
                     
                     //inserirAoFim(this->streamTokens.at(indiceStream).getLexema());
                     int tamanho = numeros.size();
                     std::stringstream Resultado;
                     Resultado << tamanho;
                     numeros.push_back(this->streamTokens.at(indiceStream).getLexema());
                     Retorno retornoF("0",1,"escopo",++indiceStream,"[ $"+Resultado.str()+" ]",indiceToken);   
                     return retornoF;
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "ID" && no.getNome() == "id" ){
                     //this->pilhaNumeros.push("<"+this->streamTokens.at(indiceStream).getLexema()+ ">");
                     //inserirAoFim(this->streamTokens.at(indiceStream).getLexema());
                     inserirArvoreExpr(this->streamTokens.at(indiceStream).getLexema(),"numero");
                     //cout << "achei um id em expr " <<endl;
                     int tamanho = numeros.size();
                     std::stringstream Resultado;
                     Resultado << tamanho;
                     numeros.push_back(this->streamTokens.at(indiceStream).getLexema());
                     Retorno retornoF("0",1,"escopo",++indiceStream,"[ $"+Resultado.str()+" ]",indiceToken);
                     return retornoF;   
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "(" && no.getNome() == "(" ){
                     //this->pilhaNumeros.push("["+this->streamTokens.at(indiceStream).getLexema()+ "]");
                     //cout << "achei um ( em expr " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;   
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == ")" && no.getNome() == ")" ){
                     //cout << "achei um ) em expr " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;   
                  }
               }else if(noPai.getNome() == "funcall"){

                  if(this->streamTokens.at(indiceStream).getTipoToken() == "ID"  && no.getNome() == "id" ){
                     //cout << "achei um id em funcall " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"["+this->streamTokens.at(indiceStream).getLexema() +"]",indiceToken);
                     return retornoF;   
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "(" && no.getNome() == "(" ){
                     //cout << "achei um ( em funcall " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;   
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == ")" && no.getNome() == ")" ){
                     //cout << "achei um ) em funcall " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;   
                  }


               }else if(noPai.getNome() == "argList"){
                  if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "," && no.getNome() == "," ){
                     //cout << "achei um , em argList " <<endl;
                     Retorno retornoF("0",1,"escopo",++indiceStream,"",indiceToken);
                     return retornoF;   
                  }
               }else if(noPai.getNome() == "assign"){
                  if(this->streamTokens.at(indiceStream).getTipoToken() == "ID" && no.getNome() == "id" ){                  
                     Retorno retornoF("0",1,"escopo",++indiceStream,"["+ this->streamTokens.at(indiceStream).getLexema() +"]",indiceToken);
                     return retornoF;   
                  }else if(this->streamTokens.at(indiceStream).getTipoToken() == "SYM" && this->streamTokens.at(indiceStream).getLexema() == "=" && no.getNome() == "=" ){
                     Retorno retornoF("0",1,"escopo",++indiceStream,"[assign",indiceToken);
                     return retornoF;   
                  }
               }
   			   
               //cout << "nao reconheci o token " << this->streamTokens.at(indiceStream).getTipoToken() << " lexema " << this->streamTokens.at(indiceStream).getLexema() << " no pai " << noPai.getNome() << " no.getNome() = " << no.getNome() <<endl; 	
   				
   				/*
   				for (int i = 0; i < no.getProducao().size(); ++i){
   					no.getProducao(indecieP, i);	
   				}
   				if(this->streamTokens.at(indiceStream) == no.producao()){

   				}
   				*/
   			}
   		
            
            Retorno retornoF("0",0,"escopo",indiceStream,"teste2",indiceToken);
            return retornoF;

   		};

   		std::string traduzirTokenStream(Token token){
   			//cout << "traduzirTokenStream" <<"  token.getTipoToken() " << token.getTipoToken() << " nome do token " << token.getLexema() << endl;	
   			if( token.getTipoToken() == "KEY" ){
   				//cout << "lexeka" << token.getLexema() << endl;
   				if(token.getLexema() == "int"){
   					//cout << "achei um int" << endl;
   					return "int";
   				}else if(token.getLexema() == "void"){
   					//cout << "achei um void" << endl;
   					return "void";
   				}
   			//cout << "achei um erro" << endl;
   			}
   			return "0"; // erro
   		};


         void gerarArquivo(char const *argv[]){      
            ofstream arquivo;
            arquivo.open(argv[2]);
            arquivo << arvoreAst ;               
            arquivo.close();
            
         };

};

#endif // ANALISADORSINTATICO_H_INCLUDED