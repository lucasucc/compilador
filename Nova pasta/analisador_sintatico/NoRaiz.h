#ifndef NORAIZ_H_INCLUDED
#define NORAIZ_H_INCLUDED

class NoRaiz{

	private:
		std::string nome;
		std::vector<std::string> producoes;
		std::vector<No> produzido;

	public:		
		NoRaiz(std::string nome,std::vector<std::string> producoes){
			this->nome = nome;
			this->producoes = producoes;
		};

		NoRaiz(){

		};

		std::string getNome(){
			return this->nome;
		};
};
#endif // NORAIZ_H_INCLUDED