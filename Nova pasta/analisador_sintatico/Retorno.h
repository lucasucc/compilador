#ifndef RETORNO_H_INCLUDED
#define RETORNO_H_INCLUDED

#include <iostream>
#include <sstream>

class Retorno{

	private:
		std::string caractereFalhou;
		int resultado; //0,1,2
		std::string escopo;
		int ultimoIndice;
		std::string subArvore;
		int indiceToken;

	public:		
		Retorno(){
			this->resultado = 0;
			this->ultimoIndice = 0;
		};

		Retorno(std::string caractereFalhou,int resultado,std::string escopo,int ultimoIndice ,std::string subArvore,int indiceToken ){
			this->caractereFalhou = caractereFalhou;
			this->resultado = resultado; //0,1,2
			this->escopo = escopo;
			this->ultimoIndice = ultimoIndice;
			this->subArvore = subArvore;			
			this->indiceToken = indiceToken;
		};

		std::string getCaractereFalhou(){
			return this->caractereFalhou;
		};
		
		int getResultado(){
			return this->resultado;
		};

		std::string getEscopo(){
			return this->escopo;
		};

		int getUltimoIndice(){
			return this->ultimoIndice;
		};

		void setResultado(int resultado){
			this->resultado = resultado;
		};

		void setUltimoIndice( int ultimoIndice){
			this->ultimoIndice = ultimoIndice;
		};

		std::string getSubArvore(){
			return this->subArvore;
		};

		void setSubArvore(std::string subArvore){
			this->subArvore = subArvore;
		};		

};
#endif // RETORNO_H_INCLUDED
