#ifndef ANALISADORLEXICO_H_INCLUDED
#define ANALISADORLEXICO_H_INCLUDED

#include "../token/Token.h"

class AnalisadorLexico{
	private:
		std::vector<std::string> tokens;		
		std::vector<Token> tokens2;
	public:				
		void definirToken(std::string lexema,int coluna,int linha){
			//std::cout << "definir token"<<lexema << std::endl;
			
			if( tokenKey(lexema) ){
				addToken(lexema,"KEY",coluna,linha);
			}else if( tokenId(lexema) ){
				addToken(lexema,"ID",coluna,linha);
			}else if( tokenDecimal(lexema) ){
				addToken(lexema,"DEC",coluna,linha);
			}else if( tokenSym(lexema,coluna,linha) ){
				


				// add dentro do tokenSym
				//tokenSym();
				//addToken(lexema,"SYM");
			}
			
		}

		bool ehTokenKey(std::string lexema){
			//lexema = lexema.substr(0,lexema.length()-1);			
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());			
			if( (aLexema.size() == 3 && aLexema[0] == 'i' && aLexema[1] == 'n' && aLexema[2] == 't') || (aLexema.size() == 4 && aLexema[0] == 'v' && aLexema[1] == 'o' && aLexema[2] == 'i' && aLexema[3] == 'd') ){
				return true;
			}else if( (aLexema.size() == 3 && aLexema[0] == 'd' && aLexema[1] == 'e' && aLexema[2] == 'f')  || (aLexema.size() == 2 && aLexema[0] == 'i' && aLexema[1] == 'f' ) ){
				return true;
			}else if( (aLexema.size() == 4 && aLexema[0] == 'e' && aLexema[1] == 'l' && aLexema[2] == 's' && aLexema[3] == 'e') || (aLexema.size() == 5 && aLexema[0] == 'w' && aLexema[1] == 'h' && aLexema[2] == 'i' && aLexema[3] == 'l' && aLexema[4] == 'e') ){
				return true;
			}else if( (aLexema.size() == 6 && aLexema[0] == 'r' && aLexema[1] == 'e' && aLexema[2] == 't' && aLexema[3] == 'u' && aLexema[4] == 'r' && aLexema[5] == 'n' )  ){
				return true;
			}else if( (aLexema.size() == 5 && aLexema[0] == 'b' && aLexema[1] == 'r' && aLexema[2] == 'e' && aLexema[3] == 'a' && aLexema[4] == 'k' )){
				return true;
			}else if( (aLexema.size() == 8 && aLexema[0] == 'c' && aLexema[1] == 'o' && aLexema[2] == 'n' && aLexema[3] == 't' && aLexema[4] == 'i' && aLexema[5] == 'n' && aLexema[6] == 'u' && aLexema[7] == 'e') ){
				return true;
			}				
			return false;			
		};

		void addToken(std::string lexema, std::string tipo, int coluna, int linha){
			//std::cout << "addToken" << std::endl; 
			Token token;
			token.setLexema(lexema);
			token.setTipoToken(tipo);
			token.setColuna(coluna);
			token.setLinha(linha);
			tokens2.push_back(token);
			//tokens.push_back(tipo);
		};

		bool podeSerInt(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 3 ){
				if( aLexema.at(0) == 'i'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'n'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 't'){
							return true;
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		};

		bool podeSerDef(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 3 ){
				if( aLexema.at(0) == 'd'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'e'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 'f'){
							return true;
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		};

		bool podeSerIf(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 2 ){
				if( aLexema.at(0) == 'i'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'f'){						
						return true;
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		};


		bool podeSerElse(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 4 ){
				if( aLexema.at(0) == 'e'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'l'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 's'){
							if( aLexema.size() >= 4 && aLexema.at(3) == 'e'){
								return true;
							}else if( aLexema.size() >= 4 ){
								return false;
							}
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		}

		bool podeSerVoid(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 4 ){
				if( aLexema.at(0) == 'v'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'o'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 'i'){
							if( aLexema.size() >= 4 && aLexema.at(3) == 'd'){
								return true;
							}else if( aLexema.size() >= 4 ){
								return false;
							}
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		}

		bool podeSerWhile(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 5 ){
				if( aLexema.at(0) == 'w'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'h'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 'i'){
							if( aLexema.size() >= 4 && aLexema.at(3) == 'l'){
								if(aLexema.size() >= 5 && aLexema.at(4) == 'e'){
									return true;
								}else if(aLexema.size() >= 5){
									return false;
								}
							}else if( aLexema.size() >= 4 ){
								return false;
							}
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		}

		bool podeSerBreak(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 5 ){
				if( aLexema.at(0) == 'b'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'r'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 'e'){
							if( aLexema.size() >= 4 && aLexema.at(3) == 'a'){
								if(aLexema.size() >= 5 && aLexema.at(4) == 'k'){
									return true;
								}else if(aLexema.size() >= 5){
									return false;
								}
							}else if( aLexema.size() >= 4 ){
								return false;
							}
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		}

		bool podeSerReturn(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 6 ){
				if( aLexema.at(0) == 'r'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'e'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 't'){
							if( aLexema.size() >= 4 && aLexema.at(3) == 'u'){
								if(aLexema.size() >= 5 && aLexema.at(4) == 'r'){
									if(aLexema.size() >= 6 && aLexema.at(5) == 'n'){
										return true;
									}else if( aLexema.size() >= 6 ){
										return false;
									}
								}else if(aLexema.size() >= 5){
									return false;
								}
							}else if( aLexema.size() >= 4 ){
								return false;
							}
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		}

		bool podeSerContinue(std::vector<char> aLexema){
			if( aLexema.size() > 0 && aLexema.size() <= 8 ){
				if( aLexema.at(0) == 'c'){
					if( aLexema.size() >= 2 && aLexema.at(1) == 'o'){
						if( aLexema.size() >= 3 && aLexema.at(2) == 'n'){
							if( aLexema.size() >= 4 && aLexema.at(3) == 't'){
								if(aLexema.size() >= 5 && aLexema.at(4) == 'i'){
									if(aLexema.size() >= 6 && aLexema.at(5) == 'n'){
										if(aLexema.size() >= 7 && aLexema.at(6) == 'u'){
											if(aLexema.size() >= 8 && aLexema.at(7) == 'e'){
												return true;
											}else if( aLexema.size() >= 8 ){
												return false;
											}
										}else if( aLexema.size() >= 7 ){
											return false;
										}
									}else if( aLexema.size() >= 6 ){
										return false;
									}
								}else if(aLexema.size() >= 5){
									return false;
								}
							}else if( aLexema.size() >= 4 ){
								return false;
							}
						}else if( aLexema.size() >= 3 ){
							return false;
						}
					}else if( aLexema.size() >= 2 ){
							return false;
					}
				}else if( aLexema.size() >= 1 ){
					return false;
				}
			}else{
				return false;
			}
			return true; 
		}


		bool podeSerTokenKey(std::string lexema){
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());	
			if(podeSerIf(aLexema)){
				return true;
			}else if( podeSerInt(aLexema) ){
				return true;
			}else if(podeSerDef(aLexema) ){
				return true;
			}else if(podeSerElse(aLexema) ){
				return true;
			}else if(podeSerVoid(aLexema) ){
				return true;
			}else if(podeSerBreak(aLexema)){
				return true;
			}else if(podeSerWhile(aLexema)){
				return true;
			}else if(podeSerReturn(aLexema)){
				return true;
			}else if(podeSerContinue(aLexema)){
				return true;
			}
			return false;
		}

		bool tokenKey(std::string lexema){
			//lexema = lexema.substr(0,lexema.length()-1);			
			//std::cout << "token Key" << std::endl; 
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());			
			if( (aLexema.size() == 3 && aLexema[0] == 'i' && aLexema[1] == 'n' && aLexema[2] == 't') || (aLexema.size() == 4 && aLexema[0] == 'v' && aLexema[1] == 'o' && aLexema[2] == 'i' && aLexema[3] == 'd') ){
				//addToken(lexema,"KEY");
				return true;
			}else if( (aLexema.size() == 3 && aLexema[0] == 'd' && aLexema[1] == 'e' && aLexema[2] == 'f')  || (aLexema.size() == 2 && aLexema[0] == 'i' && aLexema[1] == 'f' ) ){
				//addToken(lexema,"KEY");
				return true;
			}else if( (aLexema.size() == 4 && aLexema[0] == 'e' && aLexema[1] == 'l' && aLexema[2] == 's' && aLexema[3] == 'e') || (aLexema.size() == 5 && aLexema[0] == 'w' && aLexema[1] == 'h' && aLexema[2] == 'i' && aLexema[3] == 'l' && aLexema[4] == 'e') ){
				//addToken(lexema,"KEY");
				return true;
			}else if( (aLexema.size() == 6 && aLexema[0] == 'r' && aLexema[1] == 'e' && aLexema[2] == 't' && aLexema[3] == 'u' && aLexema[4] == 'r' && aLexema[5] == 'n' )  ){
				//addToken(lexema,"KEY");
				return true;
			}else if( (aLexema.size() == 5 && aLexema[0] == 'b' && aLexema[1] == 'r' && aLexema[2] == 'e' && aLexema[3] == 'a' && aLexema[4] == 'k' )){
				//addToken(lexema,"KEY");
				return true;
			}else if( (aLexema.size() == 8 && aLexema[0] == 'c' && aLexema[1] == 'o' && aLexema[2] == 'n' && aLexema[3] == 't' && aLexema[4] == 'i' && aLexema[5] == 'n' && aLexema[6] == 'u' && aLexema[7] == 'e') ){
				//addToken(lexema,"KEY");
				return true;
			}				
			return false;			
		};

		bool primeiroCaracterAlfabetico(std::vector<char> aLexema){
			//std::cout << "primeiro " <<aLexema.at(0) << std::endl;
			if( aLexema.size() >= 1 ){
				int decimalAscii = int(aLexema.at(0));	
				if( (decimalAscii >= int('A') && decimalAscii <= int('Z')) || decimalAscii >= int('a') && decimalAscii <= int('z')){
					return true;
				}				
			}
			return false;
		}

		bool tokenId(std::string lexema){			
			setlocale(LC_ALL, "portuguese");
			std::string acumulado;
			//lexema = lexema.substr(0,lexema.length()-1);			
			//std::cout << "funcao tokenID" << lexema << std::endl;
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());						
			if(primeiroCaracterAlfabetico(aLexema)){
				//std::cout << "primeiroCaracterAlfabetico" << std::endl;
				for (int i = 0; i < aLexema.size(); ++i){										
					if(std::isalpha(aLexema.at(i))!= 0 || std::isdigit(aLexema.at(i))!= 0 || aLexema.at(i) == '_'){
						acumulado.push_back(aLexema.at(i));
					}else{
						return false;
					}
				}		
			}else{				
				return false;
			}			
			//addToken(lexema,"ID");
			return true;
		};

		bool ehTokenId(std::string lexema){			
			//setlocale(LC_ALL, "portuguese");
			std::string acumulado;
			//lexema = lexema.substr(0,lexema.length()-1);			
			//std::cout << "ehTokenID " << lexema << std::endl;
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());						
			if(primeiroCaracterAlfabetico(aLexema)){
				for (int i = 0; i < aLexema.size(); ++i){										
					if(std::isalpha(aLexema.at(i))!= 0 || std::isdigit(aLexema.at(i))!= 0 || aLexema.at(i) == '_'){
						acumulado.push_back(aLexema.at(i));
					}else{
						return false;
					}
				}		
			}else{				
				return false;
			}			
			return true;
		};


		bool tokenSym(std::string lexema,int coluna,int linha){			
			std::string acumulado;
			//lexema = lexema.substr(0,lexema.length()-1);
			//std::cout << "aquiiiiii" << lexema.size() << lexema<<std:: endl;
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());
			for (int i = 0; i < aLexema.size(); ++i){
				//std::cout <<"TOKEN SYM" << aLexema.at(i) << std::endl;
				if(aLexema.at(i) == '{' || aLexema.at(i) == '}' || aLexema.at(i) == ';' || aLexema.at(i) == '+' || aLexema.at(i) == '*' || aLexema.at(i) == '[' || aLexema.at(i) == ']'){
					acumulado.push_back(aLexema.at(i));
					addToken(acumulado,"SYM",coluna,linha);
					acumulado.erase();
					//return true;
				}else if(aLexema.at(i) == '(' || aLexema.at(i) == ')' ){
					acumulado.push_back(aLexema.at(i));
					addToken(acumulado,"SYM",coluna,linha);
					acumulado.erase();
					//return true;
				}else if(aLexema.at(i) == ','){
					acumulado.push_back(aLexema.at(i));
					addToken(acumulado,"SYM",coluna,linha);
					acumulado.erase();
					//return true;
				}else if( aLexema.at(i) == '/' && (aLexema.size() > i+1) && aLexema.at(i+1) == '/' ){
					
					acumulado.push_back(aLexema.at(i));
					acumulado.push_back(aLexema.at(i+1));
					addToken(acumulado,"SYM",coluna,linha);
					acumulado.erase();
					i++;
					//comentario tem q implementar
					//return true;
				}else if( aLexema.at(i) == '/' ){
					acumulado.push_back(aLexema.at(i));
					addToken(acumulado,"SYM",coluna,linha);
					acumulado.erase();
					//return
				}else if( aLexema.at(i) == '&' && aLexema.size() > i+1 && aLexema.at(i+1) == '&' ){ 
					acumulado.push_back(aLexema.at(i));
					acumulado.push_back(aLexema.at(i+1));
					addToken(acumulado,"SYM",coluna,linha);
					acumulado.erase();
					i++;
					//return true;
				}else if( aLexema.at(i) == '|' && aLexema.size() > i+1 && aLexema.at(i+1) == '|'  ){
					acumulado.push_back(aLexema.at(i));
					acumulado.push_back(aLexema.at(i+1));
					addToken(acumulado,"SYM",coluna,linha);
					acumulado.erase();
					i++;
					//return true;
				}else if( aLexema.at(i) == '-' ){
						if( aLexema.size() > i+1 && aLexema.at(i+1) == '-'){
							acumulado.push_back(aLexema.at(i));
							acumulado.push_back(aLexema.at(i+1));
							addToken(acumulado,"SYM",coluna,linha);
							acumulado.erase();
							i++;
							//return true;
						}else{ 							
							acumulado.push_back(aLexema.at(i));
							addToken(acumulado,"SYM",coluna,linha);
							acumulado.erase();
						}
					}else if( aLexema.at(i) == '!' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								acumulado.push_back(aLexema.at(i+1));
								addToken(acumulado,"SYM",coluna,linha);
								acumulado.erase();
								i++;
								
								//return true;
							}else {
								acumulado.push_back(aLexema.at(i));
								addToken(acumulado,"SYM",coluna,linha);
								acumulado.erase();
								//return true;								
							}
					}else if( aLexema.at(i) == '<' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								acumulado.push_back(aLexema.at(i+1));
								addToken(acumulado,"SYM",coluna,linha);
								i++;
								acumulado.erase();
								//return true;
							}else {
								acumulado.push_back(aLexema.at(i));
								addToken(acumulado,"SYM",coluna,linha);
								acumulado.erase();
								//return true;
							}
					}else if( aLexema.at(i) == '>' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								acumulado.push_back(aLexema.at(i+1));
								addToken(acumulado,"SYM",coluna,linha);
								acumulado.erase();
								//return true;
								i++;
							}else {
								acumulado.push_back(aLexema.at(i));
								addToken(acumulado,"SYM",coluna,linha);
								acumulado.erase();
								//return true;
							}
					}else if( aLexema.at(i) == '=' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								acumulado.push_back(aLexema.at(i+1));
								addToken(acumulado,"SYM",coluna,linha);
								i++;
								acumulado.erase();
								//return true;
							}else {
								acumulado.push_back(aLexema.at(i));
								addToken(acumulado,"SYM",coluna,linha);
								acumulado.erase();
								//return false;
							}

					} 				
			} //fim for
			//return false;
		}; // fim  metodo	

		bool ehTokenSym(std::string lexema){			
			std::string acumulado;
			//lexema = lexema.substr(0,lexema.length()-1);
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());
			for (int i = 0; i < aLexema.size(); ++i){
				if(aLexema.at(i) == '{' || aLexema.at(i) == '}' || aLexema.at(i) == '[' || aLexema.at(i) == ']' ){
					return true;
				}else if(aLexema.at(i) == '(' || aLexema.at(i) == ')' ){
					return true;
				}else if(aLexema.at(i) == ','){
					return true;
				}else if( !(aLexema.size() > i+1) && (aLexema.at(i) == '+' || aLexema.at(i) == '*' || aLexema.at(i) == '/' ) ){
					acumulado.push_back(aLexema.at(i));
					return true;
				}else if( aLexema.at(i) == '&' && aLexema.size() > i+1 && aLexema.at(i+1) == '&' ){ 
					acumulado.push_back(aLexema.at(i));
					acumulado.push_back(aLexema.at(i+1));
					return true;
				}else if( aLexema.at(i) == '|' && aLexema.size() > i+1 && aLexema.at(i+1) == '|'  ){
					acumulado.push_back(aLexema.at(i));
					acumulado.push_back(aLexema.at(i+1));
					return true;
				}else if( aLexema.at(i) == '-' ){
						if( aLexema.size() > i+1 && aLexema.at(i+1) == '-'){
							acumulado.push_back(aLexema.at(i));
							return true;
						}else if(!(aLexema.size() > i+1)){
							acumulado.push_back(aLexema.at(i));
							return true;							
						}else{
							return false;
						}
					}else if( aLexema.at(i) == '!' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								return true;
							}else if( !(aLexema.size() > i+1) ){
								acumulado.push_back(aLexema.at(i));
								return true;								
							}else{
								return false;
							}
					}else if( aLexema.at(i) == '<' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								return true;
							}else if( ! (aLexema.size() > i+1) ){
								acumulado.push_back(aLexema.at(i));
								return true;
							}else {
								return false;
							}
					}else if( aLexema.at(i) == '>' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								return true;
							}else if( ! (aLexema.size() > i+1) ){
								acumulado.push_back(aLexema.at(i));
								return true;
							}else {
								return false;
							}
					}else if( aLexema.at(i) == '=' ){
							if( aLexema.size() > i+1 && aLexema.at(i+1) == '=' ){
								acumulado.push_back(aLexema.at(i));
								return true;
							}else if( ! (aLexema.size() > i+1) ){
								acumulado.push_back(aLexema.at(i));
								return true;
							}else {
								return false;
							}
					} 				
			} //fim for
			return false;
		} // fim  metodo

		bool podeSerTokenSym(char caractere){						
			if( caractere == '{' || caractere == '}' || caractere == '(' || caractere == ')' || caractere == '!' || caractere == '=' || caractere == '<' || caractere == '>' || caractere == '/' || caractere == '|' || caractere == '&' || caractere == ',' || caractere == '-' || caractere == '*' || caractere == '+' || caractere == ';' || caractere == '[' || caractere == ']'  ){
				return true;
			}
			
			return false;
		}


		bool ehTokenDecimal(std::string lexema){
			//lexema = lexema.substr(0,lexema.length()-1);
			//std::cout << lexema << std::endl;
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());
			std::string acumulado;
			for (int i = 0; i < aLexema.size(); ++i){
				if( std::isdigit(aLexema.at(i)) ){
					acumulado.push_back(aLexema.at(i));
				}else{
					return false;
				}
			}
			//std::cout << "eh token decimal" << std::endl;
			return true;
		}

		bool tokenDecimal(std::string lexema){
			//lexema = lexema.substr(0,lexema.length()-1);
			std::vector<char> aLexema(lexema.c_str(), lexema.c_str() + lexema.size());
			std::string acumulado;
			for (int i = 0; i < aLexema.size(); ++i){
				if(std::isdigit(aLexema.at(i)) ){
					acumulado.push_back(aLexema.at(i));
				}else{
					return false;
				}
			}
		}

		void imprimirTokens(){
			//std::cout << std::endl;
			for (int i = 0; i < tokens2.size(); ++i){
				if( tokens2.at(i).getTipoToken() == "ID"){
					std::cout<<tokens2.at(i).getTipoToken()<<"   \""<<tokens2.at(i).getLexema()<<"\""<<std::endl;	
				}else{
					std::cout<<tokens2.at(i).getTipoToken()<<"  \""<<tokens2.at(i).getLexema()<<"\""<<std::endl;
				}
				
			}
			//std::cout << std::endl;
		}


		std::vector<Token> getTokens2(){
			return this->tokens2;
		};	

};

#endif // ANALISADORLEXICO_H_INCLUDED