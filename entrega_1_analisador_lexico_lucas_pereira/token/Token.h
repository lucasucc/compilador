#ifndef TOKEN_H_INCLUDED
#define TOKEN_H_INCLUDED

class Token{

private:
	std::string tipoToken;
	std::string nomeToken;
	int linha;
	int coluna;
	std::string lexema;
	std::string tipoLexema;
	std::string valorLexema;

public:		
	void setTipoToken(std::string tipoToken){
		this->tipoToken = tipoToken;
	};

	void setNomeToken(std::string nomeToken){
		this->nomeToken = nomeToken;
	};

	void setLinha(int linha){
		this->linha = linha;
	};

	void setColuna(int coluna){
		this->coluna = coluna;
	};

	void setLexema(std::string lexema){
		this->lexema = lexema;
	};

	void setTipoLexema(std::string tipoLexema){
		this->tipoLexema = tipoLexema;
	};

	void setValorLexema(std::string valorLexema){
		this->valorLexema = valorLexema;
	};

	std::string getTipoToken(){
		return this->tipoToken;
	};

	std::string getNomeToken(){
		return this->nomeToken;
	};

	int getLinha(){
		return this->linha;
	};

	int getColuna(){
		return this->coluna;
	};

	std::string getLexema(){
		return this->lexema;
	};

	std::string getTipoLexema(){
		return this->tipoLexema;
	};

	std::string getValorLexema(){
		return this->valorLexema;
	};

};
#endif // TOKEN_H_INCLUDED