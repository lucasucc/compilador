#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <locale>
#include "analisador_lexico/AnalisadorLexico.h"
#include "Scanner.cpp"

using namespace std;


int main(int argc, char const *argv[]){
	Scanner scanner;
	scanner.executar(argc, argv);
	scanner.gerarArquivo(argv);
	return 0;
}