typedef enum { Dec, Id, Ast } TipoNo;

typedef struct {
    int decimal;                
} NoDecimal;

typedef struct {
    char id[100];                      
} NoId;

typedef struct {
	int tokenOperacao;
	int qtdFilhos;                                      
    struct Noast **filhos;	
} NoOperacao;

typedef struct Noast {
    union {
        NoId noId;
        NoDecimal noDecimal;        
        NoOperacao noOperacao;        
    };
    TipoNo tipoNo;    
} NoAst;
