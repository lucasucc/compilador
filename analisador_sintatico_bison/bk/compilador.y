%{

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <stdarg.h>
#include "NoisDaAst.h"

#define YYDEBUG 1
extern int yylex();
extern int yyparse();
extern FILE* yyin;
extern FILE* f;

//extern int i=0;
void printArvore(NoAst *no);
void inicioPrint(NoAst *no);
void yyerror(const char* s);
NoAst *criarNoAst(int idToken, int qtdFilhos, ...);
NoAst *criarNoDecimal(int idToken);
NoAst *criarNoId(char token[100]);
NoAst * root = NULL , *root1 = NULL;
%}
%error-verbose

%union {
	int ival;	
	char *cval;	
	char identificadores[100];
	NoAst *AstP;
	char tipo[5];
}

//%token<ival> T_DECIMAL
%token<fval> T_FLOAT
%token<identificadores> T_TYPE
%token T_PLUS T_MINUS T_MULTIPLY T_DIVIDE T_LEFT T_RIGHT
%token T_NEWLINE T_QUIT T_E T_OU
%token T_MAIOR_IGUAL T_NEGACAO
%token T_ID T_IGUAL T_PONTO_VIRGULA T_DECIMAL T_MAIOR
%token T_DECVAR T_DIFERENCA_IGUAL T_IGUAL_IGUAL T_MENOR T_MENOR_IGUAL T_PROGRAM
%token<tipo> T_TYPE_INT T_TYPE_VOID

%left T_E
%left T_OU
%left T_MENOR T_MENOR_IGUAL T_MAIOR T_MAIOR_IGUAL
%left T_IGUAL_IGUAL T_DIFERENCA_IGUAL
%left T_PLUS T_MINUS
%left T_MULTIPLY T_DIVIDE
%left T_NEGACAO
%left T_LEFT T_RIGHT

%type<ival> expression T_DECIMAL
%type<identificadores> T_ID
%type<AstP> program decvar expr 
%type<tipo> type

%locations

%start program

%%

//inicio : | program inicio {$$ = printArvore($1);};	   

program : | decvar program { inicioPrint(root); }

;

decvar: 
	//type T_ID T_PONTO_VIRGULA { printf("testar com 1 regra"); $$ = criarNoAst(T_DECVAR,1,criarNoId($2)); }
	type T_ID T_IGUAL expr T_PONTO_VIRGULA { printf("testar com outra regra"); $$ = criarNoAst(T_DECVAR,2,criarNoId($2)); root1 = $$; if(root != NULL){ root1->noOperacao.filhos[1] = root; root = root1; } else{root = $$;} }
	|expr
;


expr:   T_DECIMAL { $$ = criarNoDecimal($1); root = $$;}
	  	//T_DECIMAL { $$ =  criarNoAst(T_DECIMAL,1,criarNoDecimal($1)) ;} 
	  | T_ID {$$ = criarNoId($1);}	  
;

/*
line: T_NEWLINE    
    | expression T_NEWLINE { printf("\t int Result: %i\n", $1); } 
    | T_QUIT T_NEWLINE { printf("bye!\n"); exit(0); }
    | type T_NEWLINE { printf("\t type Result: %s\n", $1); }
;
*/
/*
expression: T_DECIMAL { $$ = $1; }
	  | T_DECIMAL {$$ = 10;}
	  | T_ID {$$ = 11;}
	  | expression T_PLUS expression	{ $$ = $1 + $3; }
	  | expression T_MINUS expression	{ $$ = $1 - $3; }
	  | expression T_MULTIPLY expression	{ $$ = $1 * $3; }
	  | T_LEFT expression T_RIGHT		{ $$ = $2; }
;
*/

type: 
	T_TYPE_INT {  } 
	| T_TYPE_VOID { }
;
%%
char subArvoreIncompleta[1000];
char subArvore[1000];

int main(int argc, char * argv[]) {
	//yyin = stdin;
	//yyin
	/*
	do { 
		yyparse();
	} while(!feof(yyin));
	*/
	//yyparse();
	char program[1000] = "[program ";
	char fechar[] = "]";
	FILE *f;
	printf("\n teste \n");	
	f = fopen(argv[2], "w");
	yyin = fopen( argv[1], "r" );
	yyparse();
	printf("\n teste3 \n");	
	strcat(program,subArvore);
	strcat(program,fechar);
	strcpy(subArvore,program);
	printf("%s\n",subArvore);
	//do {
		
	//} while ();

	fclose(f);
	
	return 0;
}



void inicioPrint(NoAst *no){
		
	//printf("[program ");	
	//subArvoreIncompleta = "";
	strcpy(subArvoreIncompleta,"");
	printArvore(no);	
	strcat(subArvoreIncompleta,subArvore);
	strcpy(subArvore,subArvoreIncompleta);
	
	
}

void printArvore(NoAst *no){
	char *snum;
	char vet[100];
	//printf("chegou aqui \n");
	if(no != NULL){
		//printf("nao eh null \n");
		switch(no->tipoNo){

			case Ast :
				switch(no->noOperacao.tokenOperacao){
					case T_DECVAR:
						strcpy(vet,"[decvar ");
						strcat(subArvoreIncompleta,vet);
						//subArvoreIncompleta = subArvoreIncompleta + "[decvar";
						//printf("[decvar ");
						printf("no->noOperacao.filhos[0] = %i\n", sizeof(no->noOperacao.filhos)/sizeof(NoAst *));
						printArvore(no->noOperacao.filhos[0]);
						//subArvoreIncompleta = subArvoreIncompleta + "]";
						//printArvore(no->noOperacao.filhos[1]);
						strcat(subArvoreIncompleta,"]");
						//printf("]");
						break;
					
						/*
						if (an->noOperacao.filhos[1]->tipoNodo == Dec) {
			                printf(" %d\n", an->noOperacao.filhos[1]->noDecimal.decimal);
			            } else {
							/*
							printf(" 0\n");
							an->astn.tokenOpr = EQUAL;
							atribuicoes++;
							Atribuicoes[atribuicoes] = an;
							*/
						//}
							//break;


				}

				
				break;
			case Dec:
				sprintf(snum, "%d", no->noDecimal.decimal);
				//itoa(no->noDecimal.decimal, snum, 10);
				strcat(subArvoreIncompleta,"[");
				strcat(subArvoreIncompleta,snum);
				strcat(subArvoreIncompleta,"]");
				printf("[ decimal=");
				printf("%i",no->noDecimal.decimal);	
				printf("]");
				return ;
			case Id:
				strcat(subArvoreIncompleta,"[");
				strcat(subArvoreIncompleta,no->noId.id);
				strcat(subArvoreIncompleta,"]");
				printf("[");
				printf("%s",no->noId.id );		
				printf("]");
				//printArvore(no->noOperacao.filhos[0]);
				return ;
		}

	}else{
		//printf("eh null \n");
	}

}


NoAst *criarNoAst(int idToken, int qtdFilhos, ...){
	//printf("vai criar no ast %i",idToken);
	va_list ap;	
	NoAst *novoNoAst = malloc(sizeof(NoAst));
	novoNoAst->noOperacao.filhos = malloc(qtdFilhos * sizeof(NoAst *));
	va_start(ap, qtdFilhos);
	novoNoAst->tipoNo = Ast;
	novoNoAst->noOperacao.tokenOperacao = idToken;
	novoNoAst->noOperacao.qtdFilhos = qtdFilhos;
	printf("qtdFilhos = %i\n",qtdFilhos);
	
	for (int i = 0; i < qtdFilhos; ++i)
	{
		printf("cada loop");
		novoNoAst->noOperacao.filhos[i] = va_arg(ap, NoAst* );
		//novoNoAst->noOperacao.filhos[i] = malloc(sizeof(NoAst));
	}

	va_end(ap);
	return novoNoAst;
}

NoAst *criarNoDecimal(int idToken){
	printf("\n foi criaro um no decimal %i \n",idToken);
	NoAst *novoNoAst = malloc(sizeof(NoAst*));	
	novoNoAst->tipoNo = Dec;
	novoNoAst->noDecimal.decimal = idToken;	
	return novoNoAst;
}

NoAst *criarNoId(char token[100]){	
	//printf("\n token = %s\n", token );
	NoAst *novoNoAst = malloc(sizeof(NoAst));		
	novoNoAst->tipoNo = Id;
	strcpy(novoNoAst->noId.id, token);	
	return novoNoAst;
}

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}
