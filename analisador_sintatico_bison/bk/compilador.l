%{
#include <stdio.h>

#define YY_DECL int yylex()

#include "NoisDaAst.h"
#include "compilador.tab.h"

//FILE *f;
%}

%option noyywrap

letra  [A-Za-z] 
DECIMAL [0-9][0-9]* 
ID      ({letra}|_)({letra}|({DECIMAL}|_))*

%%

[ \n\t]	 // ignore all whitespace
"+"		{return T_PLUS;}
"-"		{return T_MINUS;}
"*"		{return T_MULTIPLY;}
"/"		{return T_DIVIDE;}
"("		{return T_LEFT;}
")"		{return T_RIGHT;}
"exit"  {return T_QUIT;}
"quit"	{return T_QUIT;}
"int"	{ strcpy( yylval.tipo, yytext); return T_TYPE_INT;}
"void"  { strcpy( yylval.tipo, yytext); return T_TYPE_VOID;}
";"     {return T_PONTO_VIRGULA;}
"="     {return T_IGUAL;}
"<"     {return T_MENOR;}
"<="    {return T_MENOR_IGUAL;} 
">"     {return T_MAIOR;}
">="    {return T_MAIOR_IGUAL;}
"=="    {return T_IGUAL_IGUAL;} 
"!="    {return T_DIFERENCA_IGUAL;}
"&&"    {return T_E;}
"||"    {return T_OU;} 
"!"     {return T_NEGACAO;} 
{ID}    {strcpy(yylval.identificadores, yytext); return T_ID;}
{DECIMAL} {yylval.ival = atoi(yytext); return T_DECIMAL;}

%%

