%{

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <stdarg.h>
#include "NoisDaAst.h"

#define YYDEBUG 1
extern int yylex();
extern int yyparse();
extern FILE* yyin;
extern FILE* f;

//extern int i=0;
void printArvore(NoAst *no);
void inicioPrint(NoAst *no);
void yyerror(const char* s);
NoAst *criarNoAst(int idToken, int qtdFilhos, ...);
NoAst *criarNoDecimal(int idToken);
NoAst *criarNoId(char token[100]);
NoAst * root = NULL , *root1 = NULL;

char subArvoreIncompleta2[1000];
char subArvore2[1000];

%}
%error-verbose

%union {
	int ival;	
	char *cval;	
	char identificadores[100];
	NoAst *AstP;
	char tipo[5];
}

//%token<ival> T_DECIMAL
%token<fval> T_FLOAT
%token<identificadores> T_TYPE
%token T_PLUS T_MINUS T_MULTIPLY T_DIVIDE T_LEFT T_RIGHT
%token T_NEWLINE T_QUIT T_E T_OU ATTRIB
%token T_MAIOR_IGUAL T_NEGACAO
%token T_ID T_IGUAL T_PONTO_VIRGULA T_DECIMAL T_MAIOR
%token T_DECVAR T_DIFERENCA_IGUAL T_IGUAL_IGUAL T_MENOR T_MENOR_IGUAL T_PROGRAM
%token<tipo> T_TYPE_INT T_TYPE_VOID

%left T_E
%left T_OU
%left T_MENOR T_MENOR_IGUAL T_MAIOR T_MAIOR_IGUAL
%left T_IGUAL_IGUAL T_DIFERENCA_IGUAL
%left T_PLUS T_MINUS
%left T_MULTIPLY T_DIVIDE
%left T_NEGACAO
%left T_LEFT T_RIGHT

%type<ival> expression T_DECIMAL
%type<identificadores> T_ID
%type<AstP> program decvar expr lista_comando bloco nobr_statement
%type<tipo> type

%locations

%start bloco

%%
bloco : lista_comando  { } ;


lista_comando :
//lista_comando decvar {$$ = criarNoAst(T_DECVAR, 2, $2, $1); }
lista_comando nobr_statement { printf("[decvar "); $$ = criarNoAst(T_DECVAR, 2, $2, $1); printf("]"); }
//lista_comando nobr_statement SEMICOLON %prec STMT {$$ = criar_nodo_ast(STMT, 2, $2, $1); } |
| {$$ = NULL;}
 
;



nobr_statement:

 decvar {$$ = $1;}
//IDENTIFIER EQUAL expression {checarVariaveis($1); $$ = criar_nodo_ast(EQUAL, 2, identificador($1),$3); }|
//PRINT LPAREN expression RPAREN {$$ = criar_nodo_ast(PRINT, 1, $3);} |
//atribuirVariavel {$$ = $1;} |
//WHILE expression DO lista_comando END{ $$ = criar_nodo_ast(WHILE, 3, $2, $4); } |
//IF expression THEN lista_comando END %prec IFX { $$ = criar_nodo_ast(IF, 2, $2, $4); } |
//IF expression THEN lista_comando ELSE lista_comando END { $$ = criar_nodo_ast(IF, 3, $2, $4, $6); } 


;
/*
expression :
FALSE {$$ = constante($1);}
| TRUE  {$$ = constante($1);}
| DIGSEQ {$$ = constante($1);}
| IDENTIFIER {checarVariaveis($1); $$ = identificador($1);}
| expression PLUS expression { $$ = criar_nodo_ast(PLUS, 2, $1, $3); }
| expression MINUS expression { $$ = criar_nodo_ast(MINUS, 2, $1, $3); }
| expression STAR expression  { $$ = criar_nodo_ast(STAR, 2, $1, $3); }
| expression SLASH expression { $$ = criar_nodo_ast(SLASH, 2, $1, $3); }
| expression OR expression  { $$ = criar_nodo_ast(OR, 2, $1, $3); }
| expression AND expression { $$ = criar_nodo_ast(AND, 2, $1, $3); }
| expression LT expression  { $$ = criar_nodo_ast(LT, 2, $1, $3); }
| expression LE expression  { $$ = criar_nodo_ast(LE, 2, $1, $3); }
| expression GT expression  { $$ = criar_nodo_ast(GT, 2, $1, $3); }
| expression GE expression  { $$ = criar_nodo_ast(GE, 2, $1, $3); }
| expression EQUALEQUAL expression { $$ = criar_nodo_ast(EQUALEQUAL, 2, $1, $3); }
| expression NOTEQUAL expression { $$ = criar_nodo_ast(NOTEQUAL, 2, $1, $3); }
| NOT expression {$$ = criar_nodo_ast(NOT, 1, $2);}
| MINUS expression %prec UMINUS {$$ = criar_nodo_ast(UMINUS, 1, $2);}
| LPAREN expression RPAREN {$$ = $2;}
;
*/ 
/*
atribuirVariavel:
        VAR IDENTIFIER %prec ATTRIB                 { mapearVariavel($2); $$ = criar_nodo_ast(ATTRIB, 2, identificador($2), constante(0));}
        | VAR IDENTIFIER EQUAL expression %prec ATTRIB    { mapearVariavel($2); $$ = criar_nodo_ast(ATTRIB, 2, identificador($2), $4); }
*/

decvar :
		type T_ID T_PONTO_VIRGULA { $$ = criarNoAst(ATTRIB,2,criarNoId($2),criarNoDecimal(0) ); }
		| type T_ID T_IGUAL expr T_PONTO_VIRGULA { printf("["); printf("%s", $2); printf("]");  $$ = criarNoAst(ATTRIB, 2, criarNoId($2), $4); }
;

expr:  
	T_DECIMAL { printf("["); printf("%i",$1); printf("]");  $$ = criarNoDecimal($1);}
;

type: 
	| T_TYPE_INT {  } 
	| T_TYPE_VOID { }
;
%%
char subArvoreIncompleta[1000];
char subArvore[1000];

int main(int argc, char * argv[]) {
	//yyin = stdin;
	//yyin
	/*
	do { 
		yyparse();
	} while(!feof(yyin));
	*/
	//yyparse();
	char program[1000] = "[program ";
	char fechar[] = "]";
	FILE *f;
	printf("\n teste \n");	
	f = fopen(argv[2], "w");
	yyin = fopen( argv[1], "r" );
	yyparse();
	printf("\n teste3 \n");	
	strcat(program,subArvore);
	strcat(program,fechar);
	strcpy(subArvore,program);
	printf("%s\n",subArvore);
	//do {
		
	//} while ();

	fclose(f);
	
	return 0;
}



void inicioPrint(NoAst *no){
		
	//printf("[program ");	
	//subArvoreIncompleta = "";
	strcpy(subArvoreIncompleta,"");
	printArvore(no);	
	strcat(subArvoreIncompleta,subArvore);
	strcpy(subArvore,subArvoreIncompleta);
	
	
}

void printArvore(NoAst *no){
	char *snum;
	char vet[100];
	//printf("chegou aqui \n");
	if(no != NULL){
		printf("nao eh null \n");
		switch(no->tipoNo){

			case Ast :
				switch(no->noOperacao.tokenOperacao){
					case T_DECVAR:
						strcpy(vet,"[decvar ");
						strcat(subArvoreIncompleta,vet);
						//subArvoreIncompleta = subArvoreIncompleta + "[decvar";
						//printf("[decvar ");
						printf("no->noOperacao.filhos[0] = %i\n", sizeof(no->noOperacao.filhos)/sizeof(NoAst *));
						printArvore(no->noOperacao.filhos[0]);
						//subArvoreIncompleta = subArvoreIncompleta + "]";
						//printArvore(no->noOperacao.filhos[1]);
						strcat(subArvoreIncompleta,"]");
						//printf("]");
						break;
					
						case ATTRIB:
							printf("\n attrib no->noOperacao.filhos[0] = %i\n", sizeof(no->noOperacao.filhos)/sizeof(NoAst *));
							//printf(""no->noOperacao.filhos[0]->tipoNo);
							printf("\n ATTRIB AQUI \n");
						break;

				}

				
				break;
			case Dec:
				sprintf(snum, "%d", no->noDecimal.decimal);
				//itoa(no->noDecimal.decimal, snum, 10);
				strcat(subArvoreIncompleta,"[");
				strcat(subArvoreIncompleta,snum);
				strcat(subArvoreIncompleta,"]");
				printf("[ decimal=");
				printf("%i",no->noDecimal.decimal);	
				printf("]");
				return ;
			case Id:
				strcat(subArvoreIncompleta,"[");
				strcat(subArvoreIncompleta,no->noId.id);
				strcat(subArvoreIncompleta,"]");
				printf("[");
				printf("%s",no->noId.id );		
				printf("]");
				//printArvore(no->noOperacao.filhos[0]);
				return ;
		}

	}else{
		printf("eh null \n");
	}

}


NoAst *criarNoAst(int idToken, int qtdFilhos, ...){
	//printf("vai criar no ast %i",idToken);
	va_list ap;	
	NoAst *novoNoAst = malloc(sizeof(NoAst));
	novoNoAst->noOperacao.filhos = malloc(qtdFilhos * sizeof(NoAst *));
	va_start(ap, qtdFilhos);
	novoNoAst->tipoNo = Ast;
	novoNoAst->noOperacao.tokenOperacao = idToken;
	novoNoAst->noOperacao.qtdFilhos = qtdFilhos;
	//printf("qtdFilhos = %i\n",qtdFilhos);
	
	for (int i = 0; i < qtdFilhos; ++i)
	{
		//printf("cada loop");
		novoNoAst->noOperacao.filhos[i] = va_arg(ap, NoAst* );
		//novoNoAst->noOperacao.filhos[i] = malloc(sizeof(NoAst));
	}

	va_end(ap);
	return novoNoAst;
}

NoAst *criarNoDecimal(int idToken){
	//printf("\n foi criaro um no decimal %i \n",idToken);
	NoAst *novoNoAst = malloc(sizeof(NoAst*));	
	novoNoAst->tipoNo = Dec;
	novoNoAst->noDecimal.decimal = idToken;	
	return novoNoAst;
}

NoAst *criarNoId(char token[100]){	
	//printf("\n token = %s\n", token );
	NoAst *novoNoAst = malloc(sizeof(NoAst));		
	novoNoAst->tipoNo = Id;
	strcpy(novoNoAst->noId.id, token);	
	return novoNoAst;
}

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}
