#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "CBasicMath.hpp"
#include "../../Scanner.cpp"
#include "../../Token/Token.h"

using namespace CppUnit;
using namespace std;

//-----------------------------------------------------------------------------

class TestTeste20 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestTeste20);
    CPPUNIT_TEST(TestTeste20Lexico);
    //CPPUNIT_TEST(testMultiply);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void TestTeste20Lexico(void);    

private:
    
    Scanner *scanner;
};

//-----------------------------------------------------------------------------

void
TestTeste20::TestTeste20Lexico(void)
{
    const char* fe[2];
    fe[0] = "comando"; 
    fe[1] = "../../testes/teste20.lucas"; 
    scanner->executar(1,fe);
    vector<Token> t = scanner->mostrar();
    
    CPPUNIT_ASSERT(19 == t.size());
    CPPUNIT_ASSERT("KEY" == t.at(0).getTipoToken());
    CPPUNIT_ASSERT("def" == t.at(0).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(1).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(1).getLexema());   
    CPPUNIT_ASSERT("ID" == t.at(2).getTipoToken());
    CPPUNIT_ASSERT("add" == t.at(2).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(3).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(3).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(4).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(4).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(5).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(5).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(6).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(6).getLexema());
    CPPUNIT_ASSERT("KEY" == t.at(7).getTipoToken());
    CPPUNIT_ASSERT("int" == t.at(7).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(8).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(8).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(9).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(9).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(10).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(10).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(11).getTipoToken());
    CPPUNIT_ASSERT("return" == t.at(11).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(12).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(12).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(13).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(13).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(14).getTipoToken());
    CPPUNIT_ASSERT("+" == t.at(14).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(15).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(15).getLexema());
    
    CPPUNIT_ASSERT("SYM" == t.at(16).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(16).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(17).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(17).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(18).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(18).getLexema());
}


void TestTeste20::setUp(void)
{   
    //char *f[] = "../../testes/Teste20.lucas" ;
    //f->push_back("../../testes/Teste20.lucas");
    //scanner = new Scanner(1, f);
    scanner = new Scanner();
}

void TestTeste20::tearDown(void)
{
    delete scanner;
}

//-----------------------------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( TestTeste20 );

int main(int argc, char* argv[])
{
    //this.f = argv;
    // informs test-listener about testresults    
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    ofstream xmlFileOut("cppTestBasicMathResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
