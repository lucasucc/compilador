#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "CBasicMath.hpp"
#include "../../Scanner.cpp"
#include "../../Token/Token.h"

using namespace CppUnit;
using namespace std;

//-----------------------------------------------------------------------------

class TestTeste31 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestTeste31);
    CPPUNIT_TEST(TestTeste31Lexico);
    //CPPUNIT_TEST(testMultiply);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void TestTeste31Lexico(void);    

private:
    
    Scanner *scanner;
};

//-----------------------------------------------------------------------------

void
TestTeste31::TestTeste31Lexico(void)
{
    const char* fe[2];
    fe[0] = "comando"; 
    fe[1] = "../../testes/teste31.lucas"; 
    scanner->executar(1,fe);
    vector<Token> t = scanner->mostrar();
    
    CPPUNIT_ASSERT(43 == t.size());
    CPPUNIT_ASSERT("KEY" == t.at(0).getTipoToken());
    CPPUNIT_ASSERT("if" == t.at(0).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(1).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(1).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(2).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(2).getLexema());
    
    CPPUNIT_ASSERT("SYM" == t.at(3).getTipoToken());
    CPPUNIT_ASSERT(">" == t.at(3).getLexema()); 
    
    CPPUNIT_ASSERT("ID" == t.at(4).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(4).getLexema());
        
    CPPUNIT_ASSERT("SYM" == t.at(5).getTipoToken());
    CPPUNIT_ASSERT("||" == t.at(5).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(6).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(6).getLexema());
    
    CPPUNIT_ASSERT("SYM" == t.at(7).getTipoToken());
    CPPUNIT_ASSERT(">=" == t.at(7).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(8).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(8).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(9).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(9).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(10).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(10).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(11).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(11).getLexema());   

    CPPUNIT_ASSERT("SYM" == t.at(12).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(12).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(13).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(13).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(14).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(14).getLexema());
    
    CPPUNIT_ASSERT("SYM" == t.at(15).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(15).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(16).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(16).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(17).getTipoToken());
    CPPUNIT_ASSERT("else" == t.at(17).getLexema());

    CPPUNIT_ASSERT("KEY" == t.at(18).getTipoToken());
    CPPUNIT_ASSERT("if" == t.at(18).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(19).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(19).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(20).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(20).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(21).getTipoToken());
    CPPUNIT_ASSERT(">" == t.at(21).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(22).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(22).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(23).getTipoToken());
    CPPUNIT_ASSERT("&&" == t.at(23).getLexema());
    //
    CPPUNIT_ASSERT("ID" == t.at(24).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(24).getLexema());
    CPPUNIT_ASSERT("SYM" == t.at(25).getTipoToken());
    CPPUNIT_ASSERT(">=" == t.at(25).getLexema());
    CPPUNIT_ASSERT("ID" == t.at(26).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(26).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(27).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(27).getLexema());

    
    CPPUNIT_ASSERT("SYM" == t.at(28).getTipoToken());
    CPPUNIT_ASSERT("{" == t.at(28).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(29).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(29).getLexema());   

    CPPUNIT_ASSERT("SYM" == t.at(30).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(30).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(31).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(31).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(32).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(32).getLexema());
    
    CPPUNIT_ASSERT("SYM" == t.at(33).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(33).getLexema());   

    CPPUNIT_ASSERT("SYM" == t.at(34).getTipoToken());
    CPPUNIT_ASSERT("}" == t.at(34).getLexema());


    CPPUNIT_ASSERT("KEY" == t.at(35).getTipoToken());
    CPPUNIT_ASSERT("else" == t.at(35).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(36).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(36).getLexema());   

    CPPUNIT_ASSERT("SYM" == t.at(37).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(37).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(38).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(38).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(39).getTipoToken());
    CPPUNIT_ASSERT("," == t.at(39).getLexema());

    CPPUNIT_ASSERT("ID" == t.at(40).getTipoToken());
    CPPUNIT_ASSERT("y" == t.at(40).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(41).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(41).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(42).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(42).getLexema());




    


}


void TestTeste31::setUp(void)
{   
    //char *f[] = "../../testes/teste31.lucas" ;
    //f->push_back("../../testes/teste31.lucas");
    //scanner = new Scanner(1, f);
    scanner = new Scanner();
}

void TestTeste31::tearDown(void)
{
    delete scanner;
}

//-----------------------------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( TestTeste31 );

int main(int argc, char* argv[])
{
    //this.f = argv;
    // informs test-listener about testresults    
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    ofstream xmlFileOut("cppTestBasicMathResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
