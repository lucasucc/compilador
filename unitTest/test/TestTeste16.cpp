#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "CBasicMath.hpp"
#include "../../Scanner.cpp"
#include "../../Token/Token.h"

using namespace CppUnit;
using namespace std;

//-----------------------------------------------------------------------------

class TestTeste16 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestTeste16);
    CPPUNIT_TEST(TestTeste16Lexico);
    //CPPUNIT_TEST(testMultiply);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void TestTeste16Lexico(void);    

private:
    
    Scanner *scanner;
};

//-----------------------------------------------------------------------------

void
TestTeste16::TestTeste16Lexico(void)
{
    const char* fe[2];
    fe[0] = "comando"; 
    fe[1] = "../../testes/teste16.lucas"; 
    scanner->executar(1,fe);
    vector<Token> t = scanner->mostrar();
    
    CPPUNIT_ASSERT(7 == t.size());
    CPPUNIT_ASSERT("ID" == t.at(0).getTipoToken());
    CPPUNIT_ASSERT("printf" == t.at(0).getLexema());
    
    CPPUNIT_ASSERT("SYM" == t.at(1).getTipoToken());
    CPPUNIT_ASSERT("(" == t.at(1).getLexema());
    
    CPPUNIT_ASSERT("ID" == t.at(2).getTipoToken());
    CPPUNIT_ASSERT("x" == t.at(2).getLexema());    

    CPPUNIT_ASSERT("SYM" == t.at(3).getTipoToken());
    CPPUNIT_ASSERT("==" == t.at(3).getLexema());

    CPPUNIT_ASSERT("DEC" == t.at(4).getTipoToken());
    CPPUNIT_ASSERT("0" == t.at(4).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(5).getTipoToken());
    CPPUNIT_ASSERT(")" == t.at(5).getLexema());

    CPPUNIT_ASSERT("SYM" == t.at(6).getTipoToken());
    CPPUNIT_ASSERT(";" == t.at(6).getLexema());
}


void TestTeste16::setUp(void)
{   
    //char *f[] = "../../testes/Teste16.lucas" ;
    //f->push_back("../../testes/Teste16.lucas");
    //scanner = new Scanner(1, f);
    scanner = new Scanner();
}

void TestTeste16::tearDown(void)
{
    delete scanner;
}

//-----------------------------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( TestTeste16 );

int main(int argc, char* argv[])
{
    //this.f = argv;
    // informs test-listener about testresults    
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    ofstream xmlFileOut("cppTestBasicMathResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
